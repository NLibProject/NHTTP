#ifndef NHTTP_CLIENT_OUTIL_PROTECT
#define NHTTP_CLIENT_OUTIL_PROTECT

// ------------------------------
// namespace NHTTP::Client::Outil
// ------------------------------

/**
 * Callback reception
 *
 * @param packet
 * 		Le packet re�u
 * @param client
 * 		Le client
 *
 * @return si l'operation a reussi
 */
__CALLBACK NBOOL NHTTP_Client_Outil_CallbackReception( const NPacket *packet,
	const NClient *client );

/**
 * Callback deconnexion
 *
 * @param client
 * 		Le client qui se deconnecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Client_Outil_CallbackDeconnexion( const NClient *client );

/**
 * C
 */
#endif // !NHTTP_CLIENT_OUTIL_PROTECT
