#ifndef NHTTP_CLIENT_NCLIENTHTTP_PROTECT
#define NHTTP_CLIENT_NCLIENTHTTP_PROTECT

// ---------------------------------
// struct NHTTP::Client::NClientHTTP
// ---------------------------------

typedef struct NClientHTTP
{
	// Client
	NClient *m_client;

	// Resolution domaine
	NListeResolution *m_resolution;

	// Donnees utilisateur
	const void *m_dataUtilisateur;

	// Delais entre envois de paquets
	NU32 m_delaiEntreEnvoi;

	// Temps dernier envoi
	NU64 m_tickDernierEnvoi;
} NClientHTTP;

/**
 * Construire client par defaut qui affichera en sortie standard ce qui est recu si IS_DEFAULT_HTTP_RECEIVE_CALLBACK_DISPLAY_STDOUT est defini
 *
 * @param nomDomaine
 * 		Le nom de domaine auquel se connecter
 * @param port
 * 		Le port auquel se connecter
 * @param dataUtilisateur
 * 		Donnees utilisateur (peut etre nul)
 *
 * @return l'instance du client
 */
__ALLOC NClientHTTP *NHTTP_Client_NClientHTTP_Construire( const char *nomDomaine,
	NU32 port,
	const void *dataUtilisateur );

/**
 * Construire client avec callback reception personnalise
 *
 * @param nomDomaine
 * 		Le nom de domaine auquel se connecter
 * @param port
 * 		Le port auquel se connecter
 * @param dataUtilisateur
 * 		Donnees utilisateur (peut etre nul)
 * @param callbackReception
 * 		Le callback de reception
 *
 * @return l'instance du client
 */
__ALLOC NClientHTTP *NHTTP_Client_NClientHTTP_Construire2( const char *nomDomaine,
	NU32 port,
	const void *dataUtilisateur,
	NBOOL ( ___cdecl *callbackReception )( const NPacket *packet,
		const NClient *client ) );

/**
 * Construire client avec callback reception personnalise
 *
 * @param nomDomaine
 * 		Le nom de domaine auquel se connecter
 * @param port
 * 		Le port auquel se connecter
 * @param dataUtilisateur
 * 		Donnees utilisateur (peut etre nul)
 * @param callbackReception
 * 		Le callback de reception
 * @param timeoutReception
 * 		Le timeout de reception
 *
 * @return l'instance du client
 */
__ALLOC NClientHTTP *NHTTP_Client_NClientHTTP_Construire3( const char *nomDomaine,
	NU32 port,
	const void *dataUtilisateur,
	NBOOL ( ___cdecl *callbackReception )( const NPacket *packet,
		const NClient *client ),
	NU32 timeoutReception );

/**
 * Detruire client
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Client_NClientHTTP_Detruire( NClientHTTP** );

/**
 * Definir delai entre l'envoi de deux requetes
 *
 * @param this
 * 		Cette instance
 * @param delai
 * 		Le delai entre deux requetes (ms)
 */
void NHTTP_Client_NClientHTTP_DefinirDelaiEntreEnvoiDeuxRequete( NClientHTTP*,
	NU32 delai );
/**
 * Envoyer une requete
 *
 * @param this
 * 		Cette instance
 * @param requete
 * 		La requete a envoyer
 * @param timeoutConnexion
 * 		Le timeout a la connexion
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Client_NClientHTTP_EnvoyerRequete( NClientHTTP*,
	__WILLBEOWNED NRequeteHTTP *requete,
	NU32 timeoutConnexion );

/**
 * Envoyer une requete
 *
 * @param this
 * 		Cette instance
 * @param typeRequete
 * 		Le type de requete a envoyer
 * @param elementRequete
 * 		L'element demande
 * @param timeoutConnexion
 * 		Le timeout a la connexion
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Client_NClientHTTP_EnvoyerRequete2( NClientHTTP*,
	NTypeRequeteHTTP typeRequete,
	const char *elementRequete,
	const char *dataRequete,
	NU32 tailleDataRequete,
	NU32 timeoutConnexion );

/**
 * Envoyer une requete authentifiee
 *
 * @param this
 * 		Cette instance
 * @param typeRequete
 *		Le type de requete a envoyer
 * @param elementRequete
 * 		L'element demande
 * @param timeoutConnexion
 * 		Le timeout a la connexion
 * @param basicAuthUsername
 * 		Le nom d'utilisateur (ou NULL)
 * @param basicAuthPassword
 * 		Le mot de passe (ou NULL)
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Client_NClientHTTP_EnvoyerRequete3( NClientHTTP*,
	NTypeRequeteHTTP typeRequete,
	const char *elementRequete,
	const char *dataRequete,
	NU32 tailleDataRequete,
	NU32 timeoutConnexion,
	const char *basicAuthUsername,
	const char *basicAuthPassword );

/**
 * Obtenir donnees utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees utilisateur
 */
void *NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( const NClientHTTP* );

/**
 * Obtenir IP serveur a contacter
 *
 * @param this
 * 		Cette instance
 *
 * @return l'ip qui sera contactee
 */
const char *NHTTP_Client_NClientHTTP_ObtenirIPServeur( const NClientHTTP* );

/**
 * Est packet en attente?
 *
 * @param this
 * 		Cette instance
 *
 * @return si il y a des packets en attente
 */
NBOOL NHTTP_Client_NClientHTTP_EstPacketAttente( const NClientHTTP* );

/**
 * Est erreur?
 *
 * @param this
 * 		Cette instance
 *
 * @return si il y a une erreur
 */
NBOOL NHTTP_Client_NClientHTTP_EstErreur( const NClientHTTP* );

#endif // !NHTTP_CLIENT_NCLIENTHTTP_PROTECT
