#ifndef NHTTP_CLIENT_PROTECT
#define NHTTP_CLIENT_PROTECT

// -----------------------
// namespace NHTTP::Client
// -----------------------

// Temps avant timeout reception
#define NHTTP_CLIENT_TEMPS_AVANT_TIMEOUT_RECEPTION		500

// User agent client
#define NHTTP_CLIENT_NOM_CLIENT							"NHTTPClientV2"

// namespace NHTTP::Client::Outil
#include "Outil/NHTTP_Client_Outil.h"

// struct NHTTP::Client::NClientHTTP
#include "NHTTP_Client_NClientHTTP.h"

#endif // !NHTTP_CLIENT_PROTECT
