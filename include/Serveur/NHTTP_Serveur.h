#ifndef NHTTP_SERVEUR_PROTECT
#define NHTTP_SERVEUR_PROTECT

// ------------------------
// namespace NHTTP::Serveur
// ------------------------

// Nom du serveur
#define NHTTP_SERVEUR_NOM_SERVEUR					"NServeurHTTPV2"

// Fichier par defaut si on essaye d'obtenir /
#define NHTTP_SERVEUR_HTTP_FICHIER_DEFAUT			"index.html"

// namespace NHTTP::Serveur::Log
#include "Log/NHTTP_Serveur_Log.h"

// namespace NHTTP::Serveur::Outil
#include "Outil/NHTTP_Serveur_Outil.h"

// struct NHTTP::Serveur::NHTTPServeur
#include "NHTTP_Serveur_NHTTPServeur.h"

// namespace NHTTP::Serveur::Commande
#include "Commande/NHTTP_Serveur_Commande.h"

// namespace NHTTP::Serveur::Erreur
#include "Erreur/NHTTP_Serveur_Erreur.h"

#endif // !NHTTP_SERVEUR_PROTECT
