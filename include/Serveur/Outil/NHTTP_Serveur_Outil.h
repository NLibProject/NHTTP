#ifndef NHTTP_Serveur_Outil_PROTECT
#define NHTTP_Serveur_Outil_PROTECT

// -------------------------------
// namespace NHTTP::Serveur::Outil
// -------------------------------

/**
 * Callback reception packet
 *
 * @param client
 * 		Le client ayant emis le packet
 * @param packet
 * 		Le packet recu
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Serveur_Outil_CallbackReception( const NClientServeur *client,
	const NPacket *packet );

/**
 * Callback connexion client
 *
 * @param client
 * 		Le client qui se connecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Serveur_Outil_CallbackConnexion( const NClientServeur *client );

/**
 * Callback deconnexion client
 *
 * @param client
 * 		Le client qui se deconnecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Serveur_Outil_CallbackDeconnexion( const NClientServeur *client );

#endif // !NHTTP_Serveur_Outil_PROTECT
