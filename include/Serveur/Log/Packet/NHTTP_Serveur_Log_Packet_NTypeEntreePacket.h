#ifndef NHTTP_SERVEUR_LOG_PACKET_NTYPEENTREEPACKET_PROTECT
#define NHTTP_SERVEUR_LOG_PACKET_NTYPEENTREEPACKET_PROTECT

// ---------------------------------------------------
// enum NHTTP::Serveur::Log::Packet::NTypeEntreePacket
// ---------------------------------------------------

typedef enum NTypeEntreePacket
{
	NTYPE_ENTREE_PACKET_REQUETE,
	NTYPE_ENTREE_PACKET_REPONSE,

	NTYPES_ENTREE_PACKET
} NTypeEntreePacket;

/**
 * Obtenir type entree
 *
 * @param type
 * 		Le type d'entree
 *
 * @return le texte du type d'entree
 */
const char *NHTTP_Serveur_Log_Packet_NTypeEntreePacket_ObtenirType( NTypeEntreePacket );

#ifdef NHTTP_SERVEUR_LOG_PACKET_NTYPEENTREEPACKET_INTERNE
static const char NTypeEntreePacketTexte[ NTYPES_ENTREE_PACKET ][ 32 ] =
{
	"Requete",
	"Reponse"
};
#endif // NHTTP_SERVEUR_LOG_PACKET_NTYPEENTREEPACKET_INTERNE

#endif //NHTTP_SERVEUR_LOG_PACKET_NTYPEENTREEPACKET_PROTECT
