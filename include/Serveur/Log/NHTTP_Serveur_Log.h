#ifndef NHTTP_SERVEUR_LOG_PROTECT
#define NHTTP_SERVEUR_LOG_PROTECT

// -----------------------------
// namespace NHTTP::Serveur::Log
// -----------------------------

// Fichier de log des packets
#define NHTTP_SERVEUR_LOG_FICHIER_LOG_PACKET_DEFAUT					"NHTTPPacket.log"

// Fichier de log des evenements
#define NHTTP_SERVEUR_LOG_FICHIER_LOG_EVENEMENT_DEFAUT				"NHTTPServeur.log"

// Taille maximale fichier de log
#define NHTTP_SERVEUR_LOG_FICHIER_TAILLE_MAXIMALE_DEFAUT			10000000

// Repertoire d'archivage
#define NHTTP_SERVEUR_LOG_REPERTOIRE_ARCHIVAGE_DEFAUT				"Archive"

// enum NHTTP::Serveur::Log::NTypeEntreeCacheLogHTTP
#include "NHTTP_Serveur_Log_NTypeEntreeCacheLogHTTP.h"

// namespace NHTTP::Serveur::Log::Evenement
#include "Evenement/NHTTP_Serveur_Log_Evenement.h"

// namespace NHTTP::Serveur::Log::Packet
#include "Packet/NHTTP_Serveur_Log_Packet.h"

// struct NHTTP::Serveur::Log::NEntreeCacheLogHTTP
#include "NHTTP_Serveur_Log_NEntreeCacheLogHTTP.h"

// struct NHTTP::Serveur::Log::NCacheLogHTTP
#include "NHTTP_Serveur_Log_NCacheLogHTTP.h"

#endif // !NHTTP_SERVEUR_LOG_PROTECT

