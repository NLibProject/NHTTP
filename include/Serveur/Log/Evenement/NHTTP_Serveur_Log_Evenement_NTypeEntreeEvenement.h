#ifndef NHTTP_SERVEUR_LOG_EVENEMENT_NTYPEENTREEEVENEMENT_PROTECT
#define NHTTP_SERVEUR_LOG_EVENEMENT_NTYPEENTREEEVENEMENT_PROTECT

// ---------------------------------------------------------
// enum NHTTP::Serveur::Log::Evenement::NTypeEntreeEvenement
// ---------------------------------------------------------

typedef enum NTypeEntreeEvenement
{
	NTYPE_ENTREE_EVENEMENT_CONNEXION,
	NTYPE_ENTREE_EVENEMENT_DECONNEXION,
	NTYPE_ENTREE_EVENEMENT_REQUETE,
	NTYPE_ENTREE_EVENEMENT_REPONSE,

	NTYPES_ENTREE_EVENEMENT
} NTypeEntreeEvenement;

/**
 * Obtenir type d'entree
 *
 * @param type
 * 		Le type d'entree
 *
 * @return le texte associe
 */
const char *NHTTP_Serveur_Log_Evenement_NTypeEntreeEvenement_ObtenirType( NTypeEntreeEvenement type );

#ifdef NHTTP_SERVEUR_LOG_EVENEMENT_NTYPEENTREEEVENEMENT_INTERNE
static const char NTypeEntreeEvenementTexte[ NTYPES_ENTREE_EVENEMENT ][ 32 ] =
{
	"Connexion",
	"Deconnexion",
	"Requete",
	"Reponse"
};
#endif // NHTTP_SERVEUR_LOG_EVENEMENT_NTYPEENTREEEVENEMENT_INTERNE

#endif // !NHTTP_SERVEUR_LOG_EVENEMENT_NTYPEENTREEEVENEMENT_PROTECT

