#ifndef NHTTP_SERVEUR_LOG_NENTREECACHELOGHTTP_PROTECT
#define NHTTP_SERVEUR_LOG_NENTREECACHELOGHTTP_PROTECT

// -----------------------------------------------
// struct NHTTP::Serveur::Log::NEntreeCacheLogHTTP
// -----------------------------------------------

typedef struct NEntreeCacheLogHTTP
{
	// Type d'entree
	NTypeEntreeCacheLogHTTP m_type;

	// IP
	char *m_ip;

	// Sous type d'entree
	NU32 m_sousType;

	// Valeur
	char *m_valeur;

	// Date d'ajout
	NU64 m_date;

	// Identifiant
	NU32 m_identifiant;
} NEntreeCacheLogHTTP;

/**
 * Construire l'entree
 *
 * @param type
 * 		Le type d'entree
 * @param sousType
 * 		Le sous type de l'entree
 * @param valeur
 * 		La valeur
 * @param ip
 * 		L'ip du client ayant declenche l'evenement
 * @param identifiantRequete
 * 		L'identifiant de la requete
 *
 * @return la valeur
 */
__ALLOC NEntreeCacheLogHTTP *NHTTP_Serveur_Log_NEntreeCacheLogHTTP_Construire( NTypeEntreeCacheLogHTTP type,
	NU32 sousType,
	const char *valeur,
	__WILLBEOWNED char *ip,
	NU32 identifiantRequete );

/**
 * Detruire l'entree
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_Log_NEntreeCacheLogHTTP_Detruire( NEntreeCacheLogHTTP** );

/**
 * Obtenir type
 *
 * @param this
 * 		Cette instance
 *
 * @return le type
 */
NTypeEntreeCacheLogHTTP NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirType( const NEntreeCacheLogHTTP* );

/**
 * Obtenir sous type
 *
 * @param this
 * 		Cette instance
 *
 * @return le sous type
 */
NU32 NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirSousType( const NEntreeCacheLogHTTP* );

/**
 * Obtenir valeur
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur
 */
const char *NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirValeur( const NEntreeCacheLogHTTP* );

/**
 * Obtenir ip
 *
 * @param this
 * 		Cette instance
 *
 * @return l'ip du client
 */
const char *NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirIP( const NEntreeCacheLogHTTP* );

/**
 * Obtenir date
 *
 * @param this
 * 		Cette instance
 *
 * @return le timestamp de la date d'ajout
 */
NU64 NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirDate( const NEntreeCacheLogHTTP* );

/**
 * Generer sortie finale
 *
 * @param this
 * 		Cette instance
 *
 * @return la sortie finale a loguer
 */
__ALLOC char *NHTTP_Serveur_Log_NEntreeCacheLogHTTP_GenererSortie( const NEntreeCacheLogHTTP* );

#endif // !NHTTP_SERVEUR_LOG_NENTREECACHELOGHTTP_PROTECT

