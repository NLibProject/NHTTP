#ifndef NHTTP_SERVEUR_LOG_NCACHELOGHTTP_PROTECT
#define NHTTP_SERVEUR_LOG_NCACHELOGHTTP_PROTECT

// -----------------------------------------
// struct NHTTP::Serveur::Log::NCacheLogHTTP
// -----------------------------------------

typedef struct NCacheLogHTTP
{
	// Liste des entrees
	NListe *m_entree;

	// Lien vers fichier log packet
	char *m_lienFichierPacket;

	// Lien vers fichier log evenement
	char *m_lienFichierEvenement;

	// Repertoire d'archivage des log
	char *m_repertoireArchivage;

	// Taille maximale fichier log
	NU32 m_tailleMaximaleFichier;

	// Fichiers de log des packets
	NFichierTexte *m_fichierLogPacket;

	// Fichier de log des evenements
	NFichierTexte *m_fichierLogEvenement;

	// Thread update
	NThread *m_thread;

	// Thread en cours?
	NBOOL m_estContinuer;
} NCacheLogHTTP;

/**
 * Construire le cache
 *
 * @param lienFichierEvenement
 * 		Le lien vers le fichier de log des evenements
 * @param lienFichierPacket
 *		Le lien vers le fichier de log des packets
 * @param tailleMaximaleFichier
 * 		La taille maximale avant archivage
 * @param repertoireArchivage
 * 		Le repertoire d'archivage des logs
 *
 * @return l'instance du cache
 */
__ALLOC NCacheLogHTTP *NHTTP_Serveur_Log_NCacheLogHTTP_Construire( const char *lienFichierEvenement,
	const char *lienFichierPacket,
	NU32 tailleMaximaleFichier,
	const char *repertoireArchivage );

/**
 * Detruire le cache
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_Log_NCacheLogHTTP_Detruire( NCacheLogHTTP** );

/**
 * Ajouter une entree
 *
 * @param this
 * 		Cette instance
 * @param entree
 * 		L'entree a ajouter
 *
 * @return si l'operation s'est bien passee
 */
__WILLLOCK __WILLUNLOCK  NBOOL NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree( NCacheLogHTTP*,
	__WILLBEOWNED NEntreeCacheLogHTTP* );

/**
 * Ajouter une entree
 *
 * @param this
 * 		Cette instance
 * @param type
 * 		Le type d'entree
 * @param sousType
 * 		Le sous type d'entree
 * @param valeur
 * 		La valeur de l'entree
 * @param ip
 * 		L'ip du client
 * @param identifiant
 * 		L'identifiant de la requete
 *
 * @return si l'operation s'est bien passee
 */
__WILLLOCK __WILLUNLOCK NBOOL NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree2( NCacheLogHTTP *this,
	NTypeEntreeCacheLogHTTP type,
	NU32 sousType,
	const char *valeur,
	const char *ip,
	NU32 identifiant );

#endif // !NHTTP_SERVEUR_LOG_NCACHELOGHTTP_PROTECT

