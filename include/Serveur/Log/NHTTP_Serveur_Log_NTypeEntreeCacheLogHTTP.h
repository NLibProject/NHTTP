#ifndef NHTTP_SERVEUR_LOG_NTYPEENTREECACHELOGHTTP_PROTECT
#define NHTTP_SERVEUR_LOG_NTYPEENTREECACHELOGHTTP_PROTECT

// -------------------------------------------------
// enum NHTTP::Serveur::Log::NTypeEntreeCacheLogHTTP
// -------------------------------------------------

typedef enum NTypeEntreeCacheLogHTTP
{
	NTYPE_ENTREE_CACHE_LOG_HTTP_EVENEMENT,
	NTYPE_ENTREE_CACHE_LOG_HTTP_PACKET,

	NTYPES_ENTREE_CACHE_LOG_HTTP_PACKET
} NTypeEntreeCacheLogHTTP;

#endif // !NHTTP_SERVEUR_LOG_NTYPEENTREECACHELOGHTTP_PROTECT
