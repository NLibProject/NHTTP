#ifndef NHTTP_SERVEUR_COMMANDE_NTYPECOMMANDE_PROTECT
#define NHTTP_SERVEUR_COMMANDE_NTYPECOMMANDE_PROTECT

// --------------------------------------------
// enum NHTTP::Serveur::Commande::NTypeCommande
// --------------------------------------------

typedef enum NTypeCommande
{
	// NULL
	NTYPE_COMMANDE_AUCUNE,

	// Afficher l'aide
	NTYPE_COMMANDE_AIDE,
	NTYPE_COMMANDE_AIDE2,
	NTYPE_COMMANDE_AIDE3,

	// Fermer/ouvrir le serveur
	NTYPE_COMMANDE_FERMER_SERVEUR,
	NTYPE_COMMANDE_OUVRIR_SERVEUR,

	// Activer/Desactiver log
	NTYPE_COMMANDE_ACTIVER_LOG_CONSOLE,
	NTYPE_COMMANDE_DESACTIVER_LOG_CONSOLE,

	// Afficher informations interfaces reseau
	NTYPE_COMMANDE_AFFICHER_INTERFACE,

	// A propos
	NTYPE_COMMANDE_ABOUT,

	// Quitter
	NTYPE_COMMANDE_QUITTER,

	NTYPES_COMMANDE
} NTypeCommande;

/**
 * Interpreter commande
 *
 * @param commande
 * 		La commande saisie
 *
 * @return le resultat de l'interpretation
 */
NTypeCommande NHTTP_Serveur_Commande_NTypeCommande_Interpreter( const char *commande );

/**
 * Obtenir commande
 *
 * @param commande
 * 		La commande a obtenir
 *
 * @return la commande
 */
const char *NHTTP_Serveur_Commande_NTypeCommande_ObtenirCommande( NTypeCommande commande );

/**
 * Obtenir description commande
 *
 * @param commande
 * 		La commande a obtenir
 *
 * @return la description de la commande
 */
const char *NHTTP_Serveur_Commande_NTypeCommande_ObtenirDescriptionCommande( NTypeCommande commande );

#ifdef NHTTP_SERVEUR_COMMANDE_NTYPECOMMANDE_INTERNE
static const char NTypeCommandeTexte[ NTYPES_COMMANDE ][ 32 ] =
{
	"",

	"Aide",
	"Help",
	"?",

	"Fermer",
	"Ouvrir",

	"ActiverLog",
	"DesactiverLog",

	"AfficherIP",

	"About",

	"Quitter"
};

static const char NTypeCommandeDescriptionTexte[ NTYPES_COMMANDE ][ 64 ] =
{
	"",

	"Afficher l'aide",
	"Afficher l'aide",
	"Afficher l'aide",

	"Interdire la connexion au serveur",
	"Autoriser la connexion au serveur",

	"Activer le log en console",
	"Desactiver le log en console",

	"Afficher les adresses l'IPv4 de la machine",

	"Afficher des informations supplementaires",

	"Fermer le serveur"
};
#endif // NHTTP_SERVEUR_COMMANDE_NTYPECOMMANDE_INTERNE

#endif // !NHTTP_SERVEUR_COMMANDE_NTYPECOMMANDE_PROTECT
