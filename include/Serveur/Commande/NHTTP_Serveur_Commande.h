#ifndef NHTTP_SERVEUR_COMMANDE_PROTECT
#define NHTTP_SERVEUR_COMMANDE_PROTECT

// ----------------------------------
// namespace NHTTP::Serveur::Commande
// ----------------------------------

// enum NHTTP::Serveur::Commande::NTypeCommande
#include "NHTTP_Serveur_Commande_NTypeCommande.h"

/**
 * Afficher l'aide
 */
void NHTTP_Serveur_Commande_AfficherAide( void );

/**
 * Thread de gestion des commandes
 *
 * @param serveur
 * 		Le serveur http
 *
 * @return si le thread s'est deroule normalement
 */
__THREAD NBOOL NHTTP_Serveur_Commande_Thread( NHTTPServeur *serveur );

#endif // !NHTTP_SERVEUR_COMMANDE_PROTECT
