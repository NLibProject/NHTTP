#ifndef NHTTP_Serveur_Erreur_PROTECT
#define NHTTP_Serveur_Erreur_PROTECT

// --------------------------------
// namespace NHTTP::Serveur::Erreur
// --------------------------------

/**
 * Flux de sortie des erreurs
 *
 * @param erreur
 * 		L'erreur
 */
__CALLBACK void NHTTP_Serveur_Erreur_FluxErreur( const NErreur *erreur );

#endif // !NHTTP_Serveur_Erreur_PROTECT
