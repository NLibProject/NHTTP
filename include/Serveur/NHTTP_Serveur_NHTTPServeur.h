#ifndef NHTTP_SERVEUR_NHTTPSERVEUR_PROTECT
#define NHTTP_SERVEUR_NHTTPSERVEUR_PROTECT

// -----------------------------------
// struct NHTTP::Serveur::NHTTPServeur
// -----------------------------------

typedef struct NHTTPServeur
{
	// Serveur
	NServeur *m_serveur;

	// Est en cours?
	NBOOL m_estEnCours;

	// Utilise un thread commande?
	NBOOL m_estThreadCommande;

	// Cache log
	NCacheLogHTTP *m_cacheLog;

	// Callback traitement requete (Retourne NReponseHTTP*, prend NRequeteHTTP*)
	__CALLBACK __ALLOC NReponseHTTP *( ___cdecl *m_callbackTraitementRequete )( const NRequeteHTTP*,
		const NClientServeur* );

	// Thread commande
	NThread *m_threadCommande;

	// Data utilisateur
	void *m_data;
} NHTTPServeur;

/**
 * Construire serveur
 *
 * @param port
 * 		Le port d'ecoute du serveur
 * @param interfaceEcoute
 * 		L'adresse ip d'ecoute (NULL pour toutes les interfaces)
 * @param estOuvert
 * 		Le serveur est ouvert au lancement
 * @param estAccepteCommandeShell
 * 		Le serveur peut etre pilote via CLI
 * @param lienLogEvenement
 * 		Le lien vers le log des evenements (Si NULL, log evenements desactive)
 * @param lienLogPacket
 * 		Le lien vers le log des packets (Si NULL, log packets desactive)
 * @param tailleMaximaleFichierLog
 * 		La taille maximale des fichiers de log
 * @param repertoireArchivageLog
 * 		Le repertoire d'archivage des logs
 * @param callbackTraitementRequete
 * 		Le callback de traitement de la requete
 * @param dataUtilisateur
 * 		Les donnees utilisateur
 *
 * @return le serveur
 */
__ALLOC NHTTPServeur *NHTTP_Serveur_NHTTPServeur_Construire( NU32 port,
	const char *interfaceEcoute,
	NBOOL estOuvert,
	NBOOL estAccepteCommandeShell,
	const char *lienLogEvenement,
	const char *lienLogPacket,
	NU32 tailleMaximaleFichierLog,
	const char *repertoireArchivageLog,
	__CALLBACK __ALLOC NReponseHTTP *( *callbackTraitementRequete )( const NRequeteHTTP*,
	const NClientServeur* ),
	void *dataUtilisateur );

/**
 * Construire serveur HTTP avec log par defaut
 *
 * @param port
 * 		Le port d'ecoute du serveur
 * @param estOuvert
 * 		Le serveur est ouvert au lancement
 * @param estAccepteCommandeShell
 * 		Le serveur peut etre pilote via CLI
 *
 * @return le serveur
 */
__ALLOC NHTTPServeur *NHTTP_Serveur_NHTTPServeur_Construire2( NU32 port,
	NBOOL estOuvert,
	NBOOL estAccepteCommandeShell );

/**
 * Construire serveur HTTP sans log
 *
 * @param port
 * 		Le port d'ecoute du serveur
 * @param estOuvert
 * 		Le serveur est ouvert au lancement
 * @param estAccepteCommandeShell
 * 		Le serveur peut etre pilote via CLI
 *
 * @return le serveur
 */
__ALLOC NHTTPServeur *NHTTP_Serveur_NHTTPServeur_Construire3( NU32 port,
	NBOOL estOuvert,
	NBOOL estAccepteCommandeShell );

/**
 * Detruire serveur
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_Detruire( NHTTPServeur** );

/**
 * Est en cours?
 *
 * @param this
 * 		Cette instance
 */
NBOOL NHTTP_Serveur_NHTTPServeur_EstEnCours( const NHTTPServeur* );

/**
 * Arreter le serveur
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_Arreter( NHTTPServeur* );

/**
 * Est gere par un thread de commandes shell?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le serveur est gere par un thread de commande
 */
NBOOL NHTTP_Serveur_NHTTPServeur_EstThreadCommandeActif( NHTTPServeur* );

/**
 * Ouvrir le serveur
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_Ouvrir( NHTTPServeur* );

/**
 * Fermer le serveur
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_Fermer( NHTTPServeur* );

/**
 * Activer log console
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_ActiverLogConsole( NHTTPServeur* );

/**
 * Desactiver log console
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_DesactiverLogConsole( NHTTPServeur* );

/**
 * Obtenir cache log
 *
 * @param this
 * 		Cette instance
 *
 * @return le cache log
 */
const NCacheLogHTTP *NHTTP_Serveur_NHTTPServeur_ObtenirCacheLog( const NHTTPServeur* );

/**
 * Traiter requete
 *
 * @param this
 * 		Cette instance
 * @param requete
 * 		La requete a traiter
 * @param client
 * 		Le client ayant effectue la requete
 *
 * @return la reponse generee par le traitement (NReponseHTTP*)
 */
__ALLOC NReponseHTTP *NHTTP_Serveur_NHTTPServeur_TraiterRequete( NHTTPServeur*,
	const NRequeteHTTP *requete,
	const NClientServeur *client );

/**
 * Definir data utilisateur
 *
 * @param this
 * 		Cette instance
 * @param data
 * 		Les data de l'utilisateur
 */
void NHTTP_Serveur_NHTTPServeur_DefinirDataUtilisateur( NHTTPServeur*,
	void *data );

/**
 * Obtenir data utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return les data utilisateur
 */
void *NHTTP_Serveur_NHTTPServeur_ObtenirDataUtilisateur( const NHTTPServeur* );

#endif // !NHTTP_SERVEUR_NHTTPSERVEUR_PROTECT
