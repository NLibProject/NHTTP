#ifndef NHTTP_PROTECT
#define NHTTP_PROTECT

// ---------------
// namespace NHTTP
// ---------------

// namespace NLib
#include "../../NLib/NLib/include/NLib/NLib.h"

// Temps avant timeout pour considere qu'un packet de requete a fini d'etre transfere
#define NHTTP_SERVEUR_TEMPS_AVANT_TIMEOUT_RECEPTION		100

// Temps avant timeout pour l'emission
#define NHTTP_SERVEUR_TEMPS_AVANT_TIMEOUT_EMISSION		10000

// Le callback par defaut pour la reception affiche le message recu?
//#define IS_DEFAULT_HTTP_RECEIVE_CALLBACK_DISPLAY_STDOUT

// namespace NHTTP::Commun
#include "Commun/NHTTP_Commun.h"

// namespace NHTTP::Serveur
#include "Serveur/NHTTP_Serveur.h"

// namespace NHTTP::Client
#include "Client/NHTTP_Client.h"

// Port par defaut
#define NHTTP_PORT_DEFAUT								8080

#endif // !NHTTP_PROTECT

