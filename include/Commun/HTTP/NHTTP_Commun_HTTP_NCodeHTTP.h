#ifndef NHTTP_COMMUN_HTTP_NCODEHTTP_PROTECT
#define NHTTP_COMMUN_HTTP_NCODEHTTP_PROTECT

// -----------------------------------
// enum NHTTP::Commun::HTTP::NCodeHTTP
// -----------------------------------

typedef enum NHTTPCode
{
	NHTTP_CODE_100_CONTINUE,
	NHTTP_CODE_101_SWITCHING_PROTOCOLS,
	NHTTP_CODE_102_PROCESSING,

	NHTTP_CODE_200_OK,
	NHTTP_CODE_201_CREATED,
	NHTTP_CODE_202_ACCEPTED,
	NHTTP_CODE_203_NON_AUTHORITATIVE_INFORMATION,
	NHTTP_CODE_204_NO_CONTENT,
	NHTTP_CODE_205_RESET_CONTENT,
	NHTTP_CODE_206_PARTIAL_CONTENT,
	NHTTP_CODE_207_MULTI_STATUS,
	NHTTP_CODE_208_ALREADY_REPORTED,
	NHTTP_CODE_210_CONTENT_DIFFERENT,
	NHTTP_CODE_226_IM_USED,

	NHTTP_CODE_300_MULTIPLE_CHOICES,
	NHTTP_CODE_301_MOVED_PERMANENTLY,
	NHTTP_CODE_302_FOUND_DOCUMENT,
	NHTTP_CODE_303_SEE_OTHER,
	NHTTP_CODE_304_NOT_MODIFIED,
	NHTTP_CODE_305_USE_PROXY,
	NHTTP_CODE_306_ANY,
	NHTTP_CODE_307_TEMPORARY_REDIRECT,
	NHTTP_CODE_308_PERMANENT_REDIRECT,
	NHTTP_CODE_310_TOO_MANY_REDIRECTS,

	NHTTP_CODE_400_BAD_REQUEST,
	NHTTP_CODE_401_UNAUTHORIZED,
	NHTTP_CODE_402_PAYMENT_REQUIRED,
	NHTTP_CODE_403_FORBIDDEN,
	NHTTP_CODE_404_NOT_FOUND,
	NHTTP_CODE_405_METHOD_NOT_ALLOWED,
	NHTTP_CODE_406_NOT_ACCEPTABLE,
	NHTTP_CODE_407_PROXY_AUTHENTICATION_REQUIRED,
	NHTTP_CODE_408_REQUEST_TIMEOUT,
	NHTTP_CODE_409_CONFLICT,
	NHTTP_CODE_410_GONE,
	NHTTP_CODE_411_LENGTH_REQUIRED,
	NHTTP_CODE_412_PRECONDITION_FAILED,
	NHTTP_CODE_413_REQUEST_ENTITY_TOO_LARGE,
	NHTTP_CODE_414_REQUEST_URI_TOO_LONG,
	NHTTP_CODE_415_UNSUPPORTED_MEDIA_TYPE,
	NHTTP_CODE_416_REQUESTED_RANGE_UNSATISFIABLE,
	NHTTP_CODE_417_EXPECTATION_FAILED,
	NHTTP_CODE_418_I_M_A_TEAPOT,
	NHTTP_CODE_421_BAD_MAPPING_MISDIRECTED_REQUEST,
	NHTTP_CODE_422_UNPROCESSABLE_ENTITY,
	NHTTP_CODE_423_LOCKED,
	NHTTP_CODE_424_METHOD_FAILURE,
	NHTTP_CODE_425_UNORDERED_COLLECTION,
	NHTTP_CODE_426_UPGRADE_REQUIRED,
	NHTTP_CODE_428_PRECONDITION_REQUIRED,
	NHTTP_CODE_429_TOO_MANY_REQUESTS,
	NHTTP_CODE_431_REQUEST_HEADER_FIELDS_TOO_LARGE,
	NHTTP_CODE_449_RETRY_WITH,
	NHTTP_CODE_450_BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS,
	NHTTP_CODE_451_UNAVAILABLE_FOR_LEGAL_REASONS,
	NHTTP_CODE_456_UNRECOVERABLE_ERROR,

	NHTTP_CODE_444_NO_RESPONSE,
	NHTTP_CODE_495_SSL_CERTIFICATE_ERROR,
	NHTTP_CODE_496_SSL_CERTIFICATE_REQUIRED,
	NHTTP_CODE_497_HTTP_REQUEST_SENT_TO_HTTPS_PORT,
	NHTTP_CODE_499_CLIENT_CLOSED_REQUEST,

	NHTTP_CODE_500_INTERNAL_SERVER_ERROR,
	NHTTP_CODE_501_NOT_IMPLEMENTED,
	NHTTP_CODE_502_BAD_GATEWAY_OR_PROXY_ERROR,
	NHTTP_CODE_503_SERVICE_UNAVAILABLE,
	NHTTP_CODE_504_GATEWAY_TIMEOUT,
	NHTTP_CODE_505_HTTP_VERSION_NOT_SUPPORTED,
	NHTTP_CODE_506_VARIANT_ALSO_NEGOTIATES,
	NHTTP_CODE_507_INSUFFICIENT_STORAGE,
	NHTTP_CODE_508_LOOP_DETECTED,
	NHTTP_CODE_509_BANDWIDTH_LIMIT_EXCEEDED,
	NHTTP_CODE_510_NOT_EXTENDED,
	NHTTP_CODE_511_NETWORK_AUTHENTICATION_REQUIRED,

	NHTTP_CODE_520_UNKNOWN_ERROR,
	NHTTP_CODE_521_WEB_SERVER_IS_DOWN,
	NHTTP_CODE_522_CONNECTION_TIMED_OUT,
	NHTTP_CODE_523_ORIGIN_IS_UNREACHABLE,
	NHTTP_CODE_524_A_TIMEOUT_OCCURRED,
	NHTTP_CODE_525_SSL_HANDSHAKE_FAILED,
	NHTTP_CODE_526_INVALID_SSL_CERTIFICATE,
	NHTTP_CODE_527_RAILGUN_ERROR,

	NHTTP_CODES
} NHTTPCode;

/**
 * Obtenir code numero HTTP
 *
 * @param code
 * 		Le code http
 *
 * @return le numero du code
 */
NU32 NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeNumero( NHTTPCode code );

/**
 * Obtenir code depuis valeur numerique
 *
 * @param code
 * 		Le code numerique
 *
 * @return le code http (ou NHTTP_CODES si introuvable)
 */
NHTTPCode NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeDepuisNumero( NU32 code );

/**
 * Obtenir code texte
 *
 * @param code
 * 		Le code http
 *
 * @return le message du code
 */
const char *NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeTexte( NHTTPCode code );

#ifdef NHTTP_COMMUN_HTTP_NCODEHTTP_INTERNE
static const NU32 NHTTPCodeNumero[ NHTTP_CODES ] =
{
	100,
	101,
	102,

	200,
	201,
	202,
	203,
	204,
	205,
	206,
	207,
	208,
	210,
	226,

	300,
	301,
	302,
	303,
	304,
	305,
	306,
	307,
	308,
	310,

	400,
	401,
	402,
	403,
	404,
	405,
	406,
	407,
	408,
	409,
	410,
	411,
	412,
	413,
	414,
	415,
	416,
	417,
	418,
	421,
	422,
	423,
	424,
	425,
	426,
	428,
	429,
	431,
	449,
	450,
	451,
	456,

	444,
	495,
	496,
	497,
	499,

	500,
	501,
	502,
	503,
	504,
	505,
	506,
	507,
	508,
	509,
	510,
	511,

	520,
	521,
	522,
	523,
	524,
	525,
	526,
	527
};

static const char NHTTPCodeTexte[ NHTTP_CODES ][ 128 ] =
{
	"Continue",
	"Switching",
	"Processing",

	"OK",
	"Created",
	"Accepted",
	"Non-Authoritative Information",
	"No Content",
	"Reset Content",
	"Partial Content",
	"Multi-Status",
	"Already Reported",
	"Content Different",
	"IM Used",

	"Multiple Choices",
	"Moved Permanently",
	"Found",
	"See Other",
	"Not Modified",
	"Use Proxy",
	"(aucun)",
	"Temporary Redirect",
	"Permanent Redirect",
	"Too many Redirects",

	"Bad Request",
	"Unauthorized",
	"Payment Required",
	"Forbidden",
	"Not Found",
	"Method Not Allowed",
	"Not Acceptable",
	"Proxy Authentication Required",
	"Request Time-out",
	"Conflict",
	"Gone",
	"Length Required",
	"Precondition Failed",
	"Request Entity Too Large",
	"Request-URI Too Long",
	"Unsupported Media Type",
	"Requested range unsatisfiable",
	"Expectation failed",
	"I'm a teapot",
	"Bad mapping / Misdirected Request",
	"Unprocessable entity",
	"Locked",
	"Method failure",
	"Unordered Collection",
	"Upgrade Required",
	"Precondition Required",
	"Too Many Requests",
	"Request Header Fields Too Large",
	"Retry With",
	"Blocked by Windows Parental Controls",
	"Unavailable For Legal Reasons",
	"Unrecoverable Error",

	"No Response",
	"SSL Certificate Error",
	"SSL Certificate Required",
	"HTTP Request Sent to HTTPS Port",
	"Client Closed Request",

	"Internal Server Error",
	"Not Implemented",
	"Bad Gateway ou Proxy Error",
	"Service Unavailable",
	"Gateway Time-out",
	"HTTP Version not supported",
	"Variant Also Negotiates",
	"Insufficient storage",
	"Loop detected",
	"Bandwidth Limit Exceeded",
	"Not extended",
	"Network authentication required",
	"Unknown Error",
	"Web Server Is Down",
	"Connection Timed Out",
	"Origin Is Unreachable",
	"A Timeout Occurred",
	"SSL Handshake Failed",
	"Invalid SSL Certificate",
	"Railgun Error"
};
#endif // NHTTP_COMMUN_HTTP_NCODEHTTP_INTERNE

#endif // !NHTTP_COMMUN_HTTP_NCODEHTTP_PROTECT
