#ifndef NHTTP_COMMUN_HTTP_OUTIL_PROTECT
#define NHTTP_COMMUN_HTTP_OUTIL_PROTECT

// ------------------------------------
// namespace NHTTP::Commun::HTTP::Outil
// ------------------------------------

/**
 * Remplacer %XX par le caractere correspondant dans l'element
 *
 * @param element
 * 		L'element
 *
 * @return l'element corrige
 */
__ALLOC char *NHTTP_Commun_HTTP_Outil_RemplacerCaractereHTMLElementDemande( const char *element );

#endif // !NHTTP_COMMUN_HTTP_OUTIL_PROTECT
