#ifndef NHTTP_COMMUN_HTTP_PROTECT
#define NHTTP_COMMUN_HTTP_PROTECT

// -----------------------------
// namespace NHTTP::Commun::HTTP
// -----------------------------

// enum NHTTP::Commun::HTTP::NCodeHTTP
#include "NHTTP_Commun_HTTP_NCodeHTTP.h"

// enum NHTTP::Commun::HTTP::NTypeRequeteHTTP
#include "NHTTP_Commun_HTTP_NTypeRequeteHTTP.h"

// enum NHTTP::Commun::HTTP::NMotClefRequeteHTTP
#include "NHTTP_Commun_HTTP_NMotClefRequeteHTTP.h"

// enum NHTTP::Commun::HTTP::NMotClefReponseHTTP
#include "NHTTP_Commun_HTTP_NMotClefReponseHTTP.h"

// namespace NHTTP::Commun::HTTP::Mimetype
#include "Mimetype/NHTTP_Commun_HTTP_Mimetype.h"

// namespace NHTTP::Commun::HTTP::Traitement
#include "Traitement/NHTTP_Commun_HTTP_Traitement.h"

// namespace NHTTP::Commun::HTTP::Outil
#include "Outil/NHTTP_Commun_HTTP_Outil.h"

#endif // !NHTTP_COMMUN_HTTP_PROTECT
