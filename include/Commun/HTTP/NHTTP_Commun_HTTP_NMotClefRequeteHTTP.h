#ifndef NHTTP_COMMUN_HTTP_NMOTCLEFREQUETEHTTP_PROTECT
#define NHTTP_COMMUN_HTTP_NMOTCLEFREQUETEHTTP_PROTECT

// ---------------------------------------------
// enum NHTTP::Commun::HTTP::NMotClefRequeteHTTP
// ---------------------------------------------

typedef enum NMotClefRequeteHTTP
{
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT_CHARSET,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT_ENCODING,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT_LANGUAGE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT_DATETIME,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_AUTHORIZATION,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_CACHE_CONTROL,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_CONNECTION,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_PERMANENT,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_COOKIE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_CONTENT_LENGTH,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_CONTENT_MD5,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_CONTENT_TYPE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_DATE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_DO_NOT_TRACE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_EXPECT,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_FORWARDED,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_FROM,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_HOST,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_IF_MATCH,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_IF_MODIFIED_SINCE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_IF_NONE_MATCH,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_IF_RANGE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_IF_UNMODIFIED_SINCE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_MAX_FORWARDS,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_ORIGIN,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_PRAGMA,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_PROXY_AUTHORIZATION,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_RANGE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_REFERER,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_TE,
	NMOT_CLEF_REQUETE_HTTP_UPGRADE_INSECURE_REQUESTS,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_USER_AGENT,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_UPGRADE,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_VIA,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_WARNING,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_GITLAB_EVENT,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_GITLAB_TOKEN,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_B3_SPANID,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_B3_TRACEID,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_CF_APPLICATIONID,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_CF_INSTANCEID,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_CF_INSTANCEINDEX,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_FORWARDED_FOR,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_FORWARDED_PROTO,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_REQUEST_START,
	NMOT_CLEF_REQUETE_HTTP_KEYWORD_X_VCAP_REQUEST_ID,

	NMOT_CLEF_REQUETE_HTTP_KEYWORDS
} NMotClefRequeteHTTP;

/**
 * Obtenir mot clef requete http
 *
 * @param typeRequete
 * 		Le type de requete
 *
 * @return le mot clef associe
 */
const char *NHTTP_Commun_HTTP_NMotClefRequeteHTTP_ObtenirMotClef( NMotClefRequeteHTTP typeRequete );

/**
 * Interpreter mot clef requete HTTP
 *
 * @param motClef
 *		Le mot clef
 *
 * @return le type de requete (NMOT_CLEF_REQUETE_HTTP_KEYWORDS si introuvable)
 */
NMotClefRequeteHTTP NHTTP_Commun_HTTP_NMotClefRequeteHTTP_InterpreterMotClef( const char *motClef );

#ifdef NHTTP_COMMUN_HTTP_NMOTCLEFREQUETEHTTP_INTERNE
static const char NMotClefRequeteHTTPTexte[ NMOT_CLEF_REQUETE_HTTP_KEYWORDS ][ 32 ] =
{
	"Accept",
	"Accept-Charset",
	"Accept-Encoding",
	"Accept-Language",
	"Accept-Datetime",
	"Authorization",
	"Cache-Control",
	"Connection",
	"Permanent",
	"Cookie",
	"Content-Length",
	"Content-MD5",
	"Content-Type",
	"Date",
	"DNT",
	"Expect",
	"Forwarded",
	"From",
	"Host",
	"If-Match",
	"If-Modified-Since",
	"If-None-Match",
	"If-Range",
	"If-Unmodified-Since",
	"Max-Forwards",
	"Origin",
	"Pragma",
	"Proxy-Authorization",
	"Range",
	"Referer",
	"TE",
	"Upgrade-Insecure-Requests",
	"User-Agent",
	"Upgrade",
	"Via",
	"Warning",
	"X-Gitlab-Event",
	"X-Gitlab-Token",
	"X-B3-Spanid",
	"X-B3-Traceid",
	"X-Cf-Applicationid",
	"X-Cf-Instanceid",
	"X-Cf-Instanceindex",
	"X-Forwarded-For",
	"X-Forwarded-Proto",
	"X-Request-Start",
	"X-Vcap-Request-Id"
};

#endif // NHTTP_COMMUN_HTTP_NMOTCLEFREQUETEHTTP_INTERNE

#endif // !NHTTP_COMMUN_HTTP_NMOTCLEFREQUETEHTTP_PROTECT
