#ifndef NHTTP_COMMUN_HTTP_NTYPEREQUETEHTTP_PROTECT
#define NHTTP_COMMUN_HTTP_NTYPEREQUETEHTTP_PROTECT

// ------------------------------------------
// enum NHTTP::Commun::HTTP::NTypeRequeteHTTP
// ------------------------------------------

// Mot clef requete
typedef enum NTypeRequeteHTTP
{
	NTYPE_REQUETE_HTTP_GET,
	NTYPE_REQUETE_HTTP_HEAD,
	NTYPE_REQUETE_HTTP_POST,
	NTYPE_REQUETE_HTTP_PUT,
	NTYPE_REQUETE_HTTP_DELETE,
	NTYPE_REQUETE_HTTP_CONNECT,
	NTYPE_REQUETE_HTTP_OPTIONS,
	NTYPE_REQUETE_HTTP_TRACE,
	NTYPE_REQUETE_HTTP_PATCH,

	NTYPE_REQUETE_HTTP_ABOUT,

	NTYPES_REQUETE_HTTP
} NTypeRequeteHTTP;

/**
 * Obtenir le mot clef du type de requete
 *
 * @param typeRequete
 * 		Le type de la requete
 *
 * @return le mot clef
 */
const char *NHTTP_Commun_HTTP_NTypeRequeteHTTP_ObtenirMotClef( NTypeRequeteHTTP typeRequete );

/**
 * Interpreter le type de requete
 *
 * @param type
 * 		Le type de requete
 *
 * @return le type de requete (NTYPES_REQUETE_HTTP si introuvable)
 */
NTypeRequeteHTTP NHTTP_Commun_HTTP_NTypeRequeteHTTP_Interpreter( const char *type );

#ifdef NHTTP_COMMUN_HTTP_NTYPEREQUETEHTTP_INTERNE
static const char NTypeRequeteHTTPTexte[ NTYPES_REQUETE_HTTP ][ 32 ] =
{
	"GET",
	"HEAD",
	"POST",
	"PUT",
	"DELETE",
	"CONNECT",
	"OPTIONS",
	"TRACE",
	"PATCH",

	"ABOUT"
};
#endif // NHTTP_COMMUN_HTTP_NTYPEREQUETEHTTP_INTERNE

#endif // !NHTTP_COMMUN_HTTP_NTYPEREQUETEHTTP_PROTECT
