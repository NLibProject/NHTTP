#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_PROTECT

// --------------------------------------------------
// namespace NHTTP::Commun::HTTP::Traitement::Reponse
// --------------------------------------------------

// struct NHTTP::Commun::HTTP::Traitement::Reponse::NElementReponseHTTP
#include "NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP.h"

// struct NHTTP::Commun::HTTP::Traitement::Reponse::NReponseHTTP
#include "NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP.h"

// namespace NHTTP::Commun::HTTP::Traitement::Reponse::Usine
#include "Usine/NHTTP_Commun_HTTP_Traitement_Reponse_Usine.h"

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_PROTECT
