#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_NELEMENTREPONSEHTTP_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_NELEMENTREPONSEHTTP_PROTECT

// --------------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Reponse::NElementReponseHTTP
// --------------------------------------------------------------------

typedef struct NElementReponseHTTP
{
	// Clef
	NMotClefReponseHTTP m_clef;

	// Valeur
	char *m_valeur;
} NElementReponseHTTP;

/**
 * Construire element reponse
 *
 * @param clef
 * 		La clef
 * @param valeur
 * 		La valeur
 *
 * @return l'instance de la reponse
 */
__ALLOC NElementReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire( NMotClefReponseHTTP clef,
	const char *valeur );
/**
 * Construire element depuis ligne header
 *
 * @param ligne
 * 		La ligne a parser
 *
 * @return l'instance de l'element
 */
__ALLOC NElementReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire2( const char *ligne );

/**
 * Detruire element
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Detruire( NElementReponseHTTP ** );

/**
 * Obtenir clef
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur enum de la clef
 */
NMotClefReponseHTTP NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirClef( const NElementReponseHTTP * );

/**
 * Obtenir valeur clef
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur texte de la clef
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirClef2( const NElementReponseHTTP * );

/**
 * Obtenir valeur
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirValeur( const NElementReponseHTTP * );

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_NELEMENTREPONSEHTTP_PROTECT
