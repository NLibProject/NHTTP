#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_USINE_NTYPEREPONSEUSINE_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_USINE_NTYPEREPONSEUSINE_PROTECT

// -----------------------------------------------------------------------
// enum NHTTP::Commun::HTTP::Traitement::Reponse::Usine::NTypeReponseUsine
// -----------------------------------------------------------------------

typedef enum NTypeReponseUsine
{
	NTYPE_REPONSE_USINE_404,
	NTYPE_REPONSE_USINE_TEA_POT,
	NTYPE_REPONSE_USINE_ABOUT,
	NTYPE_REPONSE_USINE_BAD_REQUEST,

	NTYPES_REPONSE_USINE
} NTypeReponseUsine;

/**
 * Obtenir la reponse
 *
 * @param type
 * 		Le type de reponse
 *
 * @return la reponse
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_Usine_NTypeReponseUsine_ObtenirReponse( NTypeReponseUsine type );

#ifdef NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_USINE_NTYPEREPONSEUSINE_INTERNE
const char *NTypeReponseUsine404 =
{
	"<!DOCTYPE html>\n"
	"<html>\n"
	"<head>\n"
	"	<title>NRobot Erreur - 404</title>\n"
	"	<style>\n"
	"		body { \n"
	"				background-color: #C9E4FF;\n"
	"				margin: 20px 20px;\n"
	"		}\n"
	"	</style>\n"
	"</head>\n"
	"\n"
	"<body>\n"
	"	<header>\n"
	"		\n"
	"	</header>\n"
	"	\n"
	"	<p>Robot n'a pas trouv&eacute; ce fichier.</p>\n"
	"	<hr />\n"
	"	\n"
	"	<footer>\n"
	"		<p><a href=\"http://nproject.ddns.net/\">Accueil</a> - NProject (LS)</p>\n"
	"	</footer>\n"
	"</body>\n"
	"\n"
	"</html>\n"
};

// I'm a teapot
const char *NTypeReponseUsineTeaPot =
{
	"<!DOCTYPE html>\n"
	"<html>\n"
	"<head>\n"
	"	<title>Go make some coffee elsewhere</title>\n"
	"	<style>\n"
	"		body { \n"
	"				background-color: #ABCDEF;\n"
	"				margin: 20px 20px;\n"
	"		}\n"
	"	</style>\n"
	"</head>\n"
	"\n"
	"<body>\n"
	"<img src=\"http://hottopic.scene7.com/is/image/HotTopic/11055437_hi\" alt=\"I'm a teapot\" />"
	"</body>\n"
	"\n"
	"</html>\n"
};

// Description (about)
const char *NTypeReponseUsineAbout =
{
	"<!DOCTYPE html>\n"
	"<html>\n"
	"<head>\n"
	"	<title>Pr&eacute;sentation</title>\n"
	"	<style>\n"
	"		body { \n"
	"				background-color: #C9E4FF;\n"
	"				margin: 20px 20px;\n"
	"		}\n"
	"	</style>\n"
	"</head>\n"
	"\n"
	"<body>\n"
	"	<header>\n"
	"		<h1>NServeur par SOARES Lucas</h1>\n"
	"	</header>\n"
	"	\n"
	"	\n"
	"	<hr />\n"
	"	\n"
	"	<footer>\n"
	"		<p><a href=\"http://nproject.ddns.net/\">NProject</a> (LS)</p>\n"
	"	</footer>\n"
	"</body>\n"
	"\n"
	"</html>\n"
};

// Defaut
const char *NTypeReponseUsineBadRequest =
{
	"<!DOCTYPE html>\n"
	"<html>\n"
	"<head>\n"
	"<title>Quoi?</title>"
	"</head>\n"
	"\n"
	"<body>\n"
	"<p>Qu'est ce que tu me racontes?</p>"
	"</body>\n"
	"\n"
	"</html>\n"
};
#endif // NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_USINE_NTYPEREPONSEUSINE_INTERNE

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_USINE_NTYPEREPONSEUSINE_PROTECT

