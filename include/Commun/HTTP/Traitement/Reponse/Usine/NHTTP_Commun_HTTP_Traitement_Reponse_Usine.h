#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_USINE_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_USINE_PROTECT

// ---------------------------------------------------------
// namespace NHTTP::Commun::HTTP::Traitement::Reponse::Usine
// ---------------------------------------------------------

/**
 * Creer packet reponse textuelle simple
 *
 * @param message
 * 		Le message
 * @param codeHTTP
 * 		Le code http
 * @param identifiant
 * 		L'identifiant de la requete associee
 *
 * @return le packet
 */
__ALLOC NReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( const char *message,
	NHTTPCode codeHTTP,
	NU32 identifiant ) ;

// enum NHTTP::Commun::HTTP::Traitement::Reponse::Usine::NTypeReponseUsine
#include "NHTTP_Commun_HTTP_Traitement_Reponse_Usine_NTypeReponseUsine.h"

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_USINE_PROTECT
