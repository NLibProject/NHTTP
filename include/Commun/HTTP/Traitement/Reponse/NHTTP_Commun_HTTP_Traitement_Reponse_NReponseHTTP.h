#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_NREPONSEHTTP_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_NREPONSEHTTP_PROTECT

// -------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Reponse::NReponseHTTP
// -------------------------------------------------------------

typedef struct NReponseHTTP
{
	// Code reponse
	NHTTPCode m_code;

	// Liste attributs reponse
	NListe *m_attribut;

	// Fichier reponse
	NFichierHTTP *m_fichier;

	// Identifiant requete associe
	NU32 m_identifiantRequete;

	// Version
	char *m_version;
} NReponseHTTP;

/**
 * Construire la reponse
 *
 * @param code
 * 		Le code reponse
 * @param identifiantRequete
 * 		L'identifiant de la requete associee
 *
 * @return l'instance de la reponse
 */
__ALLOC NReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire( NHTTPCode code,
	NU32 identifiantRequete );

/**
 * Construire reponse depuis reponse texte
 *
 * @param reponse
 * 		La reponse recue
 *
 * @return l'instance de la reponse
 */
__ALLOC NReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire2( const char *reponse );

/**
 * Detruire la reponse
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( NReponseHTTP ** );

/**
 * Definir le fichier
 *
 * @param this
 * 		Cette instance
 * @param fichier
 * 		Le fichier de reponse
 */
void NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier( NReponseHTTP *,
	__WILLBEOWNED NFichierHTTP *fichier );

/**
 * Definir le fichier en tant que texte
 *
 * @param this
 * 		Cette instance
 * @param texte
 * 		Le texte
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier2( NReponseHTTP *,
	const char *texte );

/**
 * Definir le fichier en tant que fichier binaire
 *
 * @param this
 * 		Cette instance
 * @param fichier
 * 		Le fichier
 * @param lien
 * 		Le lien vers le fichier
 * @param typeFlux
 * 		Le type de flux
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier3( NReponseHTTP *,
	__WILLBEOWNED NFichierBinaire *fichier,
	const char *lien,
	NTypeFlux typeFlux );

/**
 * Ajouter un element a la liste des attributs
 *
 * @param this
 * 		Cette instance
 * @param motClefReponse
 * 		Le type de mot clef
 * @param valeur
 * 		La valeur a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_AjouterAttribut( NReponseHTTP *,
	NMotClefReponseHTTP motClefReponse,
	const char *valeur );

/**
 * Obtenir code
 *
 * @param this
 * 		Cette instance
 *
 * @return le code reponse HTTP
 */
NHTTPCode NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCode( const NReponseHTTP * );

/**
 * Obtenir attribut depuis clef
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef pour laquelle on cherche la valeur
 *
 * @return la valeur associee a clef (NULL si inexistante)
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut( const NReponseHTTP *,
	NMotClefReponseHTTP clef );

/**
 * Obtenir attribut depuis clef
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef pour laquelle on cherche la valeur
 * @param index
 * 		L'index de l'attribut (si plusieurs)
 *
 * @return la valeur associee a clef (NULL si inexistante)
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut2( const NReponseHTTP *,
	NMotClefReponseHTTP clef,
	NU32 index );

/**
 * Obtenir donnee
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees
 */
__ALLOC char *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( const NReponseHTTP * );

/**
 * Construire packet depuis reponse (NPacketPersonnalise contenant NPacketHTTP)
 *
 * @param this
 * 		Cette instance
 *
 * @return le packet
 */
__ALLOC NPacketPersonnalise *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_CreerPacket( NReponseHTTP * );

/**
 * Loguer reponse
 *
 * @param this
 * 		Cette instance
 * @param cacheLog
 * 		Le cache log
 * @param ipClient
 * 		L'ip du client
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Loguer( const NReponseHTTP *,
	void *cacheLog,
	const char *ipClient );

/**
 * Obtenir identifiant requete associe
 *
 * @param this
 * 		Cette instance
 *
 * @return l'identifiant requete associe
 */
NU32 NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirIdentifiant( const NReponseHTTP * );

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_NREPONSEHTTP_PROTECT
