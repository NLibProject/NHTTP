#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_NREQUETEHTTP_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_NREQUETEHTTP_PROTECT

// -------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Requete::NRequeteHTTP
// -------------------------------------------------------------

typedef struct NRequeteHTTP
{
	// Type de requete
	NTypeRequeteHTTP m_type;

	// Version http
	char *m_versionHTTP;

	// Arguments GET
	NListeArgumentGET *m_argumentGET;

	// Element requete traite
	char *m_element;

	// Element requete demande initialement
	char *m_elementInitial;

	// Donnees de la requete (securisee par un \0)
	char *m_data;

	// Taille des donnees de la requete
	NU32 m_tailleData;

	// Liste des elements requete (NElementRequeteHTTP)
	NListe *m_attribut;

	// Identifiant requete
	NU32 m_identifiant;

	// Client ayant effectue la requete (uniquement si serveur)
	const NClientServeur *m_client;
} NRequeteHTTP;

/**
 * Construire la requete HTTP
 *
 * @param requete
 * 		La requete recue
 * @param client
 * 		Le client ayant effectue la requete
 * @param identifiant
 * 		L'identifiant associe
 *
 * @return l'instance de la requete
 */
__ALLOC NRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire( const char *requete,
	const NClientServeur *client,
	NU32 identifiant );

/**
 * Construire une requete vide
 *
 * @param type
 * 		Le type de requete
 * @param element
 * 		L'element a obtenir
 *
 * @return l'instance de la requete
 */
__ALLOC NRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTypeRequeteHTTP type,
	const char *element );

/**
 * Construire une requete depuis une autre
 *
 * @param requete
 * 		La requete a dupliquer
 *
 * @return l'instance de la requete
 */
__ALLOC NRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire3( const NRequeteHTTP *requete );

/**
 * Detruire la requete
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( NRequeteHTTP** );

/**
 * Obtenir type requete
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de la requete
 */
NTypeRequeteHTTP NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( const NRequeteHTTP* );

/**
 * Obtenir liste elements
 *
 * @param this
 * 		Cette instance
 *
 * @return la liste des elements
 */
const NListe *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirListeElement( const NRequeteHTTP* );

/**
 * Obtenir attribut
 *
 * @param this
 * 		Cette instance
 * @param type
 * 		Le type d'attribut
 *
 * @return l'attribut (NULL si introuvable)
 */
const NElementRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirAttribut( const NRequeteHTTP*,
	NMotClefRequeteHTTP type );

/**
 * Obtenir element demande
 *
 * @param this
 * 		Cette instance
 *
 * @return l'element demande
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElement( const NRequeteHTTP* );

/**
 * Obtenir element demande avant traitement chaine
 *
 * @param this
 * 		Cette instance
 *
 * @return l'element demande avant traitement
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( const NRequeteHTTP* );

/**
 * Obtenir data
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirData( const NRequeteHTTP* );

/**
 * Obtenir la taille des donnees
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille des donnees
 */
NU32 NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirTailleData( const NRequeteHTTP* );

/**
 * Loguer requete
 *
 * @param this
 * 		Cette instance
 * @param cacheLog
 * 		Le cache log
 * @param ipClient
 * 		L'ip du client
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Loguer( const NRequeteHTTP*,
	void *cacheLog,
	const char *ipClient );

/**
 * Obtenir identifiant
 *
 * @param this
 * 		Cette instance
 *
 * @return l'identifiant
 */
NU32 NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirIdentifiant( const NRequeteHTTP* );

/**
 * Ajouter data
 *
 * @param this
 * 		Cette instance
 * @param data
 * 		Les donnees a ajouter
 * @param tailleData
 * 		La taille des donnees a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( NRequeteHTTP*,
	const char *data,
	NU32 tailleData );

/**
 * Ajouter un attribut
 *
 * @param this
 * 		Cette instance
 * @param typeAttribut
 * 		Le type d'attribut
 * @param valeurAttribut
 * 		La valeur d'attribut
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( NRequeteHTTP*,
	NMotClefRequeteHTTP typeAttribut,
	const char *valeurAttribut );

/**
 * Creer le packet (NPacketHTTP*)
 *
 * @param this
 * 		Cette instance
 *
 * @return le packet
 */
__ALLOC NPacketPersonnalise *NHTTP_Commun_Traitement_Requete_NRequeteHTTP_CreerPacket( const NRequeteHTTP* );

/**
 * Supprimer attribut
 *
 * @param this
 * 		Cette instance
 * @param typeAttribut
 * 		Le type d'attribut
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_Traitement_Requete_NRequeteHTTP_SupprimerAttribut( NRequeteHTTP*,
	NMotClefRequeteHTTP typeAttribut );

/**
 * Remplacer attribut
 *
 * @param this
 * 		Cette instance
 * @param typeAttribut
 * 		Le type d'attribut
 * @param valeurAttribut
 * 		La nouvelle valeur de l'attribut
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_Traitement_Requete_NRequeteHTTP_RemplacerAttribut( NRequeteHTTP*,
	NMotClefRequeteHTTP typeAttribut,
	const char *valeurAttribut );

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_NREQUETEHTTP_PROTECT
