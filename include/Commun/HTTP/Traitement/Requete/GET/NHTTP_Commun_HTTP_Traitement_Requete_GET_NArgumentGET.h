#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_GET_NARGUMENTGET_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_GET_NARGUMENTGET_PROTECT

// ------------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Requete::GET::NArgumentGET
// ------------------------------------------------------------------

typedef struct NArgumentGET
{
	// Nom
	char *m_nom;

	// Valeur
	char *m_valeur;
} NArgumentGET;

/**
 * Construire argument
 *
 * @param nom
 * 		Le nom
 * @param valeur
 * 		La valeur
 *
 * @return l'instance de l'argument
 */
__ALLOC NArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Construire( const char *nom,
	const char *valeur );

/**
 * Dupliquer argument
 *
 * @param argument
 * 		L'argument
 *
 * @return l'instance de l'argument
 */
__ALLOC NArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Construire2( const NArgumentGET *argument );

/**
 * Detruire l'argument
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Detruire( NArgumentGET** );

/**
 * Obtenir nom
 *
 * @param this
 * 		Cette instance
 *
 * @return le nom
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_ObtenirNom( const NArgumentGET* );

/**
 * Obtenir la valeur
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_ObtenirValeur( const NArgumentGET* );

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_GET_NARGUMENTGET_PROTECT
