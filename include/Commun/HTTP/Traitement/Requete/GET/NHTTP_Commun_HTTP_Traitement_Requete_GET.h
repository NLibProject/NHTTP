#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_GET_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_GET_PROTECT

// -------------------------------------------------
// namespace NHTTP::Commun::Traitement::Requete::GET
// -------------------------------------------------

// struct NHTTP::Commun::Traitement::Requete::GET::NArgumentGET
#include "NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET.h"

// struct NHTTP::Commun::Traitement::Requete::GET::NListeArgumentGET
#include "NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET.h"

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_GET_PROTECT
