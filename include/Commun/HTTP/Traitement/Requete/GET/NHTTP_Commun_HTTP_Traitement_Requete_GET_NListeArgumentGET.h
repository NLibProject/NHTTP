#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_GET_NLISTEARGUMENTGET_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_GET_NLISTEARGUMENTGET_PROTECT

// -----------------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Requete::GET::NListeArgumentGET
// -----------------------------------------------------------------------

typedef struct NListeArgumentGET
{
	// Liste d'arguments (NListe<NArgumentGET*>)
	NListe *m_argument;
} NListeArgumentGET;

/**
 * Construire la liste d'arguments
 *
 * @param element
 * 		L'element demande
 *
 * @return l'instance
 */
__ALLOC NListeArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Construire( const char *element );

/**
 * Construire liste d'arguments vide
 *
 * @return l'instance
 */
__ALLOC NListeArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Construire2( void );

/**
 * Dupliquer liste d'arguments
 *
 * @param listeArgument
 * 		La liste d'arguments
 *
 * @return l'instance
 */
__ALLOC NListeArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Construire3( const NListeArgumentGET *listeArgumentGET );

/**
 * Detruire la liste d'arguments
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( NListeArgumentGET** );

/**
 * Obtenir le nombre d'arguments
 *
 * @param this
 * 		Cette instance
 *
 * @return le nombre d'arguments
 */
NU32 NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_ObtenirNombreArgument( const NListeArgumentGET* );

/**
 * Obtenir un argument
 *
 * @param this
 * 		Cette instance
 * @param index
 * 		L'index de l'argument
 *
 * @return l'argument
 */
const NArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_ObtenirArgument( const NListeArgumentGET*,
	NU32 index );

/**
 * Ajouter argument
 *
 * @param this
 * 		Cette instance
 * @param nom
 * 		Le nom de l'argument
 * @param valeur
 * 		La valeur de l'argument
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_AjouterArgument( NListeArgumentGET*,
	const char *nom,
	const char *valeur );

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_GET_NLISTEARGUMENTGET_PROTECT
