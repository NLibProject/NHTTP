#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_NELEMENTREQUETEHTTP_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_NELEMENTREQUETEHTTP_PROTECT

// --------------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Requete::NElementRequeteHTTP
// --------------------------------------------------------------------

typedef struct NElementRequeteHTTP
{
	// Type d'element
	NMotClefRequeteHTTP m_type;

	// Valeur
	char *m_valeur;
} NElementRequeteHTTP;

/**
 * Construire element
 *
 * @param motClef
 * 		Le mot clef de l'element
 * @param valeur
 * 		La valeur
 *
 * @return l'instance de l'element
 */
__ALLOC NElementRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire( const char *motClef,
	const char *valeur );

/**
 * Construire element depuis ligne header
 *
 * @param ligne
 * 		La ligne a parser
 *
 * @return l'instance de l'element
 */
__ALLOC NElementRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire2( const char *ligne );

/**
 * Construire element
 *
 * @param type
 * 		Le type d'element
 * @param valeur
 * 		La valeur
 *
 * @return l'instance de l'element
 */
__ALLOC NElementRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire3( NMotClefRequeteHTTP type,
	const char *valeur );

/**
 * Detruire l'element
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Detruire( NElementRequeteHTTP ** );

/**
 * Obtenir type mot clef
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de mot clef
 */
NMotClefRequeteHTTP NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef( const NElementRequeteHTTP * );

/**
 * Obtenir mot clef texte
 *
 * @param this
 * 		Cette instance
 *
 * @return le mot clef texte
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef2( const NElementRequeteHTTP * );

/**
 * Obtenir valeur
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirValeur( const NElementRequeteHTTP * );

/**
 * Remplacer valeur
 *
 * @param this
 * 		Cette instance
 * @param valeur
 * 		La valeur a remplacer
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_RemplacerValeur( NElementRequeteHTTP*,
	const char *valeur );

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_NELEMENTREQUETEHTTP_PROTECT
