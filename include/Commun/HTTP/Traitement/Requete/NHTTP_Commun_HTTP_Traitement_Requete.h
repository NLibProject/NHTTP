#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_PROTECT

// --------------------------------------------------
// namespace NHTTP::Commun::HTTP::Traitement::Requete
// --------------------------------------------------

// struct NHTTP::Commun::Traitement::Requete::NElementRequeteHTTP
#include "NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP.h"

// namespace NHTTP::Commun::Traitement::Requete::GET
#include "GET/NHTTP_Commun_HTTP_Traitement_Requete_GET.h"

// struct NHTTP::Commun::Traitement::Requete::NRequeteHTTP
#include "NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP.h"

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_REQUETE_PROTECT
