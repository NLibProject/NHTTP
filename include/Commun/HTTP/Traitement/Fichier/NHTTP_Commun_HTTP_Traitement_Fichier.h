#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_FICHIER_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_FICHIER_PROTECT

// --------------------------------------------------
// namespace NHTTP::Commun::HTTP::Traitement::Fichier
// --------------------------------------------------

// enum NHTTP::Commun::HTTP::Traitement::Fichier::NTypeFichierHTTP
#include "NHTTP_Commun_HTTP_Traitement_Fichier_NTypeFichierHTTP.h"

// struct NHTTP::Commun::HTTP::Traitement::Fichier::NFichierHTTP
#include "NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP.h"

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_FICHIER_PROTECT
