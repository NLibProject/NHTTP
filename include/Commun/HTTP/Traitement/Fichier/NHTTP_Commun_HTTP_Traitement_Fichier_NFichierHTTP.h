#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_FICHIER_NFICHIERHTTP_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_FICHIER_NFICHIERHTTP_PROTECT

// -------------------------------------------------------
// struct NHTTP::Commun::Traitement::Reponse::NFichierHTTP
// -------------------------------------------------------

typedef struct NFichierHTTP
{
	// Type de fichier
	NTypeFichierHTTP m_typeData;

	// Type de flux
	NTypeFlux m_typeFlux;

	// Lien fichier
	char *m_lienFichier;

	// Donnee (char*/NFichierBinaire*)
	void *m_data;
} NFichierHTTP;

/**
 * Construire le fichier de reponse HTTP
 *
 * @param typeData
 * 		Le type de donnees
 * @param typeFlux
 * 		Le type de flux
 * @param data
 * 		La donnees
 * @param lienFichier
 * 		Le lien vers le fichier si type de flux fichier (NULL sinon)
 *
 * @return l'instance du fichier
 */
__ALLOC NFichierHTTP *NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Construire( NTypeFichierHTTP typeData,
	NTypeFlux typeFlux,
	__WILLBEOWNED void *data,
	const char *lienFichier );

/**
 * Construire a partir d'un fichier de reponse
 *
 * @param src
 * 		La source
 *
 * @return l'instance du fichier
 */
__ALLOC NFichierHTTP *NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Construire2( const NFichierHTTP *src );
/**
 * Detruire fichier
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Detruire( NFichierHTTP ** );

/**
 * Obtenir type de donnees
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de donnees
 */
NTypeFichierHTTP NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTypeData( const NFichierHTTP * );

/**
 * Obtenir type flux
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de flux
 */
NTypeFlux NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTypeFlux( const NFichierHTTP * );

/**
 * Obtenir data
 *
 * @param this
 * 		Cette instance
 *
 * @return la donnees
 */
const void *NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirData( const NFichierHTTP * );

/**
 * Obtenir lien fichier
 *
 * @param this
 * 		Cette instance
 *
 * @return le lien vers le fichier
 */
const char *NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirLienFichier( const NFichierHTTP * );

/**
 * Obtenir la taille de ce que le fichier contient (taille du texte ou du fichier)
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille
 */
NU64 NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTaille( const NFichierHTTP * );

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_FICHIER_NFICHIERHTTP_PROTECT
