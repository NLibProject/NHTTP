#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_FICHIER_NTYPEFICHIERHTTP_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_FICHIER_NTYPEFICHIERHTTP_PROTECT

// ---------------------------------------------------------------
// enum NHTTP::Commun::HTTP::Traitement::Fichier::NTypeFichierHTTP
// ---------------------------------------------------------------

typedef enum NTypeReponseHTTP
{
	NTYPE_FICHIER_HTTP_FICHIER,
	NTYPE_FICHIER_HTTP_TEXTE,

	NTYPES_FICHIER_HTTP
} NTypeFichierHTTP;

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_FICHIER_NTYPEFICHIERHTTP_PROTECT
