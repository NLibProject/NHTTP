#ifndef NHTTP_COMMUN_HTTP_TRAITEMENT_PROTECT
#define NHTTP_COMMUN_HTTP_TRAITEMENT_PROTECT

// -----------------------------------------
// namespace NHTTP::Commun::HTTP::Traitement
// -----------------------------------------

// namespace NHTTP::Commun::HTTP::Traitement::Fichier
#include "Fichier/NHTTP_Commun_HTTP_Traitement_Fichier.h"

// namespace NHTTP::Commun::HTTP::Traitement::Requete
#include "Requete/NHTTP_Commun_HTTP_Traitement_Requete.h"

// namespace NHTTP::Commun::HTTP::Traitement::Reponse
#include "Reponse/NHTTP_Commun_HTTP_Traitement_Reponse.h"

/**
 * Traiter requete
 *
 * @param requete
 * 		La requete
 * @param client
 * 		Le client ayant effectue la requete
 *
 * @return le resultat du traitement
 */
__ALLOC NReponseHTTP *NHTTP_Commun_HTTP_Traitement_TraiterRequete( const NRequeteHTTP *requete,
	const NClientServeur *client );

// Caractere d'arret dans la requete
#define NHTTP_NOMBRE_CARACTERE_ARRET_TRAITEMENT		2

/**
 * Obtenir caracteres arret traitement
 *
 * @return la liste des caractere stoppant la lecture d'une ligne lors du traitement
 */
const char *NHTTP_Commun_HTTP_Traitement_ObtenirListeCaractereArretTraitement( void );

/**
 * Est caractere de fin de ligne traitement
 *
 * @param c
 * 		Le caractere
 *
 * @return si le caractere appartient aux caracteres de fin de ligne
 */
NBOOL NHTTP_Commun_HTTP_Traitement_EstCaractereFinDeLigne( char c );

#ifdef NHTTP_COMMUN_HTTP_TRAITEMENT_INTERNE
const char NHTTP_CARACTERE_ARRET_TRAITEMENT[ NHTTP_NOMBRE_CARACTERE_ARRET_TRAITEMENT ] = { '\n',
	'\r' };
#endif // NHTTP_COMMUN_HTTP_TRAITEMENT_INTERNE

#endif // !NHTTP_COMMUN_HTTP_TRAITEMENT_PROTECT
