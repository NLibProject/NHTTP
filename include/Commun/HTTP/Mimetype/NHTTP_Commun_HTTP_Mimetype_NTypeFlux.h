#ifndef NHTTP_COMMUN_HTTP_MIMETYPE_NTYPEFLUX_PROTECT
#define NHTTP_COMMUN_HTTP_MIMETYPE_NTYPEFLUX_PROTECT

// ---------------------------------------------
// enum NHTTP::Commun::HTTP::Mimetype::NTypeFlux
// ---------------------------------------------

typedef enum NTypeFlux
{
	NTYPE_FLUX_HTML,
	NTYPE_FLUX_CSS,
	NTYPE_FLUX_TEXTE,
	NTYPE_FLUX_TELECHARGEMENT,
	NTYPE_FLUX_PDF,
	NTYPE_FLUX_IMAGE_PNG,
	NTYPE_FLUX_IMAGE_JPG,
	NTYPE_FLUX_IMAGE_GIF,

	NTYPE_FLUX_YAML,
	NTYPE_FLUX_JSON,

	NTYPES_FLUX
} NTypeFlux;

/**
 * Obtenir le type de flux
 *
 * @param type
 * 		Le type de flux
 *
 * @return le type de flux texte
 */
const char *NHTTP_Commun_HTTP_Mimetype_NTypeFlux_ObtenirType( NTypeFlux type );

#ifdef NHTTP_COMMUN_HTTP_MIMETYPE_NTYPEFLUX_INTERNE
const char NHTTPTypeFluxTexte[ NTYPES_FLUX ][ 32 ] =
{
	"text/html",
	"text/css",
	"text/plain",
	"application/octet-stream",
	"application/pdf",
	"image/png",
	"image/jpeg",
	"image/gif",
	"text/yaml",
	"application/json"
};
#endif // NHTTP_COMMUN_HTTP_MIMETYPE_NTYPEFLUX_INTERNE

#endif // !NHTTP_COMMUN_HTTP_MIMETYPE_NTYPEFLUX_PROTECT
