#ifndef NHTTP_COMMUN_HTTP_MIMETYPE_NTYPENTYPE_EXTENSION_PROTECT
#define NHTTP_COMMUN_HTTP_MIMETYPE_NTYPENTYPE_EXTENSION_PROTECT

// --------------------------------------------------
// enum NHTTP::Commun::HTTP::Mimetype::NTypeExtension
// --------------------------------------------------

typedef enum NTypeExtension
{
	// Page web
	NTYPE_EXTENSION_HTML,
	NTYPE_EXTENSION_HTM,
	NTYPE_EXTENSION_PHP,
	NTYPE_EXTENSION_CSS,

	// Texte
	NTYPE_EXTENSION_TXT,
	NTYPE_EXTENSION_LOG,
	NTYPE_EXTENSION_C,
	NTYPE_EXTENSION_H,
	NTYPE_EXTENSION_CPP,
	NTYPE_EXTENSION_HPP,
	NTYPE_EXTENSION_JAVA,
	NTYPE_EXTENSION_CS,
	NTYPE_EXTENSION_YAML,
	NTYPE_EXTENSION_YML,
	NTYPE_EXTENSION_JSON,

	// PDF
	NTYPE_EXTENSION_PDF,

	// Images
	NTYPE_EXTENSION_PNG,
	NTYPE_EXTENSION_JPG,
	NTYPE_EXTENSION_JPEG,
	NTYPE_EXTENSION_GIF,

	NTYPES_EXTENSION
} NTypeExtension;

/**
 * Obtenir l'extension associee a un type d'extension
 *
 * @param extension
 * 		Le type d'extension
 *
 * @return l'extension
 */
const char *NHTTP_Commun_HTTP_Mimetype_NTypeExtension_ObtenirExtension( NTypeExtension extension );

/**
 * Interpreter extension
 *
 * @param extension
 * 		L'extension a interpreter
 *
 * @return le type d'extension
 */
NTypeExtension NHTTP_Commun_HTTP_Mimetype_NTypeExtension_InterpreterExtension( const char *extension );

#ifdef NHTTP_COMMUN_HTTP_MIMETYPE_NTYPENTYPE_EXTENSION_INTERNE
static const char NTypeExtensionTexte[ NTYPES_EXTENSION ][ 32 ] =
{
	// Page web
	"html",
	"htm",
	"php",
	"css",

	// Texte
	"txt",
	"log",
	"c",
	"h",
	"cpp",
	"hpp",
	"java",
	"cs",
	"yaml",
	"yml",
	"json",

	// PDF
	"pdf",

	// Images
	"png",
	"jpg",
	"jpeg",
	"gif"
};
#endif // NHTTP_COMMUN_HTTP_MIMETYPE_NTYPENTYPE_EXTENSION_INTERNE

#endif // !NHTTP_COMMUN_HTTP_MIMETYPE_NTYPENTYPE_EXTENSION_PROTECT
