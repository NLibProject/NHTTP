#ifndef NHTTP_COMMUN_HTTP_MIMETYPE_PROTECT
#define NHTTP_COMMUN_HTTP_MIMETYPE_PROTECT

// ---------------------------------------
// namespace NHTTP::Commun::HTTP::Mimetype
// ---------------------------------------

// enum NHTTP::Commun::HTTP::Mimetype::NTypeExtension
#include "NHTTP_Commun_HTTP_Mimetype_NTypeExtension.h"

// enum NHTTP::Commun::HTTP::Mimetype::NTypeFlux
#include "NHTTP_Commun_HTTP_Mimetype_NTypeFlux.h"

/**
 * Determiner extension
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return l'extension
 */
NTypeExtension NHTTP_Commun_HTTP_Mimetype_DeterminerExtension( const char *lien );

/**
 * Determiner type de flux
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return le type de flux
 */
NTypeFlux NHTTP_Commun_HTTP_Mimetype_DeterminerTypeFlux( const char *lien );

/**
 * Determiner type de flux
 *
 * @param extension
 * 		L'extension
 *
 * @return le type de flux
 */
NTypeFlux NHTTP_Commun_HTTP_Mimetype_DeterminerTypeFlux2( NTypeExtension extension );

#endif // !NHTTP_COMMUN_HTTP_MIMETYPE_PROTECT

