#ifndef NHTTP_COMMUN_PROTECT
#define NHTTP_COMMUN_PROTECT

// -----------------------
// namespace NHTTP::Commun
// -----------------------

// Version HTTP
#define NHTTP_COMMUN_HTTP_VERSION_PROTOCOLE_HTTP	"HTTP/1.1"

// namespace NHTTP::Commun::HTTP
#include "HTTP/NHTTP_Commun_HTTP.h"

// namespace NHTTP::Commun::Packet
#include "Packet/NHTTP_Commun_Packet.h"

// namespace NHTTP::Commun::Outil
#include "Outil/NHTTP_Commun_Outil.h"

/**
 * Download a file
 *
 * @param hostname
 * 		The hostname
 * @param port
 * 		The port
 * @param remoteFilePath
 * 		The remote file path
 * @param localFileName
 * 		The local filename
 * @param username
 * 		The username (basic auth)
 * @param password
 * 		The password (basic auth)
 * @param connectionTimeout
 * 		The connection timeout
 *
 * @return if the operation succeeded
 */
NBOOL NHTTP_Commun_DownloadFile( const char *hostname,
	NU32 port,
	const char *remoteFilePath,
	const char *localFileName,
	const char *username,
	const char *password,
	NU32 connectionTimeout );

#endif // !NHTTP_COMMUN_PROTECT
