#ifndef NHTTP_COMMUN_OUTIL_PROTECT
#define NHTTP_COMMUN_OUTIL_PROTECT

// ------------------------------
// namespace NHTTP::Commun::Outil
// ------------------------------

/**
 * Methode d'envoi d'un packet
 *
 * @param packet
 * 		Le packet a envoyer
 * @param socket
 * 		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Commun_Outil_CallbackMethodeEnvoiPacket( const NPacketPersonnalise *packet,
	SOCKET socket );

#endif // !NHTTP_COMMUN_OUTIL_PROTECT

