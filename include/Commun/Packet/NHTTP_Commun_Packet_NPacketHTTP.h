#ifndef NHTTP_COMMUN_PACKET_NPACKETHTTP_PROTECT
#define NHTTP_COMMUN_PACKET_NPACKETHTTP_PROTECT

// -----------------------------------------
// struct NHTTP::Commun::Packet::NPacketHTTP
// -----------------------------------------

typedef struct NPacketHTTP
{
	// Donnees header
	char *m_header;

	// Fichier a envoyer (NFichierHTTP*)
	void *m_fichier;
} NPacketHTTP;

/**
 * Construire packet http
 *
 * @return le packet vide
 */
__ALLOC NPacketHTTP *NHTTP_Commun_Packet_NPacketHTTP_Construire( void );

/**
 * Construire copie packet http
 *
 * @param src
 * 		Le packet a copier
 *
 * @return une copie de src
 */
__ALLOC NPacketHTTP *NHTTP_Commun_Packet_NPacketHTTP_Construire2( const NPacketHTTP *src );

/**
 * Detruire le packet
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_Packet_NPacketHTTP_Detruire( NPacketHTTP ** );

/**
 * Ajouter des donnees a l'header
 *
 * @param this
 * 		Cette instance
 * @param texte
 * 		Le texte a ajouter a l'header
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( NPacketHTTP *,
	const char *texte );

/**
 * Definir le lien du fichier
 *
 * @param this
 * 		Cette instance
 * @param fichierReponse
 * 		Les details du fichier a envoyer (NFichierReponseHTTP*)
 *
 * @return si l'operation s'est bien passee
 */
void NHTTP_Commun_Packet_NPacketHTTP_DefinirFichier( NPacketHTTP *,
	__WILLBEOWNED void *fichierReponse );

/**
 * Obtenir header
 *
 * @param this
 * 		Cette instance
 *
 * @return l'header
 */
const char *NHTTP_Commun_Packet_NPacketHTTP_ObtenirHeader( const NPacketHTTP * );

/**
 * Obtenir la taille de l'header
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille de l'header
 */
NU32 NHTTP_Commun_HTTP_Packet_NPacketHTTP_ObtenirTailleHeader( const NPacketHTTP * );

/**
 * Obtenir le fichier (NFichierReponseHTTP*)
 *
 * @param this
 * 		Cette instance
 *
 * @return le fichier
 */
const void *NHTTP_Commun_HTTP_Packet_NPacketHTTP_ObtenirFichier( const NPacketHTTP * );

#endif // !NHTTP_COMMUN_PACKET_NPACKETHTTP_PROTECT

