#include "../../../include/NHTTP.h"

// -----------------------------------------
// struct NHTTP::Serveur::Log::NCacheLogHTTP
// -----------------------------------------

/**
 * Trouver un nom disponible pour archiver (privee)
 *
 * @param this
 * 		Cette instance
 * @param baseNom
 *		La base du nom des fichiers de log
 *
 * @return le nom trouve
 */
__PRIVATE __ALLOC char *NHTTP_Serveur_Log_NCacheLogHTTP_TrouverNomLibreArchivage( NCacheLogHTTP *this,
	const char *baseNom )
{
	// Lien
	char lien[ MAX_PATH ];

	// Sortie
	__OUTPUT char *out;

	// Iterateur
	NU32 i = 0;

	// Chercher
	do
	{
		// Composer
		snprintf( lien,
			MAX_PATH,
			"%s/%s.%d",
			this->m_repertoireArchivage,
			baseNom,
			i++ );
	} while( NLib_Fichier_Operation_EstExiste( lien ) );

	// Dupliquer lien
	if( !( out = NLib_Chaine_Dupliquer( lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Archiver (privee)
 *
 * @param this
 * 		Cette instance
 * @param type
 * 		Le type de fichier a archiver
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE NBOOL NHTTP_Serveur_Log_NCacheLogHTTP_ArchiverLog( NCacheLogHTTP *this,
	NTypeEntreeCacheLogHTTP type )
{
	// Nom libre pour archiver
	char *nomLibre;

	// Nom base
	char *nomBase;

	// Resultat
	__OUTPUT NBOOL resultat;

	// Verifier
	if( this->m_repertoireArchivage == NULL )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Creer le repertoire d'archivage
	NLib_Module_Repertoire_CreerRepertoire( this->m_repertoireArchivage );

	// Analyser
	switch( type )
	{
		case NTYPE_ENTREE_CACHE_LOG_HTTP_EVENEMENT:
			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &this->m_fichierLogEvenement );

			// Enregistrer nom base
			nomBase = this->m_lienFichierEvenement;
			break;

		case NTYPE_ENTREE_CACHE_LOG_HTTP_PACKET:
			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &this->m_fichierLogPacket );

			// Enregistrer nom base
			nomBase = this->m_lienFichierPacket;
			break;

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Quitter
			return NFALSE;
	}

	// Trouver nom libre
	if( !( nomLibre = NHTTP_Serveur_Log_NCacheLogHTTP_TrouverNomLibreArchivage( this,
		nomBase ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Quitter
		return NFALSE;
	}

	// Archiver
	resultat = NLib_Fichier_Operation_CopierFichier( nomBase,
		nomLibre );

	// Liberer
	NFREE( nomLibre );

	// Supprimer fichier
	NLib_Fichier_Operation_SupprimerFichier( nomBase );

	// OK?
	return resultat;
}

/**
 * Gerer les quotas de stockage des fichiers (privee)
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE NBOOL NHTTP_Serveur_Log_NCacheLogHTTP_GererFichier( NCacheLogHTTP *this )
{
	// Taille fichiers
	NU64 tailleFichierEvenement = 0,
		tailleFichierPacket = 0;

	// Archiver fichier si trop volumineux
	if( this->m_tailleMaximaleFichier != 0 )
	{
		// Obtenir taille fichier evenement
		if( this->m_fichierLogEvenement != NULL )
			tailleFichierEvenement = (NU64)NLib_Fichier_Operation_ObtenirTaille( NLib_Fichier_NFichierTexte_ObtenirFichier( this->m_fichierLogEvenement ) );

		// Obtenir taille fichier packet
		if( this->m_fichierLogPacket != NULL )
			tailleFichierPacket = (NU64)NLib_Fichier_Operation_ObtenirTaille( NLib_Fichier_NFichierTexte_ObtenirFichier( this->m_fichierLogPacket ) );

		// Fichier evenement
		if( tailleFichierEvenement >= this->m_tailleMaximaleFichier )
			// Archiver
			NHTTP_Serveur_Log_NCacheLogHTTP_ArchiverLog( this,
				NTYPE_ENTREE_CACHE_LOG_HTTP_EVENEMENT );

		// Fichier packet
		if( tailleFichierPacket >= this->m_tailleMaximaleFichier )
			// Archiver
			NHTTP_Serveur_Log_NCacheLogHTTP_ArchiverLog( this,
				NTYPE_ENTREE_CACHE_LOG_HTTP_PACKET );
	}

	// Ouvrir fichier evenements
	if( this->m_lienFichierEvenement != NULL
		&& this->m_fichierLogEvenement == NULL )
		// Ouvrir fichier
		if( !( this->m_fichierLogEvenement = NLib_Fichier_NFichierTexte_ConstruireEcriture( this->m_lienFichierEvenement,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

			// Quitter
			return NFALSE;
		}

	// Ouvrir fichier evenements
	if( this->m_lienFichierPacket != NULL
		&& this->m_fichierLogPacket == NULL )
		// Ouvrir fichier
		if( !( this->m_fichierLogPacket = NLib_Fichier_NFichierTexte_ConstruireEcriture( this->m_lienFichierPacket,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

			// Quitter
			return NFALSE;
		}

	// OK
	return NTRUE;
}

/**
 * Loguer une entree (privee)
 *
 * @param this
 * 		Cette instance
 * @param entree
 * 		L'entree a loguer
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE NBOOL NHTTP_Serveur_Log_NCacheLogHTTP_Loguer( NCacheLogHTTP *this,
	const NEntreeCacheLogHTTP *entree )
{
	// Chaine a loguer
	char *log;

	// Sortie
	__OUTPUT NBOOL resultat = NFALSE;

	// Generer
	if( !( log = NHTTP_Serveur_Log_NEntreeCacheLogHTTP_GenererSortie( entree ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Quitter
		return NFALSE;
	}

	// Inscrire
	switch( NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirType( entree ) )
	{
		// Evenement
		case NTYPE_ENTREE_CACHE_LOG_HTTP_EVENEMENT:
			// Verifier si le fichier est ouvert
			if( this->m_fichierLogEvenement != NULL )
				// Inscrire
				resultat = NLib_Fichier_NFichierTexte_Ecrire3( this->m_fichierLogEvenement,
					log );
			break;

			// Packet
		case NTYPE_ENTREE_CACHE_LOG_HTTP_PACKET:
			// Verifier si le fichier est ouvert
			if( this->m_fichierLogPacket != NULL )
				// Inscrire
				resultat = NLib_Fichier_NFichierTexte_Ecrire3( this->m_fichierLogPacket,
					log );
			break;

		default:
			resultat = NFALSE;
			break;
	}

	// Liberer
	NFREE( log );

	// OK?
	return resultat;
}

/**
 * Update cache (privee)
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE __THREAD NBOOL NHTTP_Serveur_Log_NCacheLogHTTP_UpdateCache( NCacheLogHTTP *this )
{
	// Entree cache
	const NEntreeCacheLogHTTP *entree;

	// Update
	do
	{
		// Lock
		NLib_Memoire_NListe_ActiverProtection( this->m_entree );

		// Mettre a jour les fichiers de log
		NHTTP_Serveur_Log_NCacheLogHTTP_GererFichier( this );

		// On a des entrees?
		if( NLib_Memoire_NListe_ObtenirNombre( this->m_entree ) > 0 )
		{
			// Recuperer la premiere entree (FIFO)
			if( !( entree = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_entree,
				0 ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_UNKNOWN );

				// Unlock
				NLib_Memoire_NListe_DesactiverProtection( this->m_entree );

				// Ignorer
				continue;
			}

			// Loguer l'entree
			NHTTP_Serveur_Log_NCacheLogHTTP_Loguer( this,
				entree );

			// Supprimer l'entree
			NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_entree,
				0 );
		}
		// Unlock
		NLib_Memoire_NListe_DesactiverProtection( this->m_entree );

		// Attendre
		NLib_Temps_Attendre( 1 );
	} while( this->m_estContinuer );

	// OK
	return NTRUE;
}

/**
 * Construire le cache
 *
 * @param lienFichierEvenement
 * 		Le lien vers le fichier de log des evenements
 * @param lienFichierPacket
 *		Le lien vers le fichier de log des packets
 * @param tailleMaximaleFichier
 * 		La taille maximale avant archivage
 * @param repertoireArchivage
 * 		Le repertoire d'archivage des logs
 *
 * @return l'instance du cache
 */
__ALLOC NCacheLogHTTP *NHTTP_Serveur_Log_NCacheLogHTTP_Construire( const char *lienFichierEvenement,
	const char *lienFichierPacket,
	NU32 tailleMaximaleFichier,
	const char *repertoireArchivage )
{
	// Sortie
	__OUTPUT NCacheLogHTTP *out;

	// Verifier les parametres
	if( tailleMaximaleFichier != 0
		&& repertoireArchivage == NULL )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NCacheLogHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire liste elements
	if( !( out->m_entree = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NHTTP_Serveur_Log_NEntreeCacheLogHTTP_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Lien evenement present?
	if( lienFichierEvenement != NULL )
		// Dupliquer
		if( !( out->m_lienFichierEvenement = NLib_Chaine_Dupliquer( lienFichierEvenement ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NLib_Memoire_NListe_Detruire( &out->m_entree );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Lien packet present?
	if( lienFichierPacket != NULL )
		// Dupliquer
		if( !( out->m_lienFichierPacket = NLib_Chaine_Dupliquer( lienFichierPacket ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( out->m_lienFichierEvenement );
			NLib_Memoire_NListe_Detruire( &out->m_entree );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Lien vers archivage?
	if( repertoireArchivage )
		// Dupliquer repertoire
		if( !( out->m_repertoireArchivage = NLib_Chaine_Dupliquer( repertoireArchivage ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( out->m_lienFichierPacket );
			NFREE( out->m_lienFichierEvenement );
			NLib_Memoire_NListe_Detruire( &out->m_entree );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Enregistrer
	out->m_tailleMaximaleFichier = tailleMaximaleFichier;

	// Lancer le thread
	out->m_estContinuer = NTRUE;

	// Creer le thread update
	if( !( out->m_thread = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))NHTTP_Serveur_Log_NCacheLogHTTP_UpdateCache,
		out,
		&out->m_estContinuer ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out->m_repertoireArchivage );
		NFREE( out->m_lienFichierPacket );
		NFREE( out->m_lienFichierEvenement );
		NLib_Memoire_NListe_Detruire( &out->m_entree );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire le cache
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_Log_NCacheLogHTTP_Detruire( NCacheLogHTTP **this )
{
	// Detruire thread
	NLib_Thread_NThread_Detruire( &(*this)->m_thread );

	// Detruire cache
	NLib_Memoire_NListe_Detruire( &(*this)->m_entree );

	// On a un log des evenements?
	if( (*this)->m_fichierLogEvenement != NULL )
		// Detruire
		NLib_Fichier_NFichierTexte_Detruire( &(*this)->m_fichierLogEvenement );

	// On a un log des packets?
	if( (*this)->m_lienFichierPacket != NULL )
		// Detruire log packet
		NLib_Fichier_NFichierTexte_Detruire( &(*this)->m_fichierLogPacket );

	// Liberer
	NFREE( (*this)->m_lienFichierPacket );
	NFREE( (*this)->m_lienFichierEvenement );
	NFREE( (*this)->m_repertoireArchivage );
	NFREE( (*this) );

}

/**
 * Ajouter une entree
 *
 * @param this
 * 		Cette instance
 * @param entree
 * 		L'entree a ajouter
 *
 * @return si l'operation s'est bien passee
 */
__WILLLOCK __WILLUNLOCK NBOOL NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree( NCacheLogHTTP *this,
	__WILLBEOWNED NEntreeCacheLogHTTP *entree )
{
	// Sortie
	__OUTPUT NBOOL resultat;

	// Lock le mutex
	NLib_Memoire_NListe_ActiverProtection( this->m_entree );

	// Ajouter
	resultat = NLib_Memoire_NListe_Ajouter( this->m_entree,
		entree );

	// Unlock le mutex
	NLib_Memoire_NListe_DesactiverProtection( this->m_entree );

	// OK?
    return resultat;
}

/**
 * Ajouter une entree
 *
 * @param this
 * 		Cette instance
 * @param type
 * 		Le type d'entree
 * @param sousType
 * 		Le sous type d'entree
 * @param valeur
 * 		La valeur de l'entree
 * @param ip
 * 		L'ip du client
 * @param identifiant
 * 		L'identifiant de la requete
 *
 * @return si l'operation s'est bien passee
 */
__WILLLOCK __WILLUNLOCK NBOOL NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree2( NCacheLogHTTP *this,
	NTypeEntreeCacheLogHTTP type,
	NU32 sousType,
	const char *valeur,
	const char *ip,
	NU32 identifiant )
{
	// Entree
	NEntreeCacheLogHTTP *entree;

	// Copie IP
	char *copieIP;

	// Dupliquer IP
	if( !( copieIP = NLib_Chaine_Dupliquer( ip ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Construire l'entree
	if( !( entree = NHTTP_Serveur_Log_NEntreeCacheLogHTTP_Construire( type,
		sousType,
		valeur,
		copieIP,
		identifiant ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter
	return NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree( this,
		entree );
}

