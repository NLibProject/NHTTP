#define NHTTP_SERVEUR_LOG_PACKET_NTYPEENTREEPACKET_INTERNE
#include "../../../../include/NHTTP.h"

// -------------------------------------------
// enum NHTTP::Serveur::Log::NTypeEntreePacket
// -------------------------------------------

/**
 * Obtenir type entree
 *
 * @param type
 * 		Le type d'entree
 *
 * @return le texte du type d'entree
 */
const char *NHTTP_Serveur_Log_Packet_NTypeEntreePacket_ObtenirType( NTypeEntreePacket type )
{
	return NTypeEntreePacketTexte[ type ];
}

