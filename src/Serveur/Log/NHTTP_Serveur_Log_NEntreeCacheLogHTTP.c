#include "../../../include/NHTTP.h"

// -----------------------------------------------
// struct NHTTP::Serveur::Log::NEntreeCacheLogHTTP
// -----------------------------------------------

/**
 * Construire l'entree
 *
 * @param type
 * 		Le type d'entree
 * @param sousType
 * 		Le sous type de l'entree
 * @param valeur
 * 		La valeur
 * @param ip
 * 		L'ip du client ayant declenche l'evenement
 * @param identifiantRequete
 * 		L'identifiant de la requete
 *
 * @return la valeur
 */
__ALLOC NEntreeCacheLogHTTP *NHTTP_Serveur_Log_NEntreeCacheLogHTTP_Construire( NTypeEntreeCacheLogHTTP type,
	NU32 sousType,
	const char *valeur,
	__WILLBEOWNED char *ip,
	NU32 identifiantRequete )
{
	// Sortie
	__OUTPUT NEntreeCacheLogHTTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NEntreeCacheLogHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( ip );

		// Quitter
		return NULL;
	}

	// Dupliquer valeur
	if( !( out->m_valeur = NLib_Chaine_Dupliquer( valeur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );
		NFREE( ip );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_type = type;
	out->m_sousType = sousType;
	out->m_ip = ip;
	out->m_date = NLib_Temps_ObtenirTimestamp( );
	out->m_identifiant = identifiantRequete;

	// OK
	return out;
}

/**
 * Detruire l'entree
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_Log_NEntreeCacheLogHTTP_Detruire( NEntreeCacheLogHTTP **this )
{
	// Liberer
	NFREE( (*this)->m_ip );
	NFREE( (*this)->m_valeur );
	NFREE( (*this) );
}

/**
 * Obtenir type
 *
 * @param this
 * 		Cette instance
 *
 * @return le type
 */
NTypeEntreeCacheLogHTTP NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirType( const NEntreeCacheLogHTTP *this )
{
	return this->m_type;
}

/**
 * Obtenir sous type
 *
 * @param this
 * 		Cette instance
 *
 * @return le sous type
 */
NU32 NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirSousType( const NEntreeCacheLogHTTP *this )
{
	return this->m_sousType;
}

/**
 * Obtenir valeur
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur
 */
const char *NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirValeur( const NEntreeCacheLogHTTP *this )
{
	return this->m_valeur;
}

/**
 * Obtenir ip
 *
 * @param this
 * 		Cette instance
 *
 * @return l'ip du client
 */
const char *NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirIP( const NEntreeCacheLogHTTP *this )
{
	return this->m_ip;
}

/**
 * Obtenir date
 *
 * @param this
 * 		Cette instance
 *
 * @return le timestamp de la date d'ajout
 */
NU64 NHTTP_Serveur_Log_NEntreeCacheLogHTTP_ObtenirDate( const NEntreeCacheLogHTTP *this )
{
	return this->m_date;
}

/**
 * Generer sortie finale
 *
 * @param this
 * 		Cette instance
 *
 * @return la sortie finale a loguer
 */
__ALLOC char *NHTTP_Serveur_Log_NEntreeCacheLogHTTP_GenererSortie( const NEntreeCacheLogHTTP *this )
{
	// Sortie
	__OUTPUT char *out = NULL;

	// Date
	char *date;

	// Type
	const char *type;

	// Buffer
	char buffer[ 256 ];

	// Maccro ajout
#define AJOUTER_DATA_LOG( texte ) \
	if( !NLib_Memoire_AjouterData2( &out, \
		(NU32)( ( out != NULL ) ? \
			strlen( out ) \
			: 0 ), \
		texte ) ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED ); \
 \
		/* Liberer */ \
		NFREE( out ); \
 \
 		/* Quitter */ \
 		return NFALSE; \
	}

	// Obtenir date
	if( !( date = NLib_Temps_ObtenirDateFormate( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Quitter
		return NULL;
	}

	// Obtenir type
	switch( this->m_type )
	{
		case NTYPE_ENTREE_CACHE_LOG_HTTP_EVENEMENT:
			// Obtenir sous type
			type = NHTTP_Serveur_Log_Evenement_NTypeEntreeEvenement_ObtenirType( (NTypeEntreeEvenement)this->m_sousType );
			break;

		case NTYPE_ENTREE_CACHE_LOG_HTTP_PACKET:
			type = NHTTP_Serveur_Log_Packet_NTypeEntreePacket_ObtenirType( (NTypeEntreePacket)this->m_sousType );
			break;

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Liberer
			NFREE( date );

			// Quitter
			return NULL;
	}

	// Base
	AJOUTER_DATA_LOG( "[" );
	AJOUTER_DATA_LOG( date );
	AJOUTER_DATA_LOG( "][" );
	AJOUTER_DATA_LOG( this->m_ip );
	AJOUTER_DATA_LOG( "][" );
	AJOUTER_DATA_LOG( type );
	AJOUTER_DATA_LOG( "][" );

	// Construire identifiant
	snprintf( buffer,
		256,
		"%d",
		this->m_identifiant );

	// Ajouter identifiant
	AJOUTER_DATA_LOG( buffer );
	AJOUTER_DATA_LOG( "]" );

	// On a une valeur
	if( this->m_valeur != NULL
		&& strlen( this->m_valeur ) > 0 )
	{
		// Ajouter separation
		AJOUTER_DATA_LOG( ":\n" );

		// Ajouter valeur
		AJOUTER_DATA_LOG( this->m_valeur );
	}

	// Separer
	AJOUTER_DATA_LOG( "\n" );

	// Liberer
	NFREE( date );

	// Undef
#undef AJOUTER_DATA_LOG

	// OK
	return out;
}

