#define NHTTP_SERVEUR_LOG_EVENEMENT_NTYPEENTREEEVENEMENT_INTERNE
#include "../../../../include/NHTTP.h"

// ---------------------------------------------------------
// enum NHTTP::Serveur::Log::Evenement::NTypeEntreeEvenement
// ---------------------------------------------------------

/**
 * Obtenir type d'entree
 *
 * @param type
 * 		Le type d'entree
 *
 * @return le texte associe
 */
const char *NHTTP_Serveur_Log_Evenement_NTypeEntreeEvenement_ObtenirType( NTypeEntreeEvenement type )
{
	return NTypeEntreeEvenementTexte[ type ];
}

