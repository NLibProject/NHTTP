#include "../../../include/NHTTP.h"

// ----------------------------------
// namespace NHTTP::Serveur::Commande
// ----------------------------------

/**
 * Afficher l'aide
 */
void NHTTP_Serveur_Commande_AfficherAide( void )
{
	// Iterateur
	NTypeCommande i = (NTypeCommande)1;

	// Afficher commandes
	for( ; i < NTYPES_COMMANDE; i++ )
		printf( "[%d]\t%s: %s\n",
			(NU32)i - 1,
			NHTTP_Serveur_Commande_NTypeCommande_ObtenirCommande( i ),
			NHTTP_Serveur_Commande_NTypeCommande_ObtenirDescriptionCommande( i ) );
}

/**
 * Thread de gestion des commandes
 *
 * @param serveur
 * 		Le serveur http
 *
 * @return si le thread s'est deroule normalement
 */
__THREAD NBOOL NHTTP_Serveur_Commande_Thread( NHTTPServeur *serveur )
{
	// Buffer
	char buffer[ 256 ];

	// Afficher l'aide une fois
	NHTTP_Serveur_Commande_AfficherAide( );

	// Boucle commandes
	do
	{
		// Lire commande
		NLib_Chaine_LireStdin2( buffer,
			256 );

		// Analyser commande
		switch( NHTTP_Serveur_Commande_NTypeCommande_Interpreter( buffer ) )
		{
			case NTYPE_COMMANDE_QUITTER:
				NHTTP_Serveur_NHTTPServeur_Arreter( serveur );
				break;

			case NTYPE_COMMANDE_AIDE:
			case NTYPE_COMMANDE_AIDE2:
			case NTYPE_COMMANDE_AIDE3:
				// Afficher aide
				NHTTP_Serveur_Commande_AfficherAide( );
				break;

			// Fermer/ouvrir le serveur
			case NTYPE_COMMANDE_FERMER_SERVEUR:
				NHTTP_Serveur_NHTTPServeur_Fermer( serveur );
				break;
			case NTYPE_COMMANDE_OUVRIR_SERVEUR:
				NHTTP_Serveur_NHTTPServeur_Ouvrir( serveur );
				break;

			// Activer/Desactiver log
			case NTYPE_COMMANDE_ACTIVER_LOG_CONSOLE:
				NHTTP_Serveur_NHTTPServeur_ActiverLogConsole( serveur );
				break;
			case NTYPE_COMMANDE_DESACTIVER_LOG_CONSOLE:
				NHTTP_Serveur_NHTTPServeur_DesactiverLogConsole( serveur );
				break;

			// Afficher informations interfaces reseau
			case NTYPE_COMMANDE_AFFICHER_INTERFACE:
#ifdef IS_WINDOWS
				system( "ipconfig" );
#else // IS_WINDOWS
				system( "ifconfig" );
#endif // !IS_WINDOWS
				break;

			case NTYPE_COMMANDE_ABOUT:
				// Informations serveur
				puts( "\nNServeurHTTP V2 (SOARES Lucas) @ http://nproject.ddns.net/\n" );

				// Choupie (///o///)
				puts( "Je vais enfin avoir mon Milou" );
				break;

			default:
				// Notifier
				NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_VERBOSE,
					"Commande inconnue...",
					0 );
				break;
		}
	} while( NHTTP_Serveur_NHTTPServeur_EstEnCours( serveur ) );

	// OK
	return NTRUE;
}

