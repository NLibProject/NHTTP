#define NHTTP_SERVEUR_COMMANDE_NTYPECOMMANDE_INTERNE
#include "../../../include/NHTTP.h"

// --------------------------------------------
// enum NHTTP::Serveur::Commande::NTypeCommande
// --------------------------------------------

/**
 * Interpreter commande
 *
 * @param commande
 * 		La commande saisie
 *
 * @return le resultat de l'interpretation
 */
NTypeCommande NHTTP_Serveur_Commande_NTypeCommande_Interpreter( const char *commande )
{
	// Identifiant
	__OUTPUT NTypeCommande i = (NTypeCommande)0;

	// Verifier si l'utilisateur a saisi un identifiant
	if( NLib_Caractere_EstUnChiffre( commande[ 0 ] ) )
	{
		// Convertir
		i = (NTypeCommande)strtol( commande,
			NULL,
			10 );

		// Interpreter
		return ( i >= NTYPES_COMMANDE ) ?
				NTYPE_COMMANDE_AUCUNE
				: i + 1;
	}
	// Commande texte
	else
		// Chercher
		for( ; i < NTYPES_COMMANDE; i++ )
			if( NLib_Chaine_Comparer( NTypeCommandeTexte[ i ],
				commande,
				NFALSE,
				0 ) )
				return i;

	// La commande n'existe pas
	return NTYPE_COMMANDE_AUCUNE;
}

/**
 * Obtenir commande
 *
 * @param commande
 * 		La commande a obtenir
 *
 * @return la commande
 */
const char *NHTTP_Serveur_Commande_NTypeCommande_ObtenirCommande( NTypeCommande commande )
{
	return NTypeCommandeTexte[ commande ];
}

/**
 * Obtenir description commande
 *
 * @param commande
 * 		La commande a obtenir
 *
 * @return la description de la commande
 */
const char *NHTTP_Serveur_Commande_NTypeCommande_ObtenirDescriptionCommande( NTypeCommande commande )
{
	return NTypeCommandeDescriptionTexte[ commande ];
}

