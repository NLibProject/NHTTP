#include "../../../include/NHTTP.h"

// -------------------------------
// namespace NHTTP::Serveur::Outil
// -------------------------------

/**
 * Callback reception packet
 *
 * @param client
 * 		Le client ayant emis le packet
 * @param packet
 * 		Le packet recu
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Serveur_Outil_CallbackReception( const NClientServeur *client,
	const NPacket *packet )
{
	// Chaine requete
	char *chaineRequete;

	// Requete
	NRequeteHTTP *requete;

	// Reponse
	NReponseHTTP *reponse;

	// Packet a envoyer
	NPacketPersonnalise *packetReponse;

	// Serveur
	const NHTTPServeur *serveur;

	// Cache log
	NCacheLogHTTP *cacheLog;

	// Recuperer serveur/cache log
	if( !( serveur = (NHTTPServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ) )
		|| !( cacheLog = (NCacheLogHTTP*)NHTTP_Serveur_NHTTPServeur_ObtenirCacheLog( serveur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Allouer la requete
	if( !( chaineRequete = calloc( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Copier requete
	memcpy( chaineRequete,
		NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) );

	// Loguer requete
	NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree2( cacheLog,
		NTYPE_ENTREE_CACHE_LOG_HTTP_PACKET,
		NTYPE_ENTREE_PACKET_REQUETE,
		chaineRequete,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

	// Construire requete
	if( !( requete = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire( chaineRequete,
		client,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( chaineRequete );

		// Quitter
		return NFALSE;
	}

	// Loguer requete
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Loguer( requete,
		cacheLog,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ) );

	// Detruire la chaine de la requete
	NFREE( chaineRequete );

	// Traiter la requete/Generer reponse
	if( !( reponse = NHTTP_Serveur_NHTTPServeur_TraiterRequete( (NHTTPServeur*)serveur,
		requete,
		client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CANT_PROCESS_REQUEST );

		// Detruire requete
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &requete );

		// Quitter
		return NFALSE;
	}

	// Detruire la requete
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &requete );

	// Creer le packet
	if( !( packetReponse = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_CreerPacket( reponse ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Detruire reponse
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &reponse );

		// Quitter
		return NFALSE;
	}

	// Loguer packet reponse
	NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree2( cacheLog,
		NTYPE_ENTREE_CACHE_LOG_HTTP_PACKET,
		NTYPE_ENTREE_PACKET_REPONSE,
		NHTTP_Commun_Packet_NPacketHTTP_ObtenirHeader( (const NPacketHTTP *)NLib_Module_Reseau_Packet_NPacketPersonnalise_ObtenirDataPersonnalise( packetReponse ) ),
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

	// Loguer reponse
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Loguer( reponse,
		cacheLog,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ) );

	// Detruire la reponse
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &reponse );

	// Envoyer le packet
	if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( (NClientServeur*)client,
		packetReponse ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MEMORY );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/**
 * Callback connexion client
 *
 * @param client
 * 		Le client qui se connecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Serveur_Outil_CallbackConnexion( const NClientServeur *client )
{
	// Serveur
	NHTTPServeur *serveur;

	// Cache log
	NCacheLogHTTP *cacheLog;

	// Obtenir serveur/Cache log
	if( !( serveur = (NHTTPServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ) )
		|| !( cacheLog = (NCacheLogHTTP*)NHTTP_Serveur_NHTTPServeur_ObtenirCacheLog( serveur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Logguer la connexion
	NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree2( cacheLog,
		NTYPE_ENTREE_CACHE_LOG_HTTP_EVENEMENT,
		NTYPE_ENTREE_EVENEMENT_CONNEXION,
		"",
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

	// OK
	return NTRUE;
}

/**
 * Callback deconnexion client
 *
 * @param client
 * 		Le client qui se deconnecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Serveur_Outil_CallbackDeconnexion( const NClientServeur *client )
{
	// Serveur
	NHTTPServeur *serveur;

	// Cache log
	NCacheLogHTTP *cacheLog;

	// Obtenir serveur/Cache log
	if( !( serveur = (NHTTPServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ) )
		|| !( cacheLog = (NCacheLogHTTP*)NHTTP_Serveur_NHTTPServeur_ObtenirCacheLog( serveur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Logguer la deconnexion
	NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree2( cacheLog,
		NTYPE_ENTREE_CACHE_LOG_HTTP_EVENEMENT,
		NTYPE_ENTREE_EVENEMENT_DECONNEXION,
		"",
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresse( client ),
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

	// OK
	return NTRUE;
}

