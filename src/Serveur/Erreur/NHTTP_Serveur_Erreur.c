#include "../../../include/NHTTP.h"

// --------------------------------
// namespace NHTTP::Serveur::Erreur
// --------------------------------

/**
 * Flux de sortie des erreurs
 *
 * @param erreur
 * 		L'erreur
 */
__CALLBACK void NHTTP_Serveur_Erreur_FluxErreur( const NErreur *erreur )
{
	// Afficher message?
	NBOOL estAfficher = NTRUE;

	// On n'affichera pas les avertissements
	if( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) <= NNIVEAU_ERREUR_AVERTISSEMENT )
		estAfficher = NFALSE;

	// Doit afficher?
	switch( NLib_Erreur_NErreur_ObtenirCode( erreur ) )
	{
		case NERREUR_SOCKET_RECV:
		case NERREUR_SOCKET_SEND:
			estAfficher = NFALSE;//(NBOOL)( NLib_Erreur_NErreur_ObtenirCodeUtilisateur( erreur ) != 0 ); // (<=> errno != 0?)
			break;

		case NERREUR_USER:
			estAfficher = NTRUE;
			break;

		default:
			break;
	}

	// Afficher l'erreur
	if( estAfficher )
		switch( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) )
		{
			case NNIVEAU_ERREUR_ERREUR:
				// Erreur
				printf( "[%s]: %s( )::(%s)\n",
					NLib_Erreur_NNiveauErreur_Traduire( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) ),
					NLib_Erreur_NErreur_ObtenirMessage( erreur ),
					NLib_Erreur_NCodeErreur_Traduire( NLib_Erreur_NErreur_ObtenirCode( erreur ) ) );

				// Origine
				printf( "\"%s\", ligne %d, code %d.\n\n",
					NLib_Erreur_NErreur_ObtenirFichier( erreur ),
					NLib_Erreur_NErreur_ObtenirLigne( erreur ),
					NLib_Erreur_NErreur_ObtenirCodeUtilisateur( erreur ) );
				break;

			case NNIVEAU_ERREUR_AVERTISSEMENT:
				puts( NLib_Erreur_NErreur_ObtenirMessage( erreur ) );
				break;

			default:
				break;
		}
}