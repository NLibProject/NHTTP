#include "../../include/NHTTP.h"

// -----------------------------------
// struct NHTTP::Serveur::NHTTPServeur
// -----------------------------------

/**
 * Construire serveur
 *
 * @param port
 * 		Le port d'ecoute du serveur
 * @param interfaceEcoute
 * 		L'adresse ip d'ecoute (NULL pour toutes les interfaces)
 * @param estOuvert
 * 		Le serveur est ouvert au lancement
 * @param estAccepteCommandeShell
 * 		Le serveur peut etre pilote via CLI
 * @param lienLogEvenement
 * 		Le lien vers le log des evenements (Si NULL, log evenements desactive)
 * @param lienLogPacket
 * 		Le lien vers le log des packets (Si NULL, log packets desactive)
 * @param tailleMaximaleFichierLog
 * 		La taille maximale des fichiers de log
 * @param repertoireArchivageLog
 * 		Le repertoire d'archivage des logs
 * @param callbackTraitementRequete
 * 		Le callback de traitement de la requete
 * @param dataUtilisateur
 * 		Les donnees utilisateur
 *
 * @return le serveur
 */
__ALLOC NHTTPServeur *NHTTP_Serveur_NHTTPServeur_Construire( NU32 port,
	const char *interfaceEcoute,
	NBOOL estOuvert,
	NBOOL estAccepteCommandeShell,
	const char *lienLogEvenement,
	const char *lienLogPacket,
	NU32 tailleMaximaleFichierLog,
	const char *repertoireArchivageLog,
	__CALLBACK __ALLOC NReponseHTTP *( *callbackTraitementRequete )( const NRequeteHTTP*,
	const NClientServeur* ),
	void *dataUtilisateur )
{
	// Sortie
	__OUTPUT NHTTPServeur *out;

	// Ignorer signal SIGPIPE
#ifndef IS_WINDOWS
	signal( SIGPIPE,
		SIG_IGN );
#endif // !IS_WINDOWS

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NHTTPServeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_estThreadCommande = estAccepteCommandeShell;
	out->m_callbackTraitementRequete = callbackTraitementRequete;
	out->m_data = dataUtilisateur;

	// Construire le cache log
	if( !( out->m_cacheLog = NHTTP_Serveur_Log_NCacheLogHTTP_Construire( lienLogEvenement,
		lienLogPacket,
		tailleMaximaleFichierLog,
		repertoireArchivageLog ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer le serveur
	if( !( out->m_serveur = NLib_Module_Reseau_Serveur_NServeur_Construire2( port,
		interfaceEcoute,
		(NBOOL ( ___cdecl* )( const NClientServeur*,
			const NPacket* ))NHTTP_Serveur_Outil_CallbackReception,
		(NBOOL ( ___cdecl* )( const NClientServeur* ))NHTTP_Serveur_Outil_CallbackConnexion,
		(NBOOL ( ___cdecl* )( const NClientServeur* ))NHTTP_Serveur_Outil_CallbackDeconnexion,
		out,
		NHTTP_SERVEUR_TEMPS_AVANT_TIMEOUT_RECEPTION,
		NHTTP_SERVEUR_TEMPS_AVANT_TIMEOUT_EMISSION,
		NTYPE_SERVEUR_TCP,
		NTYPE_CACHE_PACKET_PERSONNALISE,
		NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketRaw, // TODO methode de reception personnalisee, la premiere partie jusqu'au \n\n de separation des deux parties doit etre lue dans la memoire, l'autre dans un fichier temporaire pour eviter les depassements de limite de memoire
		(NBOOL ( ___cdecl* )( const void*,
			SOCKET ))NHTTP_Commun_Outil_CallbackMethodeEnvoiPacket ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire cache log
		NHTTP_Serveur_Log_NCacheLogHTTP_Detruire( &out->m_cacheLog );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// On doit ouvrir?
	if( estOuvert )
		// Ouvrir
		NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( out->m_serveur );

	// Allumer serveur
	out->m_estEnCours = NTRUE;

	// On veut une gestion des commandes?
	if( estAccepteCommandeShell )
		// Creer thread
		if( !( out->m_threadCommande = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void*))NHTTP_Serveur_Commande_Thread,
			out,
			NULL ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire serveur
			NLib_Module_Reseau_Serveur_NServeur_Detruire( &out->m_serveur );

			// Detruire cache log
			NHTTP_Serveur_Log_NCacheLogHTTP_Detruire( &out->m_cacheLog );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

	// OK
	return out;
}

/**
 * Construire serveur HTTP avec log par defaut
 *
 * @param port
 * 		Le port d'ecoute du serveur
 * @param estOuvert
 * 		Le serveur est ouvert au lancement
 * @param estAccepteCommandeShell
 * 		Le serveur peut etre pilote via CLI
 *
 * @return le serveur
 */
__ALLOC NHTTPServeur *NHTTP_Serveur_NHTTPServeur_Construire2( NU32 port,
	NBOOL estOuvert,
	NBOOL estAccepteCommandeShell )
{
	return NHTTP_Serveur_NHTTPServeur_Construire( port,
		NULL,
		estOuvert,
		estAccepteCommandeShell,
		NHTTP_SERVEUR_LOG_FICHIER_LOG_EVENEMENT_DEFAUT,
		NHTTP_SERVEUR_LOG_FICHIER_LOG_PACKET_DEFAUT,
		NHTTP_SERVEUR_LOG_FICHIER_TAILLE_MAXIMALE_DEFAUT,
		NHTTP_SERVEUR_LOG_REPERTOIRE_ARCHIVAGE_DEFAUT,
		NHTTP_Commun_HTTP_Traitement_TraiterRequete,
		NULL );
}

/**
 * Construire serveur HTTP sans log
 *
 * @param port
 * 		Le port d'ecoute du serveur
 * @param estOuvert
 * 		Le serveur est ouvert au lancement
 * @param estAccepteCommandeShell
 * 		Le serveur peut etre pilote via CLI
 *
 * @return le serveur
 */
__ALLOC NHTTPServeur *NHTTP_Serveur_NHTTPServeur_Construire3( NU32 port,
	NBOOL estOuvert,
	NBOOL estAccepteCommandeShell )
{
	return NHTTP_Serveur_NHTTPServeur_Construire( port,
		NULL,
		estOuvert,
		estAccepteCommandeShell,
		NULL,
		NULL,
		0,
		NULL,
		NHTTP_Commun_HTTP_Traitement_TraiterRequete,
		NULL );
}

/**
 * Detruire serveur
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_Detruire( NHTTPServeur **this )
{
	// Thread commande existe?
	if( (*this)->m_estThreadCommande )
		// Detruire thread commande
		NLib_Thread_NThread_Detruire( &(*this)->m_threadCommande );

	// Detruire serveur
	NLib_Module_Reseau_Serveur_NServeur_Detruire( &(*this)->m_serveur );

	// Detruire cache log
	NHTTP_Serveur_Log_NCacheLogHTTP_Detruire( &(*this)->m_cacheLog );

	// Liberer
	NFREE( (*this) );
}

/**
 * Est en cours?
 *
 * @param this
 * 		Cette instance
 */
NBOOL NHTTP_Serveur_NHTTPServeur_EstEnCours( const NHTTPServeur *this )
{
	return this->m_estEnCours;
}

/**
 * Arreter le serveur
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_Arreter( NHTTPServeur *this )
{
	// Arreter
	this->m_estEnCours = NFALSE;
}

/**
 * Est gere par un thread de commandes shell?
 *
 * @param this
 * 		Cette instance
 *
 * @return si le serveur est gere par un thread de commande
 */
NBOOL NHTTP_Serveur_NHTTPServeur_EstThreadCommandeActif( NHTTPServeur *this )
{
	return this->m_estThreadCommande;
}

/**
 * Ouvrir le serveur
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_Ouvrir( NHTTPServeur *this )
{
	NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( this->m_serveur );
}

/**
 * Fermer le serveur
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_Fermer( NHTTPServeur *this )
{
	NLib_Module_Reseau_Serveur_NServeur_InterdireConnexion( this->m_serveur );
}

/**
 * Activer log console
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_ActiverLogConsole( NHTTPServeur *this )
{
	NLib_Module_Reseau_Serveur_NServeur_ActiverLogConsole( this->m_serveur );
}

/**
 * Desactiver log console
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Serveur_NHTTPServeur_DesactiverLogConsole( NHTTPServeur *this )
{
	NLib_Module_Reseau_Serveur_NServeur_DesactiverLogConsole( this->m_serveur );
}

/**
 * Obtenir cache log
 *
 * @param this
 * 		Cette instance
 *
 * @return le cache log
 */
const NCacheLogHTTP *NHTTP_Serveur_NHTTPServeur_ObtenirCacheLog( const NHTTPServeur *this )
{
	return this->m_cacheLog;
}

/**
 * Traiter requete
 *
 * @param this
 * 		Cette instance
 * @param requete
 * 		La requete a traiter
 * @param client
 * 		Le client ayant effectue la requete
 *
 * @return la reponse generee par le traitement (NReponseHTTP*)
 */
__ALLOC NReponseHTTP *NHTTP_Serveur_NHTTPServeur_TraiterRequete( NHTTPServeur *this,
	const NRequeteHTTP *requete,
	const NClientServeur *client )
{
	return this->m_callbackTraitementRequete( requete,
		client );
}

/**
 * Definir data utilisateur
 *
 * @param this
 * 		Cette instance
 * @param data
 * 		Les data de l'utilisateur
 */
void NHTTP_Serveur_NHTTPServeur_DefinirDataUtilisateur( NHTTPServeur *this,
	void *data )
{
	this->m_data = data;
}

/**
 * Obtenir data utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return les data utilisateur
 */
void *NHTTP_Serveur_NHTTPServeur_ObtenirDataUtilisateur( const NHTTPServeur *this )
{
	return this->m_data;
}

