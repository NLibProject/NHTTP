#include "../../include/NHTTP.h"

// ------------------------
// namespace NHTTP::Serveur
// ------------------------

#if defined( NHTTP_SERVEUR_PROJECT ) && !defined( NHTTP_TEST_PROJECT )
/**
 * Point d'entree
 *
 * @param argc
 * 		Le nombre d'arguments
 * @param argv
 * 		Le tableau d'arguments
 *
 * @return EXIT_SUCCESS si tout s'est bien passee EXIT_FAILURE sinon
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Port
	NU32 port;

	// Serveur
	NHTTPServeur *serveur;

	// Selectionner le port
	if( argc <= 1 )
		// Port par defaut
		port = NHTTP_PORT_DEFAUT;
	else
		// Recuperer port dans la ligne de commande
		if( ( port = (NU32)strtol( argv[ 1 ],
			NULL,
			10 ) ) == 0
			|| port > 65535 )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SYNTAX );

			// Quitter
			return EXIT_FAILURE;
		}

	// Notifier port
	printf( "[Lancement] Port: %d\n", port );

	// Initialiser NLib
	NLib_Initialiser( NHTTP_Serveur_Erreur_FluxErreur );

	// Construire le serveur
	if( !( serveur = NHTTP_Serveur_NHTTPServeur_Construire2( port,
		NTRUE,
#ifdef NHTTP_DESACTIVER_THREAD_COMMANDE
		NFALSE ) ) )
#else // NHTTP_DESACTIVER_THREAD_COMMANDE
		NTRUE ) ) )
#endif // !NHTTP_DESACTIVER_THREAD_COMMANDE
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Attendre la fin de l'execution
	if( NHTTP_Serveur_NHTTPServeur_EstThreadCommandeActif( serveur ) )
		while( NHTTP_Serveur_NHTTPServeur_EstEnCours( serveur ) )
			// 60 fps
			NLib_Temps_Attendre( 16 );
	else
		while( getchar( ) != 'q' )
			NLib_Temps_Attendre( 1 );

	// Detruire le serveur
	NHTTP_Serveur_NHTTPServeur_Detruire( &serveur );

	// Quitter NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}
#endif // NHTTP_SERVEUR_PROJECT && !NHTTP_TEST_PROJECT
