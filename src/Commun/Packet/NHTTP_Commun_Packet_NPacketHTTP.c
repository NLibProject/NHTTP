#include "../../../include/NHTTP.h"

// -----------------------------------------
// struct NHTTP::Commun::Packet::NPacketHTTP
// -----------------------------------------

/**
 * Construire packet http
 *
 * @return le packet vide
 */
__ALLOC NPacketHTTP *NHTTP_Commun_Packet_NPacketHTTP_Construire( void )
{
	// Sortie
	__OUTPUT NPacketHTTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacketHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Construire copie packet http
 *
 * @param src
 * 		Le packet a copier
 *
 * @return une copie de src
 */
__ALLOC NPacketHTTP *NHTTP_Commun_Packet_NPacketHTTP_Construire2( const NPacketHTTP *src )
{
	// Sortie
	__OUTPUT NPacketHTTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NPacketHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer les donnees
	if( !( out->m_header = calloc( strlen( src->m_header ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier header
	memcpy( out->m_header,
		src->m_header,
		strlen( src->m_header ) );

	// Copier fichier
	if( src->m_fichier != NULL )
		if( !( out->m_fichier = NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Construire2( src->m_fichier ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( out->m_header );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// OK
	return out;
}

/**
 * Detruire le packet
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_Packet_NPacketHTTP_Detruire( NPacketHTTP **this )
{
	// Il y a un contenu?
	if( ( *this )->m_fichier != NULL )
		// Detruire contenu
		NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Detruire( (NFichierHTTP **)&( *this )->m_fichier );

	// Liberer
	NFREE( (*this)->m_header );
	NFREE( (*this) );
}

/**
 * Ajouter des donnees a l'header
 *
 * @param this
 * 		Cette instance
 * @param texte
 * 		Le texte a ajouter a l'header
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( NPacketHTTP *this,
	const char *texte )
{
	// Ajouter
	return NLib_Memoire_AjouterData( &this->m_header,
		(NU32)( this->m_header != NULL ?
			strlen( this->m_header )
			: 0 ),
		texte,
		(NU32)( strlen( texte ) + 1 ) );
}

/**
 * Definir fichier reponse
 *
 * @param this
 * 		Cette instance
 * @param fichierReponse
 * 		Les details du fichier a envoyer
 *
 * @return si l'operation s'est bien passee
 */
void NHTTP_Commun_Packet_NPacketHTTP_DefinirFichier( NPacketHTTP *this,
	__WILLBEOWNED void *fichierReponse )
{
	// Detruire si deja defini
	if( this->m_fichier != NULL )
		// Detruire
		NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Detruire( (NFichierHTTP **)&this->m_fichier );

	// Dupliquer
	this->m_fichier = fichierReponse;
}

/**
 * Obtenir header
 *
 * @param this
 * 		Cette instance
 *
 * @return l'header
 */
const char *NHTTP_Commun_Packet_NPacketHTTP_ObtenirHeader( const NPacketHTTP *this )
{
	return this->m_header;
}

/**
 * Obtenir la taille de l'header
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille de l'header
 */
NU32 NHTTP_Commun_HTTP_Packet_NPacketHTTP_ObtenirTailleHeader( const NPacketHTTP *this )
{
	return (NU32)strlen( this->m_header );
}

/**
 * Obtenir le fichier (NFichierReponseHTTP*)
 *
 * @param this
 * 		Cette instance
 *
 * @return le fichier
 */
const void *NHTTP_Commun_HTTP_Packet_NPacketHTTP_ObtenirFichier( const NPacketHTTP *this )
{
	return this->m_fichier;
}

