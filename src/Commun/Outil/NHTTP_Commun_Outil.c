#include "../../../include/NHTTP.h"

// ------------------------------
// namespace NHTTP::Commun::Outil
// ------------------------------

/**
 * Methode d'envoi d'un packet
 *
 * @param packet
 * 		Le packet a envoyer
 * @param socket
 * 		La socket sur laquelle envoyer
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Commun_Outil_CallbackMethodeEnvoiPacket( const NPacketPersonnalise *packet,
	SOCKET socket )
{
	// Packet HTTP
	NPacketHTTP *packetHTTP;

	// Fichier HTTP
	NFichierHTTP *fichierHTTP;

	// Taille du contenu
	NU64 tailleContenu;

	// Fichier a envoyer
	NFichierBinaire *fichierAEnvoyer;

	// Extraire packet HTTP
	if( !( packetHTTP = NLib_Module_Reseau_Packet_NPacketPersonnalise_ObtenirDataPersonnalise( packet ) )
		|| !( fichierHTTP = (NFichierHTTP*)NHTTP_Commun_HTTP_Packet_NPacketHTTP_ObtenirFichier( packetHTTP ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet header
	if( !NLib_Module_Reseau_Packet_PacketIO_EnvoyerDataRaw( NHTTP_Commun_Packet_NPacketHTTP_ObtenirHeader( packetHTTP ),
		NHTTP_Commun_HTTP_Packet_NPacketHTTP_ObtenirTailleHeader( packetHTTP ),
		socket ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

		// Quitter
		return NFALSE;
	}

	// Recuperer la taille du contenu
	tailleContenu = NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTaille( fichierHTTP );

	// Envoyer le contenu
	switch( NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTypeData( fichierHTTP ) )
	{
		case NTYPE_FICHIER_HTTP_TEXTE:
			if( !NLib_Module_Reseau_Packet_PacketIO_EnvoyerDataRaw( NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirData( fichierHTTP ),
				(NU32)tailleContenu,
				socket ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

				// Quitter
				return NFALSE;
			}
			break;

		case NTYPE_FICHIER_HTTP_FICHIER:
			// Recuperer fichier
			if( !( fichierAEnvoyer = (NFichierBinaire*)NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirData( fichierHTTP ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_UNKNOWN );

				// Quitter
				return NFALSE;
			}

			// Envoyer fichier
			if( !NLib_Module_Reseau_Packet_PacketIO_EnvoyerFichierRaw( fichierAEnvoyer,
				socket ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

				// Quitter
				return NFALSE;
			}
			break;

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Quitter
			return NFALSE;
	}

	// OK
	return NTRUE;
}

