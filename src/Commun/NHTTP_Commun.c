#include "../../include/NHTTP.h"

// -----------------------
// namespace NHTTP::Commun
// -----------------------

struct DownloadState
{
	// Is received?
	NBOOL m_isReceived;

	// Local filename
	const char *m_localFileName;
} DownloadState;

/**
 * Download file receive callback
 *
 * @param packet
 * 		The received packet
 * @param client
 * 		The receiving client
 *
 * @return if operation succeeded
 */
__PRIVATE __CALLBACK NBOOL NHTTP_Commun_DownloadReceiveCallback( const NPacket *packet,
	const NClient *client )
{
	// HTTP client
	NClientHTTP *httpClient;

	// File
	NFichierBinaire *file;

	// Download state
	struct DownloadState *downloadState;

	// HTTP response packet
	NReponseHTTP *response;

	// Read length
	NU32 readLength = 0;

	// Length to read
	NU32 lengthToRead;

	// Secure data
	char *secureData;

#define BUFFER_SIZE		256

	// Get http client
	if( !( httpClient = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) )
		|| !( downloadState = NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( httpClient ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quit
		return NFALSE;
	}

	// Allocate data secure
	if( !( secureData = calloc( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) + 1,
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NFALSE;
	}

	// Copy data
	memcpy( secureData,
		NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) );

	// Build http packet
	if( !( response = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire2( secureData ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Free
		NFREE( secureData );

		// Quit
		return NFALSE;
	}

	// Free
	NFREE( secureData );

	// Open file
	if( !( file = NLib_Fichier_NFichierBinaire_ConstruireEcriture( downloadState->m_localFileName,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Destroy HTTP response
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &response );

		// Quit
		return NFALSE;
	}

	// Write content
	while( readLength < NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) )
	{
		// Calculate length to be read
		lengthToRead = ( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) - readLength >= BUFFER_SIZE ) ?
			BUFFER_SIZE
			: ( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) - readLength );

		// Write data
		NLib_Fichier_NFichierBinaire_Ecrire( file,
			NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( response ) + readLength,
			lengthToRead );

		// Increase read length
		readLength += lengthToRead;
	}

	// Close file
	NLib_Fichier_NFichierBinaire_Detruire( &file );

	// Destroy HTTP response
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &response );

	// Destroy file
	NLib_Fichier_NFichierBinaire_Detruire( &file );

#undef BUFFER_SIZE

	// OK
	return downloadState->m_isReceived = NTRUE;
}

/**
 * Download a file
 *
 * @param hostname
 * 		The hostname
 * @param port
 * 		The port
 * @param remoteFilePath
 * 		The remote file path
 * @param localFileName
 * 		The local filename
 * @param username
 * 		The username (basic auth)
 * @param password
 * 		The password (basic auth)
 * @param connectionTimeout
 * 		The connection timeout
 *
 * @return if the operation succeeded
 */
NBOOL NHTTP_Commun_DownloadFile( const char *hostname,
	NU32 port,
	const char *remoteFilePath,
	const char *localFileName,
	const char *username,
	const char *password,
	NU32 connectionTimeout )
{
	// HTTP client
	NClientHTTP *httpClient;

	// Request
	NRequeteHTTP *request;

	// Buffer
	char buffer[ 256 ];

	// Base64 encode
	char *base64Token;

	// Download state
	struct DownloadState downloadState;

	// Init download state
	downloadState.m_isReceived = NFALSE;
	downloadState.m_localFileName = localFileName;

	// Construire client
	if( !( httpClient = NHTTP_Client_NClientHTTP_Construire2( hostname,
		port,
		&downloadState,
		NHTTP_Commun_DownloadReceiveCallback ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Build request
	if( !( request = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_GET,
		remoteFilePath ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy client
		NHTTP_Client_NClientHTTP_Detruire( &httpClient );

		// Quit
		return NFALSE;
	}

	// Username?
	if( username != NULL )
	{
		// Build authentication
		snprintf( buffer,
			256,
			"%s:%s",
			username,
			password != NULL ?
				password
				: "" );

		// Base64 encode
		if( !( base64Token = NLib_Chaine_ConvertirBase64( buffer,
			(NU32)strlen( buffer ) ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_COPY );

			// Destroy client
			NHTTP_Client_NClientHTTP_Detruire( &httpClient );

			// Quit
			return NFALSE;
		}

		// Final string
		snprintf( buffer,
			256,
			"Basic %s",
			 base64Token );

		// Free
		NFREE( base64Token );

		// Add authentication attribute
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( request,
			NMOT_CLEF_REQUETE_HTTP_KEYWORD_AUTHORIZATION,
			buffer );
	}

	// Send request
	if( !NHTTP_Client_NClientHTTP_EnvoyerRequete( httpClient,
		request,
		connectionTimeout ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_SOCKET_SEND );

		// Destroy client
		NHTTP_Client_NClientHTTP_Detruire( &httpClient );

		// Quit
		return NFALSE;
	}

	// Wait for packet to be sent
	while( NHTTP_Client_NClientHTTP_EstPacketAttente( httpClient )
		&& !NHTTP_Client_NClientHTTP_EstErreur( httpClient ) )
		NLib_Temps_Attendre( 1 );

	// Wait for response
	while( !NHTTP_Client_NClientHTTP_EstErreur( httpClient )
		&& !downloadState.m_isReceived )
		NLib_Temps_Attendre( 1 );

	// Destroy client
	NHTTP_Client_NClientHTTP_Detruire( &httpClient );

	// OK
	return NTRUE;
}

