#include "../../../../../include/NHTTP.h"

// --------------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Requete::NElementRequeteHTTP
// --------------------------------------------------------------------

/**
 * Construire element
 *
 * @param motClef
 * 		Le mot clef de l'element
 * @param valeur
 * 		La valeur
 *
 * @return l'instance de l'element
 */
__ALLOC NElementRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire( const char *motClef,
	const char *valeur )
{
	// Mot clef
	NMotClefRequeteHTTP type;

	// Chercher le type de requete
	if( ( type = NHTTP_Commun_HTTP_NMotClefRequeteHTTP_InterpreterMotClef( motClef ) ) >= NMOT_CLEF_REQUETE_HTTP_KEYWORDS )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Quitter
		return NULL;
	}

	// Construire
	return NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire3( type,
		valeur );
}

/**
 * Construire element depuis ligne header
 *
 * @param ligne
 * 		La ligne a parser
 *
 * @return l'instance de l'element
 */
__ALLOC NElementRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire2( const char *ligne )
{
	// Sortie
	__OUTPUT NElementRequeteHTTP *out;

	// Curseur
	NU32 curseur = 0;

	// Clef
	char *clef;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NElementRequeteHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Lire clef
	if( !( clef = NLib_Chaine_LireJusqua( ligne,
		':',
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Parser
	if( ( out->m_type = NHTTP_Commun_HTTP_NMotClefRequeteHTTP_InterpreterMotClef( clef ) ) >= NMOT_CLEF_REQUETE_HTTP_KEYWORDS )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Liberer
		NFREE( clef );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Liberer
	NFREE( clef );

	// Lire valeur
	if( !( out->m_valeur = NLib_Chaine_LireJusqua( ligne,
		'\0',
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Supprimer ' ' de depart
	NLib_Chaine_SupprimerCaractereDebut( out->m_valeur,
		' ' );

	// OK
	return out;
}

/**
 * Construire element
 *
 * @param type
 * 		Le type d'element
 * @param valeur
 * 		La valeur
 *
 * @return l'instance de l'element
 */
__ALLOC NElementRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire3( NMotClefRequeteHTTP type,
	const char *valeur )
{
	// Sortie
	__OUTPUT NElementRequeteHTTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NElementRequeteHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer type requete
	out->m_type = type;

	// Duplier valeur
	if( !( out->m_valeur = NLib_Chaine_Dupliquer( valeur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire l'element
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Detruire( NElementRequeteHTTP **this )
{
	// Liberer
	NFREE( (*this)->m_valeur );
	NFREE( (*this) );
}

/**
 * Obtenir type mot clef
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de mot clef
 */
NMotClefRequeteHTTP NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef( const NElementRequeteHTTP *this )
{
	return this->m_type;
}

/**
 * Obtenir mot clef texte
 *
 * @param this
 * 		Cette instance
 *
 * @return le mot clef texte
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef2( const NElementRequeteHTTP *this )
{
	return NHTTP_Commun_HTTP_NMotClefRequeteHTTP_ObtenirMotClef( this->m_type );
}

/**
 * Obtenir valeur
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirValeur( const NElementRequeteHTTP *this )
{
	return this->m_valeur;
}

/**
 * Remplacer valeur
 *
 * @param this
 * 		Cette instance
 * @param valeur
 * 		La valeur a remplacer
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_RemplacerValeur( NElementRequeteHTTP *this,
	const char *valeur )
{
	// Copie valeur
	char *copieValeur;

	// Dupliquer
	if( !( copieValeur = NLib_Chaine_Dupliquer( valeur ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NFALSE;
	}

	// Liberer
	NFREE( this->m_valeur );

	// Enregistrer valeur
	this->m_valeur = copieValeur;

	// OK
	return NTRUE;
}
