#include "../../../../../include/NHTTP.h"

// -------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Requete::NRequeteHTTP
// -------------------------------------------------------------

/**
 * Construire la requete HTTP
 *
 * @param requete
 * 		La requete recue
 * @param client
 * 		Le client ayant effectue la requete
 * @param identifiant
 * 		L'identifiant associe
 *
 * @return l'instance de la requete
 */
__ALLOC NRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire( const char *requete,
	const NClientServeur *client,
	NU32 identifiant )
{
	// Sortie
	__OUTPUT NRequeteHTTP *out;

	// Element requete
	NElementRequeteHTTP *element;

	// Element brut
	char *elementBrut;
	NU32 curseurElementBrut;

	// Iterateur
	NU32 i;

	// Curseur dans les donnees
	NU32 curseur = 0;

	// Buffer
	char *buffer;

	// Compteur
	NU32 compteur;

	// Chemin absolu
	char *cheminAbsoluRepertoire;
	NU32 tailleCheminAbsoluRepertoire;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NRequeteHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer identifiant
	out->m_identifiant = identifiant;

	// Lire le type de requete
	if( !( buffer = NLib_Chaine_LireJusqua( requete,
		' ',
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Interpreter le type de requete
	if( ( out->m_type = NHTTP_Commun_HTTP_NTypeRequeteHTTP_Interpreter( buffer ) ) >= NTYPES_REQUETE_HTTP )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Liberer
		NFREE( buffer );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Liberer
	NFREE( buffer );

	// Lire le fichier demande
	if( !( elementBrut = NLib_Chaine_LireJusqua( requete,
		' ',
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer la liste des arguments
	if( !( out->m_argumentGET = NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Construire( elementBrut ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( elementBrut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Lire fichier demande sans arguments
	curseurElementBrut = 0;
	if( !( out->m_element = NLib_Chaine_LireJusqua( elementBrut,
		'?',
		&curseurElementBrut,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NFREE( elementBrut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Liberer
	NFREE( elementBrut );

	// Lire version
	if( !( out->m_versionHTTP = NLib_Chaine_LireJusqua2( requete,
		NHTTP_Commun_HTTP_Traitement_ObtenirListeCaractereArretTraitement( ),
		NHTTP_NOMBRE_CARACTERE_ARRET_TRAITEMENT,
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NFREE( out->m_element );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire liste elements
	if( !( out->m_attribut = NLib_Memoire_NListe_Construire( (void ( ___cdecl * )( void * ))NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NFREE( out->m_versionHTTP );
		NFREE( out->m_element );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Lire donnees
	while( !NLib_Chaine_EstEOF( requete,
		curseur ) )
	{
		// Chercher prochaine lettre
		compteur = 0;
		while( !NLib_Chaine_EstEOF( requete,
			curseur )
			   && NHTTP_Commun_HTTP_Traitement_EstCaractereFinDeLigne( requete[ curseur ] ) )
		{
			// Incrementer curseur
			curseur++;

			// Verifier le nombre de caractere de fin de lignes lus
			if( compteur++ >= 2 )
				break;
		}

		// Verifier compteur
		if( compteur >= 2 )
			// On arrive a la partie concernant les donnees supplementaires
			break;

		// Lire ligne
		if( !( buffer = NLib_Chaine_LireJusqua2( requete,
			NHTTP_Commun_HTTP_Traitement_ObtenirListeCaractereArretTraitement( ),
			NHTTP_NOMBRE_CARACTERE_ARRET_TRAITEMENT,
			&curseur,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT( NERREUR_FILE_CANT_READ );

			// Detruire liste arguments
			NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );


			// Liberer
			NLib_Memoire_NListe_Detruire( &out->m_attribut );
			NFREE( out->m_versionHTTP );
			NFREE( out->m_element );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Supprimer \n et \r
		for( i = 0; i < NHTTP_NOMBRE_CARACTERE_ARRET_TRAITEMENT; i++ )
			NLib_Chaine_SupprimerCaractere( buffer,
				NHTTP_Commun_HTTP_Traitement_ObtenirListeCaractereArretTraitement( )[ i ] );

		// Verifier si ligne vide
		if( NLib_Chaine_EstVide( buffer ) )
		{
			// Liberer
			NFREE( buffer );

			// Quitter la boucle de lecture des elements
			break;
		}

		// Construire attribut
		if( !( element = NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire2( buffer ) ) )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire liste arguments
			NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

			// Liberer
			NFREE( buffer );
			NLib_Memoire_NListe_Detruire( &out->m_attribut );
			NFREE( out->m_versionHTTP );
			NFREE( out->m_element );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Liberer
		NFREE( buffer );

		// Ajouter l'element
		if( !NLib_Memoire_NListe_Ajouter( out->m_attribut,
			element ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Detruire liste arguments
			NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

			// Liberer
			NLib_Memoire_NListe_Detruire( &out->m_attribut );
			NFREE( out->m_versionHTTP );
			NFREE( out->m_element );
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// Lire les donnees
	if( !( out->m_data = NLib_Chaine_LireJusqua( requete,
		'\0',
		&curseur,
		NFALSE ) ) )
		// Taille a zero
		out->m_tailleData = 0;
	// On a des donnees
	else
		// Calculer taille donnees
		out->m_tailleData = (NU32)strlen( out->m_data );

	// Remplacer %XX par le caractere associe
	if( !( buffer = NHTTP_Commun_HTTP_Outil_RemplacerCaractereHTMLElementDemande( out->m_element ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out->m_versionHTTP );
		NFREE( out->m_element );
		NFREE( out->m_data );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Liberer ancien element
	NFREE( out->m_element );

	// Enregistrer
	out->m_element = buffer;

	// Dupliquer l'element initial
	if( !( out->m_elementInitial = NLib_Chaine_Dupliquer( out->m_element ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out->m_versionHTTP );
		NFREE( out->m_element );
		NFREE( out->m_data );
		NFREE( out );

		// Quitter
		return NULL;
	}

	/* Ajouter . devant l'url */
	// Allouer la memoire
	if( !( buffer = calloc( strlen( out->m_element ) + 2,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NFREE( out->m_elementInitial );
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out->m_versionHTTP );
		NFREE( out->m_element );
		NFREE( out->m_data );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Ajouter .
	buffer[ 0 ] = '.';
	memcpy( buffer + 1,
		out->m_element,
		strlen( out->m_element ) );

	// Liberer
	NFREE( out->m_element );

	// Enregistrer
	out->m_element = buffer;

	/* index.html si url vide */
	if( NLib_Chaine_Comparer( out->m_element,
		"./",
		NTRUE,
		0 ) )
		// Ajouter index.html
		if( !NLib_Memoire_AjouterData( &out->m_element,
			(NU32)strlen( out->m_element ),
			NHTTP_SERVEUR_HTTP_FICHIER_DEFAUT,
			(NU32)( strlen( NHTTP_SERVEUR_HTTP_FICHIER_DEFAUT ) + 1 ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_MEMORY );

			// Detruire liste arguments
			NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

			// Liberer
			NFREE( out->m_elementInitial );
			NLib_Memoire_NListe_Detruire( &out->m_attribut );
			NFREE( out->m_versionHTTP );
			NFREE( out->m_element );
			NFREE( out->m_data );
			NFREE( out );

			// Quitter
			return NULL;
		}

	/* Recuperer le chemin absolu de l'element */
	if( !( cheminAbsoluRepertoire = NLib_Module_Repertoire_ObtenirCheminAbsolu( "./" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NFREE( out->m_elementInitial );
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out->m_versionHTTP );
		NFREE( out->m_element );
		NFREE( out->m_data );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Calculer taille chemin repertoire
	tailleCheminAbsoluRepertoire = (NU32)strlen( cheminAbsoluRepertoire );

	// Allouer memoire
	if( !( buffer = calloc( tailleCheminAbsoluRepertoire // Chemin repertoire
			+ 1 // '/'
			+ strlen( out->m_element ) // Element
			+ 1,
		sizeof( char ) ) ) ) // '\0
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NFREE( cheminAbsoluRepertoire );
		NFREE( out->m_elementInitial );
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out->m_versionHTTP );
		NFREE( out->m_element );
		NFREE( out->m_data );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Composer lien final
	memcpy( buffer,
		cheminAbsoluRepertoire,
		tailleCheminAbsoluRepertoire );
	buffer[ tailleCheminAbsoluRepertoire ] = '/';
	memcpy( buffer + tailleCheminAbsoluRepertoire + 1,
		out->m_element,
		strlen( out->m_element ) );

	// Liberer
	NFREE( cheminAbsoluRepertoire );
	NFREE( out->m_element );

	// Enregistrer
	out->m_element = buffer;

	// Il s'agit d'un repertoire
	if( NLib_Module_Repertoire_EstRepertoire( out->m_element ) )
	{
		// Allouer buffer
		if( !( buffer = calloc( strlen( out->m_element )
				// '/'
				+ 1
				// index.html
				+ strlen( NHTTP_SERVEUR_HTTP_FICHIER_DEFAUT )
				// '\0'
				+ 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Detruire liste arguments
			NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

			// Liberer
			NFREE( out->m_elementInitial );
			NLib_Memoire_NListe_Detruire( &out->m_attribut );
			NFREE( out->m_versionHTTP );
			NFREE( out->m_element );
			NFREE( out->m_data );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Construire lien final
		curseur = 0;
		memcpy( buffer,
			out->m_element,
			strlen( out->m_element ) );
		curseur += strlen( out->m_element );
		buffer[ curseur ] = '/';
		curseur++;
		memcpy( buffer + curseur,
			NHTTP_SERVEUR_HTTP_FICHIER_DEFAUT,
			strlen( NHTTP_SERVEUR_HTTP_FICHIER_DEFAUT ) );
		curseur += strlen( NHTTP_SERVEUR_HTTP_FICHIER_DEFAUT );

		// Liberer
		NFREE( out->m_element );

		// Enregistrer
		out->m_element = buffer;
	}

	// Enregistrer
	out->m_client = client;

	// OK
	return out;
}

/**
 * Construire une requete vide
 *
 * @param type
 * 		Le type de requete
 * @param element
 * 		L'element a obtenir
 *
 * @return l'instance de la requete
 */
__ALLOC NRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTypeRequeteHTTP type,
	const char *element )
{
	// Sortie
	__OUTPUT NRequeteHTTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NRequeteHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire liste attributs
	if( !( out->m_attribut = NLib_Memoire_NListe_Construire( (void ( ___cdecl * )( void * ))NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire liste arguments GET
	if( !( out->m_argumentGET = NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Construire2( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier version
	if( !( out->m_versionHTTP = NLib_Chaine_Dupliquer( NHTTP_COMMUN_HTTP_VERSION_PROTOCOLE_HTTP ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer element
	if( !( out->m_element = NLib_Chaine_Dupliquer( element ) )
		|| !( out->m_elementInitial = NLib_Chaine_Dupliquer( element ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Detruire liste arguments
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NFREE( out->m_element );
		NFREE( out->m_versionHTTP );
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer type
	out->m_type = type;

	// OK
	return out;
}

/**
 * Construire une requete depuis une autre
 *
 * @param requete
 * 		La requete a dupliquer
 *
 * @return l'instance de la requete
 */
__ALLOC NRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire3( const NRequeteHTTP *requete )
{
	// Sortie
	__OUTPUT NRequeteHTTP *out;

	// Iterateur
	NU32 i;

	// Element
	const NElementRequeteHTTP *element;

	// Element copy
	NElementRequeteHTTP *elementCopy;

	// Allouer memoire
	if( !( out = calloc( 1,
		sizeof( NRequeteHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Sauvegarder
	out->m_type = requete->m_type;
	out->m_tailleData = requete->m_tailleData;
	out->m_identifiant = requete->m_identifiant;
	out->m_client = requete->m_client;

	// Dupliquer
	if( !( out->m_versionHTTP = NLib_Chaine_Dupliquer( requete->m_versionHTTP ) )
		|| !( out->m_element = NLib_Chaine_Dupliquer( requete->m_elementInitial ) )
		|| !( out->m_elementInitial = NLib_Chaine_Dupliquer( requete->m_elementInitial ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Liberer
		NFREE( out->m_element );
		NFREE( out->m_versionHTTP );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Il y a des donn\E9es?
	if( requete->m_data != NULL )
	{
		// Allouer la memoire
		if( !( out->m_data = calloc( out->m_tailleData + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( out->m_elementInitial );
			NFREE( out->m_element );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Copier
		memcpy( out->m_data,
			requete->m_data,
			requete->m_tailleData );
	}

	// Duplicate GET arguments list
	if( !( out->m_argumentGET = NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Construire3( requete->m_argumentGET ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Liberer
		NFREE( out->m_data );
		NFREE( out->m_elementInitial );
		NFREE( out->m_element );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire liste
	if( !( out->m_attribut = NLib_Memoire_NListe_Construire( (void ( ___cdecl * )( void * ))NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Detruire
		NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &out->m_argumentGET );

		// Liberer
		NFREE( out->m_data );
		NFREE( out->m_elementInitial );
		NFREE( out->m_element );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Ajouter attributs
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( requete->m_attribut ); i++ )
		// Obtenir element
		if( ( element = NLib_Memoire_NListe_ObtenirElementDepuisIndex( requete->m_attribut,
			i ) ) != NULL )
			// Dupliquer element
			if( ( elementCopy = NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire3( NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef( element ),
				NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirValeur( element ) ) ) != NULL )
				// Ajouter element
				NLib_Memoire_NListe_Ajouter( out->m_attribut,
					elementCopy );

	// OK
	return out;
}

/**
 * Detruire la requete
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( NRequeteHTTP **this )
{
	// Detruire liste
	NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( &(*this)->m_argumentGET );
	NLib_Memoire_NListe_Detruire( &(*this)->m_attribut );

	// Liberer
	NFREE( (*this)->m_data );
	NFREE( (*this)->m_elementInitial );
	NFREE( (*this)->m_element );
	NFREE( (*this)->m_versionHTTP );
	NFREE( (*this) );
}

/**
 * Obtenir type requete
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de la requete
 */
NTypeRequeteHTTP NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( const NRequeteHTTP *this )
{
	return this->m_type;
}

/**
 * Obtenir liste elements
 *
 * @param this
 * 		Cette instance
 *
 * @return la liste des elements
 */
const NListe *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirListeElement( const NRequeteHTTP *this )
{
	return this->m_attribut;
}

/**
 * Obtenir attribut
 *
 * @param this
 * 		Cette instance
 * @param type
 * 		Le type d'attribut
 *
 * @return l'attribut (NULL si introuvable)
 */
const NElementRequeteHTTP *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirAttribut( const NRequeteHTTP *this,
	NMotClefRequeteHTTP type )
{
	// Iterateur
	NU32 i = 0;

	// Chercher
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_attribut ); i++ )
		// Verifier
		if( NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
			i ) ) == type )
			// OK
			return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
				i );

	// Introuvable
	return NULL;
}

/**
 * Obtenir element demande
 *
 * @param this
 * 		Cette instance
 *
 * @return l'element demande
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElement( const NRequeteHTTP *this )
{
	return this->m_element;
}

/**
 * Obtenir element demande avant traitement chaine
 *
 * @param this
 * 		Cette instance
 *
 * @return l'element demande avant traitement
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( const NRequeteHTTP *this )
{
	return this->m_elementInitial;
}

/**
 * Obtenir data
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirData( const NRequeteHTTP *this )
{
	return this->m_data;
}

/**
 * Obtenir la taille des donnees
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille des donnees
 */
NU32 NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirTailleData( const NRequeteHTTP *this )
{
	return this->m_tailleData;
}

/**
 * Loguer requete
 *
 * @param this
 * 		Cette instance
 * @param cacheLog
 * 		Le cache log (NCacheLog*)
 * @param ipClient
 * 		L'ip du client
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Loguer( const NRequeteHTTP *this,
	void *cacheLog,
	const char *ipClient )
{
	// Chaine loguee
	char *log = NULL;

	// Iterateur
	NU32 i;

	// Maccro ajout data
#define AJOUTER_DATA_LOG( texte ) \
	if( !NLib_Memoire_AjouterData2( &log, \
		( ( log != NULL ) ? \
			(NU32)strlen( log ) \
			: 0 ), \
		texte ) ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED ); \
 \
		/* Liberer */ \
		NFREE( log ); \
 \
 		/* Quitter */ \
 		return NFALSE; \
	}

#define CHANGEMENT_LIGNE_LOG		"\n\t\t"
#define CHANGEMENT_LIGNE_LOG_SIMPLE	"\n\t"

	// Noter base
	AJOUTER_DATA_LOG( CHANGEMENT_LIGNE_LOG_SIMPLE"Methode: " );
	AJOUTER_DATA_LOG( NHTTP_Commun_HTTP_NTypeRequeteHTTP_ObtenirMotClef( this->m_type ) );
	AJOUTER_DATA_LOG( CHANGEMENT_LIGNE_LOG_SIMPLE"Version: " );
	AJOUTER_DATA_LOG( this->m_versionHTTP );
	AJOUTER_DATA_LOG( CHANGEMENT_LIGNE_LOG_SIMPLE"Veut: " );
	AJOUTER_DATA_LOG( this->m_element );

	// Noter attributs
	AJOUTER_DATA_LOG( CHANGEMENT_LIGNE_LOG_SIMPLE"Avec:"CHANGEMENT_LIGNE_LOG );
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_attribut ); i++ )
	{
		// Nom attribut
		AJOUTER_DATA_LOG( NHTTP_Commun_HTTP_NMotClefRequeteHTTP_ObtenirMotClef( NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
			i ) ) ) );

		// Separation
		AJOUTER_DATA_LOG( ": [" );

		// Attribut
		AJOUTER_DATA_LOG( NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirValeur( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
			i ) ) );

		// Retour ligne
		AJOUTER_DATA_LOG( "]"CHANGEMENT_LIGNE_LOG );
	}

	// Final
	AJOUTER_DATA_LOG( "\n" );

#undef AJOUTER_DATA_LOG

	// Ajouter log
	if( !NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree2( cacheLog,
		NTYPE_ENTREE_CACHE_LOG_HTTP_EVENEMENT,
		NTYPE_ENTREE_EVENEMENT_REQUETE,
		log,
		ipClient,
		this->m_identifiant ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Liberer
		NFREE( log );

		// Quitter
		return NFALSE;
	}

	// Liberer
	NFREE( log );

	// OK
	return NTRUE;
}

/**
 * Obtenir identifiant
 *
 * @param this
 * 		Cette instance
 *
 * @return l'identifiant
 */
NU32 NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirIdentifiant( const NRequeteHTTP *this )
{
	return this->m_identifiant;
}

/**
 * Ajouter data
 *
 * @param this
 * 		Cette instance
 * @param data
 * 		Les donnees a ajouter
 * @param tailleData
 * 		La taille des donnees a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( NRequeteHTTP *this,
	const char *data,
	NU32 tailleData )
{
	// Allouer et ajouter
	if( !NLib_Memoire_AjouterData( &this->m_data,
		this->m_tailleData,
		data,
		tailleData ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Incrementer taille
	this->m_tailleData += tailleData;

	// OK
	return NTRUE;
}

/**
 * Ajouter un attribut
 *
 * @param this
 * 		Cette instance
 * @param typeAttribut
 * 		Le type d'attribut
 * @param valeurAttribut
 * 		La valeur d'attribut
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( NRequeteHTTP *this,
	NMotClefRequeteHTTP typeAttribut,
	const char *valeurAttribut )
{
	// Element requete
	NElementRequeteHTTP *attribut;

	// Construire attribut
	if( !( attribut = NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_Construire3( typeAttribut,
		valeurAttribut ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter attribut
	return NLib_Memoire_NListe_Ajouter( this->m_attribut,
		attribut );
}

/**
 * Preparer la creation du packet (privee)
 *
 * @param this
 * 		Cette instance
 * @param fichier
 * 		Le fichier HTTP
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_PreparerCreationPacket( NRequeteHTTP *this,
	const NFichierHTTP *fichier )
{
	// Buffer
	char buffer[ 256 ] = { 0, };

	// Taille specifiee?
	if( fichier != NULL
		&& NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirAttribut( this,
			NMOT_CLEF_REQUETE_HTTP_KEYWORD_CONTENT_LENGTH ) == NULL
		&& NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTaille( fichier ) > 0 )
	{
		// Inscrire la taille
		snprintf( buffer,
			256,
			"%llu",
			NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTaille( fichier ) );

		// Construire attribut
		if( !NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( this,
			NMOT_CLEF_REQUETE_HTTP_KEYWORD_CONTENT_LENGTH,
			buffer ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}
	}

	// Agent specifiee?
	if( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirAttribut( this,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_USER_AGENT ) == NULL )
		// Construire attribut
		if( !NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( this,
			NMOT_CLEF_REQUETE_HTTP_KEYWORD_USER_AGENT,
			NHTTP_CLIENT_NOM_CLIENT ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}

	// Donnees acceptee specifiee
	if( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirAttribut( this,
		NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT ) == NULL )
		// Construire attribut
		if( !NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( this,
			NMOT_CLEF_REQUETE_HTTP_KEYWORD_ACCEPT,
			"*/*" ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}

	// OK
	return NTRUE;
}

/**
 * Ajouter code reponse dans packet reponse (privee)
 *
 * @param this
 * 		Cette instance
 * @param packet
 * 		Le packet dans lequel ajouter
 */
__PRIVATE NBOOL NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_InitialiserEntetePacketRequete( NRequeteHTTP *this,
	NPacketHTTP *packet )
{
	if(
	// Ajouter methode
		!NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
		NHTTP_Commun_HTTP_NTypeRequeteHTTP_ObtenirMotClef( this->m_type ) )

	// Separer
		|| !NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			" " )

	// Ajouter fichier a obtenir
		|| !NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			this->m_element )

	// Separer
		|| !NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
		" " )

	// Ajouter version http
		|| !NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			NHTTP_COMMUN_HTTP_VERSION_PROTOCOLE_HTTP )


	// Ajouter retour ligne
		|| !NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			"\r\n" ) )
		return NFALSE;

	// OK
	return NTRUE;
}

/**
 * Creer le packet
 *
 * @param this
 * 		Cette instance
 *
 * @return le packet
 */
__ALLOC NPacketPersonnalise *NHTTP_Commun_Traitement_Requete_NRequeteHTTP_CreerPacket( const NRequeteHTTP *this )
{
	// Sortie
	__OUTPUT NPacketPersonnalise *out;

	// Packet
	NPacketHTTP *packet;

	// Iterateur
	NU32 i;

	// Fichier HTTP
	NFichierHTTP *fichier;

	// Donnees dupliquees
	char *copieData;

	// Dupliquer donnees
		// Allouer memoire
			if( !( copieData = calloc( this->m_tailleData + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NULL;
			}
		// Copier
			if( this->m_data != NULL )
				memcpy( copieData,
					this->m_data,
					this->m_tailleData );

	// Construire le fichier
	if( !( fichier = NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Construire( NTYPE_FICHIER_HTTP_TEXTE,
		NTYPE_FLUX_TEXTE,
		copieData,
		NULL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Preparer la creation
	if( !NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_PreparerCreationPacket( (NRequeteHTTP*)this,
		fichier ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Detruire fichier
		NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( packet = NHTTP_Commun_Packet_NPacketHTTP_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Etat reponse
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_InitialiserEntetePacketRequete( (NRequeteHTTP*)this,
		packet );

	// Remplir l'header
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_attribut ); i++ )
	{
		// Ajouter mot clef
		NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef2( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
				i ) ) );

		// Ajouter ": "
		NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			": " );

		// Ajouter valeur
		NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirValeur( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
				i ) ) );

		// Ajouter "\n"
		NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			"\r\n" );
	}

	// Ajouter separation contenu reponse
	NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
		"\r\n" );

	// Definir le fichier
	NHTTP_Commun_Packet_NPacketHTTP_DefinirFichier( packet,
		fichier );

	// Construire le packet final
	if( !( out = NLib_Module_Reseau_Packet_NPacketPersonnalise_Construire( (void *( ___cdecl * )( const void * ))NHTTP_Commun_Packet_NPacketHTTP_Construire2,
		(void ( ___cdecl * )( void * ))NHTTP_Commun_Packet_NPacketHTTP_Detruire,
		packet ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NHTTP_Commun_Packet_NPacketHTTP_Detruire( &packet );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Supprimer attribut
 *
 * @param this
 * 		Cette instance
 * @param typeAttribut
 * 		Le type d'attribut
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_Traitement_Requete_NRequeteHTTP_SupprimerAttribut( NRequeteHTTP *this,
	NMotClefRequeteHTTP typeAttribut )
{
	// Iterateur
	NU32 i = 0;

	// Attribut
	const NElementRequeteHTTP *attribut;

	// Chercher attribut
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_attribut ); i++ )
		// Obtenir attribut
		if( ( attribut = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
			i ) ) != NULL )
			// Est ce le bon attribut?
			if( NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef( attribut ) == typeAttribut )
			{
				// Supprimer element
				NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_attribut,
					i );

				// OK
				return NTRUE;
			}

	// Introuvable
	return NFALSE;
}

/**
 * Remplacer attribut
 *
 * @param this
 * 		Cette instance
 * @param typeAttribut
 * 		Le type d'attribut
 * @param valeurAttribut
 * 		La nouvelle valeur de l'attribut
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_Traitement_Requete_NRequeteHTTP_RemplacerAttribut( NRequeteHTTP *this,
	NMotClefRequeteHTTP typeAttribut,
	const char *valeurAttribut )
{
	// Iterateur
	NU32 i = 0;

	// Attribut
	NElementRequeteHTTP *attribut;

	// Chercher attribut
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_attribut ); i++ )
		// Obtenir attribut
		if( ( attribut = (NElementRequeteHTTP*)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
			i ) ) != NULL )
			// Est ce le bon attribut?
			if( NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_ObtenirClef( attribut ) == typeAttribut )
			{
				// Remplacer valeur
				NHTTP_Commun_HTTP_Traitement_Requete_NElementRequeteHTTP_RemplacerValeur( attribut,
					valeurAttribut );

				// OK
				return NTRUE;
			}

	// Introuvable
	return NFALSE;
}
