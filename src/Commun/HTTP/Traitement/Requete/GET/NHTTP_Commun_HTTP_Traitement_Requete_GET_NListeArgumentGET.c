#include "../../../../../../include/NHTTP.h"

// -----------------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Requete::GET::NListeArgumentGET
// -----------------------------------------------------------------------

/**
 * Construire la liste d'arguments
 *
 * @param element
 * 		L'element demande
 *
 * @return l'instance
 */
__ALLOC NListeArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Construire( const char *element )
{
	// Sortie
	__OUTPUT NListeArgumentGET *out;

	// Curseur
	NU32 curseur = 0;

	// Buffer
	char *buffer;
	NU32 curseurBuffer;

	// Nom
	char *nom;

	// Valeur
	char *valeur;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NListeArgumentGET ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Construire liste
	if( !( out->m_argument = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Se placer au '?'
	if( NLib_Chaine_PlacerAuCaractere( element,
		'?',
		&curseur ) )
	{
		// Lire jusqu'au &
		while( ( buffer = NLib_Chaine_LireJusqua( element,
			'&',
			&curseur,
			NFALSE ) ) != NULL )
		{
			// Verifier contenu
			if( strlen( buffer ) <= 0
				|| NLib_Chaine_EstVide( buffer ) )
			{
				// Liberer
				NFREE( buffer );

				// Stopper
				break;
			}

			// Curseur a zero
			curseurBuffer = 0;

			// Lire nom
			if( !( nom = NLib_Chaine_LireJusqua( buffer,
				'=',
				&curseurBuffer,
				NFALSE ) ) )
			{
				// Liberer
				NFREE( buffer );

				// Stopper
				break;
			}

			// Lire valeur
			if( !( valeur = NLib_Chaine_LireJusqua( buffer,
				'\0',
				&curseurBuffer,
				NFALSE ) ) )
			{
				// Liberer
				NFREE( nom );
				NFREE( buffer );

				// Stopper
				break;
			}

			// Verifier nom/valeur
			if( strlen( nom ) <= 0
				|| NLib_Chaine_EstVide( nom )
				|| strlen( valeur ) <= 0
				|| NLib_Chaine_EstVide( valeur ) )
			{
				// Liberer
				NFREE( valeur );
				NFREE( nom );
				NFREE( buffer );

				// Stopper
				break;
			}

			// Construire argument
			if( !NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_AjouterArgument( out,
				nom,
				valeur ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_COPY );

				// Detruire liste
				NLib_Memoire_NListe_Detruire( &out->m_argument );

				// Liberer
				NFREE( valeur );
				NFREE( nom );
				NFREE( buffer );
				NFREE( out );

				// Quitter
				return NULL;
			}

			// Liberer
			NFREE( valeur );
			NFREE( nom );
			NFREE( buffer );
		}
	}

	// OK
	return out;
}

/**
 * Construire liste d'arguments vide
 *
 * @return l'instance
 */
__ALLOC NListeArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Construire2( void )
{
	// Sortie
	__OUTPUT NListeArgumentGET *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NListeArgumentGET ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Construire liste
	if( !( out->m_argument = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Dupliquer liste d'arguments
 *
 * @param listeArgument
 * 		La liste d'arguments
 *
 * @return l'instance
 */
__ALLOC NListeArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Construire3( const NListeArgumentGET *listeArgumentGET )
{
	// Sortie
	__OUTPUT NListeArgumentGET *out;

	// Argument
	const NArgumentGET *argument;

	// Argument copy
	NArgumentGET *argumentCopy;

	// Iterateur
	NU32 i;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NListeArgumentGET ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire liste
	if( !( out->m_argument = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Ajouter arguments
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( listeArgumentGET->m_argument ); i++ )
		// Dupliquer argument
		if( ( argument = NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Construire2( NLib_Memoire_NListe_ObtenirElementDepuisIndex( listeArgumentGET->m_argument,
			i ) ) ) != NULL )
			// Duplicate
			if( ( argumentCopy = NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Construire2( argument ) ) != NULL )
				// Ajouter argument
				NLib_Memoire_NListe_Ajouter( out->m_argument,
					argumentCopy );


	// OK
	return out;
}

/**
 * Detruire la liste d'arguments
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_Detruire( NListeArgumentGET **this )
{
	// Detruire liste
	NLib_Memoire_NListe_Detruire( &(*this)->m_argument );

	// Liberer
	NFREE( (*this) );
}

/**
 * Obtenir le nombre d'arguments
 *
 * @param this
 * 		Cette instance
 *
 * @return le nombre d'arguments
 */
NU32 NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_ObtenirNombreArgument( const NListeArgumentGET *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_argument );
}

/**
 * Obtenir un argument
 *
 * @param this
 * 		Cette instance
 * @param index
 * 		L'index de l'argument
 *
 * @return l'argument
 */
const NArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_ObtenirArgument( const NListeArgumentGET *this,
	NU32 index )
{
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_argument,
		index );
}

/**
 * Ajouter argument
 *
 * @param this
 * 		Cette instance
 * @param nom
 * 		Le nom de l'argument
 * @param valeur
 * 		La valeur de l'argument
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Requete_GET_NListeArgumentGET_AjouterArgument( NListeArgumentGET *this,
	const char *nom,
	const char *valeur )
{
	// Argument
	NArgumentGET *argumentGET;

	// Construire argument
	if( !( argumentGET = NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Construire( nom,
		valeur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter
	return NLib_Memoire_NListe_Ajouter( this->m_argument,
		argumentGET );
}


