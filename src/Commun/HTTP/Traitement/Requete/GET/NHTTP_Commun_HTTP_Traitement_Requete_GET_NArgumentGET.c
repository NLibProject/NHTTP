#include "../../../../../../include/NHTTP.h"

// ------------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Requete::GET::NArgumentGET
// ------------------------------------------------------------------

/**
 * Construire argument
 *
 * @param nom
 * 		Le nom
 * @param valeur
 * 		La valeur
 *
 * @return l'instance de l'argument
 */
__ALLOC NArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Construire( const char *nom,
	const char *valeur )
{
	// Sortie
	__OUTPUT NArgumentGET *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NArgumentGET ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Dupliquer
	if( !( out->m_valeur = NLib_Chaine_Dupliquer( valeur ) )
		|| !( out->m_nom = NLib_Chaine_Dupliquer( nom ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Liberer
		NFREE( out->m_valeur );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Dupliquer argument
 *
 * @param argument
 * 		L'argument
 *
 * @return l'instance de l'argument
 */
__ALLOC NArgumentGET *NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Construire2( const NArgumentGET *argument )
{
	// Sortie
	__OUTPUT NArgumentGET *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NArgumentGET ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Duplicate
	if( !( out->m_nom = NLib_Chaine_Dupliquer( argument->m_nom ) )
		|| !( out->m_valeur = NLib_Chaine_Dupliquer( argument->m_valeur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Liberer
		NFREE( out->m_nom );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire l'argument
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_Detruire( NArgumentGET **this )
{
	NFREE( (*this)->m_valeur );
	NFREE( (*this)->m_nom );
	NFREE( (*this) );
}

/**
 * Obtenir nom
 *
 * @param this
 * 		Cette instance
 *
 * @return le nom
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_ObtenirNom( const NArgumentGET *this )
{
	return this->m_nom;
}

/**
 * Obtenir la valeur
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur
 */
const char *NHTTP_Commun_HTTP_Traitement_Requete_GET_NArgumentGET_ObtenirValeur( const NArgumentGET *this )
{
	return this->m_valeur;
}