#include "../../../../../include/NHTTP.h"

// -------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Reponse::NReponseHTTP
// -------------------------------------------------------------

/**
 * Construire la reponse
 *
 * @param code
 * 		Le code reponse
 * @param identifiantRequete
 * 		L'identifiant de la requete associee
 *
 * @return l'instance de la reponse
 */
__ALLOC NReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire( NHTTPCode code,
	NU32 identifiantRequete )
{
	// Sortie
	__OUTPUT NReponseHTTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NReponseHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer liste elements
	if( !( out->m_attribut = NLib_Memoire_NListe_Construire( (void ( ___cdecl * )( void * ))NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_code = code;
	out->m_identifiantRequete = identifiantRequete;

	// OK
	return out;
}

/**
 * Construire reponse depuis reponse texte
 *
 * @param reponse
 * 		La reponse recue
 *
 * @return l'instance de la reponse
 */
__ALLOC NReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire2( const char *reponse )
{
	// Sortie
	__OUTPUT NReponseHTTP *out;

	// Curseur
	NU32 curseur = 0;

	// Code texte
	char *codeTexte;

	// Compteur
	NU32 compteur;

	// Buffer
	char *buffer;

	// Iterateur
	NU32 i;

	// Element
	NElementReponseHTTP *element;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NReponseHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer liste elements
	if( !( out->m_attribut = NLib_Memoire_NListe_Construire( (void ( ___cdecl * )( void * ))NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Detruire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Lire version
	if( !( out->m_version = NLib_Chaine_LireJusqua( reponse,
		' ',
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Detruire
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Lire code reponse
	if( !( codeTexte = NLib_Chaine_LireJusqua( reponse,
		' ',
		&curseur,
		NFALSE ) )
		|| !NLib_Chaine_EstUnNombreEntier( codeTexte,
			10 ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Detruire
		NFREE( out->m_version );
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Convertir code
	if( ( out->m_code = NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeDepuisNumero( (NU32)strtol( codeTexte,
		NULL,
		10 ) ) ) >= NHTTP_CODES )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Detruire
		NFREE( codeTexte );
		NFREE( out->m_version );
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Liberer
	NFREE( codeTexte );

	// Lire chaine associee au code
	if( !( codeTexte = NLib_Chaine_LireJusqua2( reponse,
		NHTTP_Commun_HTTP_Traitement_ObtenirListeCaractereArretTraitement( ),
		NHTTP_NOMBRE_CARACTERE_ARRET_TRAITEMENT,
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Detruire
		NFREE( out->m_version );
		NLib_Memoire_NListe_Detruire( &out->m_attribut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Verifier si le message correspond a ce qui est attendu
	if( !NLib_Chaine_Comparer( NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeTexte( out->m_code ),
		codeTexte,
		NTRUE,
		0 ) )
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

	// Liberer
	NFREE( codeTexte );

	// Lire attributs
	while( !NLib_Chaine_EstEOF( reponse,
		curseur ) )
	{
		// Chercher prochaine lettre
		compteur = 0;
		while( !NLib_Chaine_EstEOF( reponse,
			curseur )
				&& NHTTP_Commun_HTTP_Traitement_EstCaractereFinDeLigne( reponse[ curseur ] ) )
		{
			// Incrementer curseur
			curseur++;

			// Verifier le nombre de caractere de fin de lignes lus
			if( compteur++ >= 2 )
				break;
		}

		// Verifier compteur
		if( compteur >= 2 )
			// On arrive a la partie concernant les donnees supplementaires
			break;

		// Lire ligne
		if( !( buffer = NLib_Chaine_LireJusqua2( reponse,
			NHTTP_Commun_HTTP_Traitement_ObtenirListeCaractereArretTraitement( ),
			NHTTP_NOMBRE_CARACTERE_ARRET_TRAITEMENT,
			&curseur,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT( NERREUR_FILE_CANT_READ );

			// Liberer
			NFREE( out->m_version );
			NLib_Memoire_NListe_Detruire( &out->m_attribut );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Supprimer \n et \r
		for( i = 0; i < NHTTP_NOMBRE_CARACTERE_ARRET_TRAITEMENT; i++ )
			NLib_Chaine_SupprimerCaractere( buffer,
				NHTTP_Commun_HTTP_Traitement_ObtenirListeCaractereArretTraitement( )[ i ] );

		// Verifier si ligne vide
		if( NLib_Chaine_EstVide( buffer ) )
		{
			// Liberer
			NFREE( buffer );

			// Quitter la boucle de lecture des elements
			break;
		}

		// Construire attribut
		if( !( element = NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire2( buffer ) ) )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( buffer );
			NFREE( out->m_version );
			NLib_Memoire_NListe_Detruire( &out->m_attribut );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Liberer
		NFREE( buffer );

		// Ajouter l'element
		if( !NLib_Memoire_NListe_Ajouter( out->m_attribut,
			element ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Liberer
			NFREE( out->m_version );
			NLib_Memoire_NListe_Detruire( &out->m_attribut );
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// Lire les donnees
	if( ( buffer = NLib_Chaine_LireJusqua( reponse,
		'\0',
		&curseur,
		NFALSE ) ) != NULL )
	{
		// Definir fichier
		if( !NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier2( out,
			buffer ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Detruire
			NFREE( buffer );
			NFREE( out->m_version );
			NLib_Memoire_NListe_Detruire( &out->m_attribut );
			NFREE( out );

			// Quit
			return NULL;
		}

		// Liberer
		NFREE( buffer );
	}

	// OK
	return out;
}

/**
 * Detruire la reponse
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( NReponseHTTP **this )
{
	// Liberer version (qui peut etre nulle)
	NFREE( (*this)->m_version );

	// Detruire liste attributs
	NLib_Memoire_NListe_Detruire( &(*this)->m_attribut );

	// Fichier declare?
	if( (*this)->m_fichier != NULL )
		// Detruire fichier
		NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Detruire( &( *this )->m_fichier );

	// Liberer
	NFREE( (*this) );
}

/**
 * Definir le fichier
 *
 * @param this
 * 		Cette instance
 * @param fichier
 * 		Le fichier de reponse
 */
void NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier( NReponseHTTP *this,
	__WILLBEOWNED NFichierHTTP *fichier )
{
	// Detruire ancien fichier si present
	if( this->m_fichier != NULL )
		// Detruire
		NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Detruire( &this->m_fichier );

	// Enregistrer
	this->m_fichier = fichier;
}

/**
 * Definir le fichier en tant que texte
 *
 * @param this
 * 		Cette instance
 * @param texte
 * 		Le texte
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier2( NReponseHTTP *this,
	const char *texte )
{
	// Fichier
	NFichierHTTP *fichierReponseHTTP;

	// Copie texte
	char *copieTexte;

	// Dupliquer texte
	if( !( copieTexte = NLib_Chaine_Dupliquer( texte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Construire fichier contenant du texte
	if( !( fichierReponseHTTP = NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Construire( NTYPE_FICHIER_HTTP_TEXTE,
		NTYPE_FLUX_HTML,
		copieTexte,
		NULL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Definir fichier
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier( this,
		fichierReponseHTTP );

	// OK
	return NTRUE;
}

/**
 * Definir le fichier en tant que fichier binaire
 *
 * @param this
 * 		Cette instance
 * @param fichier
 * 		Le fichier
 * @param lien
 * 		Le lien vers le fichier
 * @param typeFlux
 * 		Le type de flux
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier3( NReponseHTTP *this,
	__WILLBEOWNED NFichierBinaire *fichier,
	const char *lien,
	NTypeFlux typeFlux )
{
	// Fichier
	NFichierHTTP *fichierReponseHTTP;

	// Construire fichier contenant du texte
	if( !( fichierReponseHTTP = NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Construire( NTYPE_FICHIER_HTTP_FICHIER,
		typeFlux,
		fichier,
		lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Definir
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier( this,
		fichierReponseHTTP );

	// OK
	return NTRUE;
}

/**
 * Ajouter un element a la liste des attributs
 *
 * @param this
 * 		Cette instance
 * @param motClefReponse
 * 		Le type de mot clef
 * @param valeur
 * 		La valeur a ajouter
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_AjouterAttribut( NReponseHTTP *this,
	NMotClefReponseHTTP motClefReponse,
	const char *valeur )
{
	// Element
	NElementReponseHTTP *element;

	// Construire element
	if( !( element = NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire( motClefReponse,
		valeur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ajouter
	return NLib_Memoire_NListe_Ajouter( this->m_attribut,
		element );
}

/**
 * Obtenir code
 *
 * @param this
 * 		Cette instance
 *
 * @return le code reponse HTTP
 */
NHTTPCode NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCode( const NReponseHTTP *this )
{
	return this->m_code;
}

/**
 * Obtenir attribut depuis clef
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef pour laquelle on cherche la valeur
 *
 * @return la valeur associee a clef (NULL si inexistante)
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut( const NReponseHTTP *this,
	NMotClefReponseHTTP clef )
{
	return NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut2( this,
		clef,
		0 );
}

/**
 * Obtenir attribut depuis clef
 *
 * @param this
 * 		Cette instance
 * @param clef
 * 		La clef pour laquelle on cherche la valeur
 * @param index
 * 		L'index de l'attribut (si plusieurs)
 *
 * @return la valeur associee a clef (NULL si inexistante)
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut2( const NReponseHTTP *this,
	NMotClefReponseHTTP clef,
	NU32 index )
{
	// Iterateur
	NU32 i = 0;

	// Chercher
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_attribut ); i++ )
		// Comparer
		if( NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirClef( (NElementReponseHTTP *)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
			i ) ) == clef )
			if( index-- == 0 )
				return NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirValeur( (NElementReponseHTTP *)NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
					i ) );

	// Introuvable
	return NULL;
}

/**
 * Obtenir donnee
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees
 */
__ALLOC char *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirCopieData( const NReponseHTTP *this )
{
	// Sortie
	__OUTPUT char *out = NULL;

	// On a donnees
	if( this->m_fichier != NULL )
	{
		// Recuperer les donnees
		switch( NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTypeData( this->m_fichier ) )
		{
			case NTYPE_FICHIER_HTTP_FICHIER:
				if( !( out = NLib_Fichier_NFichierBinaire_LireTout( (NFichierBinaire*)NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirData( this->m_fichier ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_COPY );

					// Quitter
					return NULL;
				}
				break;
			case NTYPE_FICHIER_HTTP_TEXTE:
				// Dupliquer texte
				if( !( out = NLib_Chaine_Dupliquer( NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirData( this->m_fichier ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_COPY );

					// Quitter
					return NULL;
				}
				break;

			default:
				// Notifier
				NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );
				break;
		}
	}

	// OK
	return out;
}

/**
 * Preparer la creation du packet (privee)
 *
 * @param this
 * 		Cette instance
 *
 * @return si l'operation s'est bien passee
 */
__PRIVATE NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_PreparerCreationPacket( NReponseHTTP *this )
{
	// Attribut
	NElementReponseHTTP *attribut;

	// Buffer
	char buffer[ 256 ] = { 0, };

	// Taille specifiee?
	if( this->m_fichier != NULL
		&& NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut( this,
			NMOT_CLEF_REPONSE_HTTP_CONTENT_LENGTH ) == NULL )
	{
		// Inscrire la taille
		snprintf( buffer,
			256,
			"%llu",
			NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTaille( this->m_fichier ) );

		// Construire attribut
		if( !( attribut = NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire( NMOT_CLEF_REPONSE_HTTP_CONTENT_LENGTH,
			buffer ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}

		// Ajouter le type de flux
		if( !NLib_Memoire_NListe_Ajouter( this->m_attribut,
			attribut ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Quitter
			return NFALSE;
		}
	}

	// Gestion connexion specifiee?
	if( NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut( this,
		NMOT_CLEF_REPONSE_HTTP_CONNECTION ) == NULL )
	{
		// Construire attribut
		if( !( attribut = NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire( NMOT_CLEF_REPONSE_HTTP_CONNECTION,
			"close" ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}

		// Ajouter le type de flux
		if( !NLib_Memoire_NListe_Ajouter( this->m_attribut,
			attribut ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Quitter
			return NFALSE;
		}
	}

	// Agent specifiee?
	if( NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut( this,
		NMOT_CLEF_REPONSE_HTTP_SERVER ) == NULL )
	{
		// Construire attribut
		if( !( attribut = NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire( NMOT_CLEF_REPONSE_HTTP_SERVER,
			NHTTP_SERVEUR_NOM_SERVEUR ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}

		// Ajouter le type de flux
		if( !NLib_Memoire_NListe_Ajouter( this->m_attribut,
			attribut ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Quitter
			return NFALSE;
		}
	}

	// Type de flux specifie?
	if( this->m_fichier != NULL
		&& NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirAttribut( this,
		NMOT_CLEF_REPONSE_HTTP_CONTENT_TYPE ) == NULL )
	{
		// Construire attribut
		if( !( attribut = NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire( NMOT_CLEF_REPONSE_HTTP_CONTENT_TYPE,
			NHTTP_Commun_HTTP_Mimetype_NTypeFlux_ObtenirType( NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTypeFlux( this->m_fichier ) ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}

		// Ajouter le type de flux
		if( !NLib_Memoire_NListe_Ajouter( this->m_attribut,
			attribut ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN );

			// Quitter
			return NFALSE;
		}
	}

	// OK
	return NTRUE;
}

/**
 * Ajouter code reponse dans packet reponse (privee)
 *
 * @param this
 * 		Cette instance
 * @param packet
 * 		Le packet dans lequel ajouter
 */
__PRIVATE NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_InitialiserEntetePacketReponse( NReponseHTTP *this,
	NPacketHTTP *packet )
{
	// Buffer
	char buffer[ 32 ] = { 0, };

	// Ajouter version http
	if( !NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
		NHTTP_COMMUN_HTTP_VERSION_PROTOCOLE_HTTP )
		|| !NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			" " ))
		return NFALSE;

	// Ajouter code http
	snprintf( buffer,
		32,
		"%d ",
		NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeNumero( this->m_code ) );
	if( !NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
		buffer ) )
		return NFALSE;

	// Ajouter message associe au code
	if( !NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
		NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeTexte( this->m_code ) ) )
		return NFALSE;

	// Ajouter retour ligne
	NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
		"\n" );

	// OK
	return NTRUE;
}

/**
 * Construire packet depuis reponse (NPacketPersonnalise contenant NPacketHTTP)
 *
 * @param this
 * 		Cette instance
 *
 * @return le packet
 */
__ALLOC NPacketPersonnalise *NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_CreerPacket( NReponseHTTP *this )
{
	// Sortie
	__OUTPUT NPacketPersonnalise *out;

	// Packet
	NPacketHTTP *packet;

	// Iterateur
	NU32 i;

	// Preparer la creation
	if( !NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_PreparerCreationPacket( this ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( packet = NHTTP_Commun_Packet_NPacketHTTP_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Etat reponse
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_InitialiserEntetePacketReponse( this,
		packet );

	// Remplir l'header
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_attribut ); i++ )
	{
		// Ajouter mot clef
		NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirClef2( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
				i ) ) );

		// Ajouter ": "
		NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			": " );

		// Ajouter valeur
		NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirValeur( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
				i ) ) );

		// Ajouter "\n"
		NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
			"\r\n" );
	}

	// Ajouter separation contenu reponse
	NHTTP_Commun_Packet_NPacketHTTP_AjouterTexteHeader( packet,
		"\r\n" );

	// Definir le fichier
	NHTTP_Commun_Packet_NPacketHTTP_DefinirFichier( packet,
		this->m_fichier );

	// Oublier le fichier
	NDISSOCIER_ADRESSE( this->m_fichier );

	// Construire le packet final
	if( !( out = NLib_Module_Reseau_Packet_NPacketPersonnalise_Construire( (void *( ___cdecl * )( const void * ))NHTTP_Commun_Packet_NPacketHTTP_Construire2,
		(void ( ___cdecl * )( void * ))NHTTP_Commun_Packet_NPacketHTTP_Detruire,
		packet ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NHTTP_Commun_Packet_NPacketHTTP_Detruire( &packet );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Loguer reponse
 *
 * @param this
 * 		Cette instance
 * @param cacheLog
 * 		Le cache log (NCacheLogHTTP*)
 * @param ipClient
 * 		L'ip du client
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Loguer( const NReponseHTTP *this,
	void *cacheLog,
	const char *ipClient )
{
	// Cha�ne loguee
	char *log = NULL;

	// Iterateur
	NU32 i;

	// Buffer
	char buffer[ 256 ];

	// Maccro ajout data
#define AJOUTER_DATA_LOG( texte ) \
	if( !NLib_Memoire_AjouterData2( &log, \
		( ( log != NULL ) ? \
			(NU32)strlen( log ) \
			: 0 ), \
		texte ) ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED ); \
 \
		/* Liberer */ \
		NFREE( log ); \
 \
 		/* Quitter */ \
 		return NFALSE; \
	}

#define CHANGEMENT_LIGNE_LOG	"\n\t\t"

	// Recuperer code erreur
	snprintf( buffer,
		256,
		"%d ",
		NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeNumero( this->m_code ) );

	// Noter base
	AJOUTER_DATA_LOG( "\n\tRetour: " );
	AJOUTER_DATA_LOG( buffer );
	AJOUTER_DATA_LOG( NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeTexte( this->m_code ) );

	// Noter attributs
	AJOUTER_DATA_LOG( "\n\tAvec:"CHANGEMENT_LIGNE_LOG );
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( this->m_attribut ); i++ )
	{
		// Nom attribut
		AJOUTER_DATA_LOG( NHTTP_Commun_HTTP_NMotClefReponseHTTP_ObtenirMotClef( NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirClef( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
			i ) ) ) );

		// Separation
		AJOUTER_DATA_LOG( ": [" );

		// Attribut
		AJOUTER_DATA_LOG( NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirValeur( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_attribut,
			i ) ) );

		// Retour ligne
		AJOUTER_DATA_LOG( "]"CHANGEMENT_LIGNE_LOG );
	}

	// Final
	AJOUTER_DATA_LOG( "\n" );

#undef AJOUTER_DATA_LOG

	// Ajouter log
	if( !NHTTP_Serveur_Log_NCacheLogHTTP_AjouterEntree2( cacheLog,
		NTYPE_ENTREE_CACHE_LOG_HTTP_EVENEMENT,
		NTYPE_ENTREE_EVENEMENT_REPONSE,
		log,
		ipClient,
		this->m_identifiantRequete ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Liberer
		NFREE( log );

		// Quitter
		return NFALSE;
	}

	// Liberer
	NFREE( log );

	// OK
	return NTRUE;
}

/**
 * Obtenir identifiant requete associe
 *
 * @param this
 * 		Cette instance
 *
 * @return l'identifiant requete associe
 */
NU32 NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_ObtenirIdentifiant( const NReponseHTTP *this )
{
	return this->m_identifiantRequete;
}

