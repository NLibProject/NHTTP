#include "../../../../../include/NHTTP.h"

// --------------------------------------------------------------------
// struct NHTTP::Commun::HTTP::Traitement::Reponse::NElementReponseHTTP
// --------------------------------------------------------------------

/**
 * Construire element reponse
 *
 * @param clef
 * 		La clef
 * @param valeur
 * 		La valeur
 *
 * @return l'instance de la reponse
 */
__ALLOC NElementReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire( NMotClefReponseHTTP clef,
	const char *valeur )
{
	// Sortie
	__OUTPUT NElementReponseHTTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NElementReponseHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer valeur
	if( !( out->m_valeur = NLib_Chaine_Dupliquer( valeur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer clef
	out->m_clef = clef;

	// OK
	return out;
}

/**
 * Construire element depuis ligne header
 *
 * @param ligne
 * 		La ligne a parser
 *
 * @return l'instance de l'element
 */
__ALLOC NElementReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Construire2( const char *ligne )
{
	// Sortie
	__OUTPUT NElementReponseHTTP *out;

	// Curseur
	NU32 curseur = 0;

	// Clef
	char *clef;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NElementReponseHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Lire clef
	if( !( clef = NLib_Chaine_LireJusqua( ligne,
		':',
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Parser
	if( ( out->m_clef = NHTTP_Commun_HTTP_NMotClefReponseHTTP_InterpreterMotClef( clef ) ) >= NMOT_CLEF_REPONSE_HTTP_KEYWORDS )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Liberer
		NFREE( clef );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Liberer
	NFREE( clef );

	// Lire valeur
	if( !( out->m_valeur = NLib_Chaine_LireJusqua( ligne,
		'\0',
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_SYNTAX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Supprimer ' ' de depart
	NLib_Chaine_SupprimerCaractereDebut( out->m_valeur,
		' ' );

	// OK
	return out;
}

/**
 * Detruire element
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_Detruire( NElementReponseHTTP **this )
{
	// Liberer
	NFREE( (*this)->m_valeur );
	NFREE( (*this) );
}

/**
 * Obtenir clef
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur enum de la clef
 */
NMotClefReponseHTTP NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirClef( const NElementReponseHTTP *this )
{
	return this->m_clef;
}

/**
 * Obtenir valeur clef
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur texte de la clef
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirClef2( const NElementReponseHTTP *this )
{
	return NHTTP_Commun_HTTP_NMotClefReponseHTTP_ObtenirMotClef( this->m_clef );
}

/**
 * Obtenir valeur
 *
 * @param this
 * 		Cette instance
 *
 * @return la valeur
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_NElementReponseHTTP_ObtenirValeur( const NElementReponseHTTP *this )
{
	return this->m_valeur;
}

