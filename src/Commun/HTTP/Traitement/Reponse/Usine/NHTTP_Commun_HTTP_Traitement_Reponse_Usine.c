#include "../../../../../../include/NHTTP.h"

// ---------------------------------------------------------
// namespace NHTTP::Commun::HTTP::Traitement::Reponse::Usine
// ---------------------------------------------------------

/**
 * Creer packet reponse textuelle simple
 *
 * @param message
 * 		Le message
 * @param codeHTTP
 * 		Le code http
 * @param identifiant
 * 		L'identifiant de la requete associee
 *
 * @return le packet
 */
__ALLOC NReponseHTTP *NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( const char *message,
	NHTTPCode codeHTTP,
	NU32 identifiant )
{
	// Sortie
	__OUTPUT NReponseHTTP *out;

	// Construire reponse
	if( !( out = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire( codeHTTP,
		identifiant ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Definir texte
	if( !NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier2( out,
		message ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNKNOWN );

		// Detruire
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

