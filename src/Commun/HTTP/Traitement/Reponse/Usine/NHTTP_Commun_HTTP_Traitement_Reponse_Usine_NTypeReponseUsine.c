#define NHTTP_COMMUN_HTTP_TRAITEMENT_REPONSE_USINE_NTYPEREPONSEUSINE_INTERNE
#include "../../../../../../include/NHTTP.h"

// -----------------------------------------------------------------------
// enum NHTTP::Commun::HTTP::Traitement::Reponse::Usine::NTypeReponseUsine
// -----------------------------------------------------------------------

/**
 * Obtenir la reponse
 *
 * @param type
 * 		Le type de reponse
 *
 * @return la reponse
 */
const char *NHTTP_Commun_HTTP_Traitement_Reponse_Usine_NTypeReponseUsine_ObtenirReponse( NTypeReponseUsine type )
{
	switch( type )
	{
		case NTYPE_REPONSE_USINE_404:
			return NTypeReponseUsine404;

		case NTYPE_REPONSE_USINE_TEA_POT:
			return NTypeReponseUsineTeaPot;

		case NTYPE_REPONSE_USINE_ABOUT:
			return NTypeReponseUsineAbout;

		case NTYPE_REPONSE_USINE_BAD_REQUEST:
		default:
			return NTypeReponseUsineBadRequest;
	}
}

