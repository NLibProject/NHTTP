#include "../../../../../include/NHTTP.h"

// -------------------------------------------------------
// struct NHTTP::Commun::Traitement::Reponse::NFichierHTTP
// -------------------------------------------------------

/**
 * Construire le fichier HTTP
 *
 * @param typeData
 * 		Le type de donnees
 * @param typeFlux
 * 		Le type de flux
 * @param data
 * 		La donnees
 * @param lienFichier
 * 		Le lien vers le fichier si type de flux fichier (NULL sinon)
 *
 * @return l'instance du fichier
 */
__ALLOC NFichierHTTP *NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Construire( NTypeFichierHTTP typeData,
	NTypeFlux typeFlux,
	__WILLBEOWNED void *data,
	const char *lienFichier )
{
	// Output
	NFichierHTTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFichierHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		switch( typeData )
		{
			case NTYPE_FICHIER_HTTP_FICHIER:
				NLib_Fichier_NFichierBinaire_Detruire( (NFichierBinaire**)&data );
				break;

			case NTYPE_FICHIER_HTTP_TEXTE:
				NFREE( data );
				break;

			default:
				break;
		}

		// Quitter
		return NULL;
	}

	// Fichier binaire?
	switch( typeData )
	{
		// Copier le lien
		case NTYPE_FICHIER_HTTP_FICHIER:
			// Copier lien fichier
			if( !( out->m_lienFichier = NLib_Chaine_Dupliquer( lienFichier ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer
				NLib_Fichier_NFichierBinaire_Detruire( (NFichierBinaire**)&data );
				NFREE( out );

				// Quitter
				return NULL;
			}
			break;

		default:
		case NTYPE_FICHIER_HTTP_TEXTE:
			break;
	}

	// Enregistrer
	out->m_data = data;
	out->m_typeData = typeData;
	out->m_typeFlux = typeFlux;

	// OK
	return out;
}

/**
 * Construire a partir d'un fichier de reponse
 *
 * @param src
 * 		La source
 *
 * @return l'instance du fichier
 */
__ALLOC NFichierHTTP *NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Construire2( const NFichierHTTP *src )
{
	// Sortie
	__OUTPUT NFichierHTTP *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NFichierHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Analyser type
	switch( src->m_typeData )
	{
		case NTYPE_FICHIER_HTTP_FICHIER:
			// Dupliquer lien fichier
			if( !( out->m_lienFichier = NLib_Chaine_Dupliquer( src->m_lienFichier ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}

			// Ouvrir le fichier
			if( !( out->m_data = NLib_Fichier_NFichierBinaire_ConstruireLecture( src->m_lienFichier ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Liberer
				NFREE( out->m_lienFichier );
				NFREE( out );

				// Quitter
				return NULL;
			}
			break;

		case NTYPE_FICHIER_HTTP_TEXTE:
			// Dupliquer le texte
			if( !( out->m_data = NLib_Chaine_Dupliquer( src->m_data ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}
			break;

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
	}

	// Enregistrer
	out->m_typeFlux = src->m_typeFlux;
	out->m_typeData = src->m_typeData;

	// OK
	return out;
}

/**
 * Detruire fichier
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_Detruire( NFichierHTTP **this )
{
	// Liberer
	switch( (*this)->m_typeData )
	{
		case NTYPE_FICHIER_HTTP_FICHIER:
			NLib_Fichier_NFichierBinaire_Detruire( (NFichierBinaire**)&( (*this)->m_data ) );
			break;

		case NTYPE_FICHIER_HTTP_TEXTE:
			NFREE( (*this)->m_data );
			break;

		default:
			break;
	}
	NFREE( (*this)->m_lienFichier );
	NFREE( *this );
}

/**
 * Obtenir type de donnees
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de donnees
 */
NTypeFichierHTTP NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTypeData( const NFichierHTTP *this )
{
	return this->m_typeData;
}

/**
 * Obtenir type flux
 *
 * @param this
 * 		Cette instance
 *
 * @return le type de flux
 */
NTypeFlux NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTypeFlux( const NFichierHTTP *this )
{
	return this->m_typeFlux;
}

/**
 * Obtenir data
 *
 * @param this
 * 		Cette instance
 *
 * @return la donnees
 */
const void *NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirData( const NFichierHTTP *this )
{
	return this->m_data;
}

/**
 * Obtenir lien fichier
 *
 * @param this
 * 		Cette instance
 *
 * @return le lien vers le fichier
 */
const char *NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirLienFichier( const NFichierHTTP *this )
{
	return this->m_lienFichier;
}

/**
 * Obtenir la taille de ce que le fichier contient (taille du texte ou du fichier)
 *
 * @param this
 * 		Cette instance
 *
 * @return la taille
 */
NU64 NHTTP_Commun_HTTP_Traitement_Fichier_NFichierHTTP_ObtenirTaille( const NFichierHTTP *this )
{
	switch( this->m_typeData )
	{
		default:
			return 0;

		case NTYPE_FICHIER_HTTP_FICHIER:
			return NLib_Fichier_NFichierBinaire_ObtenirTaille( (NFichierBinaire*)this->m_data );

		case NTYPE_FICHIER_HTTP_TEXTE:
			return (NU32)strlen( this->m_data );
	}
}

