#define NHTTP_COMMUN_HTTP_TRAITEMENT_INTERNE
#include "../../../../include/NHTTP.h"

// -----------------------------------------
// namespace NHTTP::Commun::HTTP::Traitement
// -----------------------------------------

/**
 * Traiter requete
 *
 * @param requete
 * 		La requete
 * @param client
 * 		Le client ayant effectue la requete
 *
 * @return le resultat du traitement
 */
__ALLOC NReponseHTTP *NHTTP_Commun_HTTP_Traitement_TraiterRequete( const NRequeteHTTP *requete,
	const NClientServeur *client )
{
	// Fichier
	NFichierBinaire *fichier;

	// Sortie
	__OUTPUT NReponseHTTP *out;

	// Buffer
	char *buffer;

	// Referencer client
	NREFERENCER( client );

	// Type de requete inconnu?
	switch( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( requete ) )
	{
		case NTYPE_REQUETE_HTTP_GET:
			break;

		case NTYPE_REQUETE_HTTP_ABOUT:
			return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NHTTP_Commun_HTTP_Traitement_Reponse_Usine_NTypeReponseUsine_ObtenirReponse( NTYPE_REPONSE_USINE_ABOUT ),
				NHTTP_CODE_510_NOT_EXTENDED,
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirIdentifiant( requete ) );

		default:
			return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NHTTP_Commun_HTTP_Traitement_Reponse_Usine_NTypeReponseUsine_ObtenirReponse( NTYPE_REPONSE_USINE_BAD_REQUEST ),
				NHTTP_CODE_400_BAD_REQUEST,
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirIdentifiant( requete ) );
	}

	// Le fichier n'existe pas?
	if( !NLib_Fichier_Operation_EstExiste( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElement( requete ) ) )
		// Introuvable
		return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NHTTP_Commun_HTTP_Traitement_Reponse_Usine_NTypeReponseUsine_ObtenirReponse( NTYPE_REPONSE_USINE_404 ),
			NHTTP_CODE_404_NOT_FOUND,
			NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirIdentifiant( requete ) );

	// Obtenir chemin reel
	if( !( buffer = NLib_Module_Repertoire_ObtenirCheminAbsolu( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElement( requete ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Quitter
		return NULL;
	}

	// Anti cross-directory
	if( strstr( buffer,
		NLib_Module_Repertoire_ObtenirInitial( ) ) == NULL )
	{
		// Liberer
		NFREE( buffer );

		// I'm a teapot
		return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NHTTP_Commun_HTTP_Traitement_Reponse_Usine_NTypeReponseUsine_ObtenirReponse( NTYPE_REPONSE_USINE_TEA_POT ),
			NHTTP_CODE_418_I_M_A_TEAPOT,
			NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirIdentifiant( requete ) );
	}

	// Liberer
	NFREE( buffer );

	// Construire la reponse
	if( !( out = NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Construire( NHTTP_CODE_200_OK,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Ouvrir fichier
	if( !( fichier = NLib_Fichier_NFichierBinaire_ConstruireLecture( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElement( requete ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_Detruire( &out );

		// Quitter
		return NULL;
	}

	// Enregistrer fichier
	NHTTP_Commun_HTTP_Traitement_Reponse_NReponseHTTP_DefinirFichier3( out,
		fichier,
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElement( requete ),
		NHTTP_Commun_HTTP_Mimetype_DeterminerTypeFlux( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElement( requete ) ) );

	// OK
	return out;
}

/**
 * Obtenir caracteres arret traitement
 *
 * @return la liste des caractere stoppant la lecture d'une ligne lors du traitement
 */
const char *NHTTP_Commun_HTTP_Traitement_ObtenirListeCaractereArretTraitement( void )
{
	return NHTTP_CARACTERE_ARRET_TRAITEMENT;
}

/**
 * Est caractere de fin de ligne traitement
 *
 * @param c
 * 		Le caractere
 *
 * @return si le caractere appartient aux caracteres de fin de ligne
 */
NBOOL NHTTP_Commun_HTTP_Traitement_EstCaractereFinDeLigne( char c )
{
	// Iterateur
	NU32 i = 0;

	// Chercher
	for( ; i < NHTTP_NOMBRE_CARACTERE_ARRET_TRAITEMENT; i++ )
		if( c == NHTTP_CARACTERE_ARRET_TRAITEMENT[ i ] )
			return NTRUE;

	// OK
	return NFALSE;
}

