#include "../../../../include/NHTTP.h"

// ------------------------------------
// namespace NHTTP::Commun::HTTP::Outil
// ------------------------------------

/**
 * Remplacer %XX par le caractere correspondant dans l'element
 *
 * @param element
 * 		L'element
 *
 * @return l'element corrige
 */
__ALLOC char *NHTTP_Commun_HTTP_Outil_RemplacerCaractereHTMLElementDemande( const char *element )
{
	// Buffer
	char *buffer = NULL;

	// Curseur entree
	NU32 curseurEntree = 0;

	// Position %XX
	NU32 curseurPourcentage = 0;

	// Curseur buffer
	NU32 curseurBuffer = 0;

	// Curseur precedent
	NU32 curseurPrecedent = 0;

	// Iterateur
	NU32 i;

	// Taille entree
	unsigned int tailleEntree;

	// Caracteres
	char caractere[ 2 ];
	char caractereAssemble;

	// Calculer taille en entree
	tailleEntree = (NU32)strlen( element );

	// Tant qu'on trouve un espace
	while( curseurEntree < tailleEntree )
	{
		// Chercher %
		if( NLib_Chaine_PlacerAuCaractere( element,
			'%',
			&curseurPourcentage ) )
		{
			// Recuperer les deux caracteres
			caractere[ 0 ] = NLib_Chaine_ObtenirProchainCaractere( element,
				&curseurPourcentage,
				NFALSE );
			caractere[ 1 ] = NLib_Chaine_ObtenirProchainCaractere( element,
				&curseurPourcentage,
				NFALSE );

			// Copier
			for( i = curseurPrecedent; i < curseurPourcentage - 3; i++ )
			{
				// Ajouter data
				NLib_Memoire_AjouterData( &buffer,
					curseurBuffer,
					&element[ i ],
					sizeof( char ) );

				// Incrementer taille
				curseurBuffer++;
			}

			// Assembler caractere
			caractereAssemble = ( ( (char)NLib_Caractere_ConvertirDecimal( caractere[ 0 ] ) ) << 4 ) | (char)( NLib_Caractere_ConvertirDecimal( caractere[ 1 ] ) );

			// Ajouter caractere
			NLib_Memoire_AjouterData( &buffer,
				curseurBuffer,
				&caractereAssemble,
				sizeof( char ) );

			// Incrementer curseur
			curseurBuffer++;

			// Enregistrer
			curseurPrecedent = curseurPourcentage;
			curseurEntree = curseurPourcentage;
		}
		else
			// Copier
			for( ; curseurEntree < tailleEntree; curseurEntree++ )
			{
				// Ajouter caractere
				NLib_Memoire_AjouterData( &buffer,
					curseurBuffer,
					&element[ curseurEntree ],
					sizeof( char ) );

				// Incrementer curseur
				curseurBuffer++;
			}
	}

	// Ajouter \0
	caractere[ 0 ] = '\0';
	NLib_Memoire_AjouterData( &buffer,
		curseurBuffer,
		&caractere[ 0 ],
		sizeof( char ) );

	// OK
	return buffer;
}