#define NHTTP_COMMUN_HTTP_NMOTCLEFREPONSEHTTP_INTERNE
#include "../../../include/NHTTP.h"

// ---------------------------------------------
// enum NHTTP::Commun::HTTP::NMotClefReponseHTTP
// ---------------------------------------------

/**
 * Obtenir le mot clef associe a la reponse
 *
 * @param typeReponse
 * 		La reponse
 *
 * @return le mot clef associe a la reponse
 */
const char *NHTTP_Commun_HTTP_NMotClefReponseHTTP_ObtenirMotClef( NMotClefReponseHTTP typeReponse )
{
	return NMotClefReponseHTTPTexte[ typeReponse ];
}

/**
 * Interpreter mot clef requete HTTP
 *
 * @param motClef
 *		Le mot clef
 *
 * @return le type de requete (NMOT_CLEF_REQUETE_HTTP_KEYWORDS si introuvable)
 */
NMotClefReponseHTTP NHTTP_Commun_HTTP_NMotClefReponseHTTP_InterpreterMotClef( const char *motClef )
{
	// Sortie
	__OUTPUT NMotClefReponseHTTP out = (NMotClefReponseHTTP)0;

	// Chercher
	for( ; out < NMOT_CLEF_REPONSE_HTTP_KEYWORDS; out++ )
		// Comparer
		if( NLib_Chaine_Comparer( NMotClefReponseHTTPTexte[ out ],
			motClef,
			NFALSE,
			0 ) )
			return out;

	// Introuvable
	return NMOT_CLEF_REPONSE_HTTP_KEYWORDS;
}

