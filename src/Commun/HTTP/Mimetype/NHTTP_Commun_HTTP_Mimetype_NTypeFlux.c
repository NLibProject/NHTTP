#define NHTTP_COMMUN_HTTP_MIMETYPE_NTYPEFLUX_INTERNE
#include "../../../../include/NHTTP.h"

// ---------------------------------------------
// enum NHTTP::Commun::HTTP::Mimetype::NTypeFlux
// ---------------------------------------------

/**
 * Obtenir le type de flux
 *
 * @param type
 * 		Le type de flux
 *
 * @return le type de flux texte
 */
const char *NHTTP_Commun_HTTP_Mimetype_NTypeFlux_ObtenirType( NTypeFlux type )
{
	return NHTTPTypeFluxTexte[ type ];
}

