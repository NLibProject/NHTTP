#include "../../../../include/NHTTP.h"

// ---------------------------------------
// namespace NHTTP::Commun::HTTP::Mimetype
// ---------------------------------------

/**
 * Determiner extension
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return l'extension (ou NTYPES_EXTENSION si introuvable)
 */
NTypeExtension NHTTP_Commun_HTTP_Mimetype_DeterminerExtension( const char *lien )
{
	// Curseur
	NU32 curseur = 0;

	// Extension
	char *extension;

	// Derniere position
	NU32 dernierePosition = 0;

	// Sortie
	__OUTPUT NTypeExtension out;

	// Chercher le dernier point
	while( NLib_Chaine_PlacerAuCaractere( lien,
		'.',
		&curseur ) )
		dernierePosition = curseur;

	// Restaurer
	curseur = dernierePosition;

	// Lire extension
	if( !( extension = NLib_Chaine_LireJusqua( lien,
		'\0',
		&curseur,
		NFALSE ) ) )
		return NTYPES_EXTENSION;

	// Interpreter
	out = NHTTP_Commun_HTTP_Mimetype_NTypeExtension_InterpreterExtension( extension );

	// Liberer
	NFREE( extension );

	// OK
	return out;
}

/**
 * Determiner type de flux
 *
 * @param lien
 * 		Le lien vers le fichier
 *
 * @return le type de flux
 */
NTypeFlux NHTTP_Commun_HTTP_Mimetype_DeterminerTypeFlux( const char *lien )
{
	// Determiner type de flux
	return NHTTP_Commun_HTTP_Mimetype_DeterminerTypeFlux2( NHTTP_Commun_HTTP_Mimetype_DeterminerExtension( lien ) );
}

/**
 * Determiner type de flux
 *
 * @param extension
 * 		L'extension
 *
 * @return le type de flux
 */
NTypeFlux NHTTP_Commun_HTTP_Mimetype_DeterminerTypeFlux2( NTypeExtension extension )
{
	// Analyser extension
	switch( extension )
	{
		// Page web
		case NTYPE_EXTENSION_HTML:
		case NTYPE_EXTENSION_HTM:
		case NTYPE_EXTENSION_PHP:
			return NTYPE_FLUX_HTML;

		case NTYPE_EXTENSION_CSS:
			return NTYPE_FLUX_CSS;

		// Texte
		case NTYPE_EXTENSION_TXT:
		case NTYPE_EXTENSION_LOG:
		case NTYPE_EXTENSION_C:
		case NTYPE_EXTENSION_H:
		case NTYPE_EXTENSION_CPP:
		case NTYPE_EXTENSION_HPP:
		case NTYPE_EXTENSION_JAVA:
		case NTYPE_EXTENSION_CS:
			return NTYPE_FLUX_TEXTE;

		case NTYPE_EXTENSION_YAML:
		case NTYPE_EXTENSION_YML:
			return NTYPE_FLUX_YAML;
		case NTYPE_EXTENSION_JSON:
			return NTYPE_FLUX_JSON;

		// PDF
		case NTYPE_EXTENSION_PDF:
			return NTYPE_FLUX_PDF;

		// Images
		case NTYPE_EXTENSION_PNG:
			return NTYPE_FLUX_IMAGE_PNG;
		case NTYPE_EXTENSION_JPG:
		case NTYPE_EXTENSION_JPEG:
			return NTYPE_FLUX_IMAGE_JPG;
		case NTYPE_EXTENSION_GIF:
			return NTYPE_FLUX_IMAGE_GIF;

		default:
			return NTYPE_FLUX_TELECHARGEMENT;
	}
}

