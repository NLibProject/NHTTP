#define NHTTP_COMMUN_HTTP_MIMETYPE_NTYPENTYPE_EXTENSION_INTERNE
#include "../../../../include/NHTTP.h"

// --------------------------------------------------
// enum NHTTP::Commun::HTTP::Mimetype::NTypeExtension
// --------------------------------------------------

/**
 * Obtenir l'extension associee a un type d'extension
 *
 * @param extension
 * 		Le type d'extension
 *
 * @return l'extension
 */
const char *NHTTP_Commun_HTTP_Mimetype_NTypeExtension_ObtenirExtension( NTypeExtension extension )
{
	return NTypeExtensionTexte[ extension ];
}

/**
 * Interpreter extension
 *
 * @param extension
 * 		L'extension a interpreter
 *
 * @return le type d'extension (NTYPES_EXTENSION si introuvable)
 */
NTypeExtension NHTTP_Commun_HTTP_Mimetype_NTypeExtension_InterpreterExtension( const char *extension )
{
	// Sortie
	__OUTPUT NTypeExtension i = (NTypeExtension)0;

	// Chercher
	for( ; i < NTYPES_EXTENSION; i++ )
		if( NLib_Chaine_Comparer( NTypeExtensionTexte[ i ],
			extension,
			NFALSE,
			0 ) )
			return i;

	// Introuvable
	return NTYPES_EXTENSION;
}

