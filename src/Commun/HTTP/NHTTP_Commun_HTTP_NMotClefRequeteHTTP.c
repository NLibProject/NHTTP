#define NHTTP_COMMUN_HTTP_NMOTCLEFREQUETEHTTP_INTERNE
#include "../../../include/NHTTP.h"

// ---------------------------------------------
// enum NHTTP::Commun::HTTP::NMotClefRequeteHTTP
// ---------------------------------------------

/**
 * Obtenir mot clef requete http
 *
 * @param typeRequete
 * 		Le type de requete
 *
 * @return le mot clef associe
 */
const char *NHTTP_Commun_HTTP_NMotClefRequeteHTTP_ObtenirMotClef( NMotClefRequeteHTTP typeRequete )
{
	return NMotClefRequeteHTTPTexte[ typeRequete ];
}

/**
 * Interpreter mot clef requete HTTP
 *
 * @param motClef
 *		Le mot clef
 *
 * @return le type de requete (NMOT_CLEF_REQUETE_HTTP_KEYWORDS si introuvable)
 */
NMotClefRequeteHTTP NHTTP_Commun_HTTP_NMotClefRequeteHTTP_InterpreterMotClef( const char *motClef )
{
	// Sortie
	__OUTPUT NMotClefRequeteHTTP out = (NMotClefRequeteHTTP)0;

	// Chercher
	for( ; out < NMOT_CLEF_REQUETE_HTTP_KEYWORDS; out++ )
		// Comparer
		if( NLib_Chaine_Comparer( NMotClefRequeteHTTPTexte[ out ],
			motClef,
			NFALSE,
			0 ) )
			return out;

	// Introuvable
	return NMOT_CLEF_REQUETE_HTTP_KEYWORDS;
}

