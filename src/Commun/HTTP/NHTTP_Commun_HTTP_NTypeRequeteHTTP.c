#define NHTTP_COMMUN_HTTP_NTYPEREQUETEHTTP_INTERNE
#include "../../../include/NHTTP.h"

// ------------------------------------------
// enum NHTTP::Commun::HTTP::NTypeRequeteHTTP
// ------------------------------------------

/**
 * Obtenir le mot clef du type de requete
 *
 * @param typeRequete
 * 		Le type de la requete
 *
 * @return le mot clef
 */
const char *NHTTP_Commun_HTTP_NTypeRequeteHTTP_ObtenirMotClef( NTypeRequeteHTTP typeRequete )
{
	return NTypeRequeteHTTPTexte[ typeRequete ];
}

/**
 * Interpreter le type de requete
 *
 * @param type
 * 		Le type de requete
 *
 * @return le type de requete (NTYPES_REQUETE_HTTP si introuvable)
 */
NTypeRequeteHTTP NHTTP_Commun_HTTP_NTypeRequeteHTTP_Interpreter( const char *type )
{
	// Sortie
	__OUTPUT NTypeRequeteHTTP out = (NTypeRequeteHTTP)0;

	// Chercher
	for( ; out < NTYPES_REQUETE_HTTP; out++ )
		// Comparer
		if( NLib_Chaine_Comparer( NTypeRequeteHTTPTexte[ out ],
			type,
			NTRUE,
			0 ) )
			return out;

	// Introuvable
	return NTYPES_REQUETE_HTTP;
}