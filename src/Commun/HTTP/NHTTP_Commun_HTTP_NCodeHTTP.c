#define NHTTP_COMMUN_HTTP_NCODEHTTP_INTERNE
#include "../../../include/NHTTP.h"

// -----------------------------------
// enum NHTTP::Commun::HTTP::NCodeHTTP
// -----------------------------------

/**
 * Obtenir code numero HTTP
 *
 * @param code
 * 		Le code http
 *
 * @return le numero du code
 */
NU32 NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeNumero( NHTTPCode code )
{
	return NHTTPCodeNumero[ code ];
}

/**
 * Obtenir code depuis valeur numerique
 *
 * @param code
 * 		Le code numerique
 *
 * @return le code http (ou NHTTP_CODES si introuvable)
 */
NHTTPCode NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeDepuisNumero( NU32 code )
{
	// Sortie
	__OUTPUT NHTTPCode out = (NHTTPCode)0;

	// Chercher
	for( ; out < NHTTP_CODES; out++ )
		// Verifier
		if( NHTTPCodeNumero[ out ] == code )
			// OK
			return out;

	// Introuvable
	return NHTTP_CODES;
}

/**
 * Obtenir code texte
 *
 * @param code
 * 		Le code http
 *
 * @return le message du code
 */
const char *NHTTP_Commun_HTTP_NCodeHTTP_ObtenirCodeTexte( NHTTPCode code )
{
	return NHTTPCodeTexte[ code ];
}

