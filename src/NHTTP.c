#include "../include/NHTTP.h"

// ---------------
// namespace NHTTP
// ---------------

#ifdef NHTTP_TEST_PROJECT
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Client HTTP
	NClientHTTP *client;

	NRequeteHTTP *requete;

	// Initialiser NLib
	NLib_Initialiser( NULL );

	// Construire client
	client = NHTTP_Client_NClientHTTP_Construire( "nproject.ddns.net",
		80,
		NULL );

	// Construire requete
	requete = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( NTYPE_REQUETE_HTTP_GET,
		"/lucas/projet/architecture/cpu.zip" );

	// Envoyer la requete (reponse sera affichee dans console)
	NHTTP_Client_NClientHTTP_EnvoyerRequete( client,
		requete,
		0 );

	// Attendre saisie utilisateur
	getchar( );

	// Detruire client
	NHTTP_Client_NClientHTTP_Detruire( &client );

	// Quitter NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}
#endif // NHTTP_TEST_PROJECT

