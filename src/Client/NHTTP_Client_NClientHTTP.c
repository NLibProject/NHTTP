#include "../../include/NHTTP.h"

// ---------------------------------
// struct NHTTP::Client::NClientHTTP
// ---------------------------------

/**
 * Construire client par defaut qui affichera en sortie standard ce qui est recu
 *
 * @param nomDomaine
 * 		Le nom de domaine auquel se connecter
 * @param port
 * 		Le port auquel se connecter
 * @param dataUtilisateur
 * 		Donnees utilisateur (peut etre nul)
 *
 * @return l'instance du client
 */
__ALLOC NClientHTTP *NHTTP_Client_NClientHTTP_Construire( const char *nomDomaine,
	NU32 port,
	const void *dataUtilisateur )
{
	// Construire
	return NHTTP_Client_NClientHTTP_Construire2( nomDomaine,
		port,
		dataUtilisateur,
		NHTTP_Client_Outil_CallbackReception );
}

/**
 * Construire client avec callback reception personnalise
 *
 * @param nomDomaine
 * 		Le nom de domaine auquel se connecter
 * @param port
 * 		Le port auquel se connecter
 * @param dataUtilisateur
 * 		Donnees utilisateur (peut etre nul)
 * @param callbackReception
 * 		Le callback de reception
 *
 * @return l'instance du client
 */
__ALLOC NClientHTTP *NHTTP_Client_NClientHTTP_Construire2( const char *nomDomaine,
	NU32 port,
	const void *dataUtilisateur,
	NBOOL ( ___cdecl *callbackReception )( const NPacket *packet,
		const NClient *client ) )
{
	return NHTTP_Client_NClientHTTP_Construire3( nomDomaine,
		port,
		dataUtilisateur,
		callbackReception,
		NHTTP_CLIENT_TEMPS_AVANT_TIMEOUT_RECEPTION );
}

/**
 * Construire client avec callback reception personnalise
 *
 * @param nomDomaine
 * 		Le nom de domaine auquel se connecter
 * @param port
 * 		Le port auquel se connecter
 * @param dataUtilisateur
 * 		Donnees utilisateur (peut etre nul)
 * @param callbackReception
 * 		Le callback de reception
 * @param timeoutReception
 * 		Le timeout de reception
 *
 * @return l'instance du client
 */
__ALLOC NClientHTTP *NHTTP_Client_NClientHTTP_Construire3( const char *nomDomaine,
	NU32 port,
	const void *dataUtilisateur,
	NBOOL ( ___cdecl *callbackReception )( const NPacket *packet,
		const NClient *client ),
	NU32 timeoutReception )
{
	// Sortie
	__OUTPUT NClientHTTP *out;

	// Buffer
	char buffer[ 256 ];

	// IP
	const NDetailIP *ip;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( NClientHTTP ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_dataUtilisateur = dataUtilisateur;

	// Mettre port dans chaine
	snprintf( buffer,
		256,
		"%d",
		port );

	// Resoudre le nom de domaine
	if( !( out->m_resolution = NLib_Module_Reseau_Resolution_NListeResolution_Construire( nomDomaine,
		buffer ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Obtenir une IPv4
	if( !( ip = NLib_Module_Reseau_Resolution_NListeResolution_ObtenirIPV4( out->m_resolution ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DNS_RESOLVE_FAILED );

		// Liberer
		NLib_Module_Reseau_Resolution_NListeResolution_Detruire( &out->m_resolution );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire client
	if( !( out->m_client = NLib_Module_Reseau_Client_NClient_Construire2( NLib_Module_Reseau_IP_NDetailIP_ObtenirAdresse( ip ),
		port,
		callbackReception,
		NHTTP_Client_Outil_CallbackDeconnexion,
		out,
		timeoutReception,
		NTYPE_CLIENT_TCP,
		NTYPE_CACHE_PACKET_PERSONNALISE,
		NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketRaw,
		(NBOOL ( ___cdecl* )( const void*,
			SOCKET ) )NHTTP_Commun_Outil_CallbackMethodeEnvoiPacket ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NLib_Module_Reseau_Resolution_NListeResolution_Detruire( &out->m_resolution );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/**
 * Detruire client
 *
 * @param this
 * 		Cette instance
 */
void NHTTP_Client_NClientHTTP_Detruire( NClientHTTP **this )
{
	// Detruire client
	NLib_Module_Reseau_Client_NClient_Detruire( &(*this)->m_client );

	// Detruire liste resolution domaine
	NLib_Module_Reseau_Resolution_NListeResolution_Detruire( &(*this)->m_resolution );

	// Liberer
	NFREE( *this );
}

/**
 * Definir delai entre l'envoi de deux requetes
 *
 * @param this
 * 		Cette instance
 * @param delai
 * 		Le delai entre deux requetes (ms)
 */
void NHTTP_Client_NClientHTTP_DefinirDelaiEntreEnvoiDeuxRequete( NClientHTTP *this,
	NU32 delai )
{
	this->m_delaiEntreEnvoi = delai;
}

/**
 * Envoyer une requete
 *
 * @param this
 * 		Cette instance
 * @param requete
 * 		La requete a envoyer
 * @param timeoutConnexion
 * 		Le timeout a la connexion
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Client_NClientHTTP_EnvoyerRequete( NClientHTTP *this,
	__WILLBEOWNED NRequeteHTTP *requete,
	NU32 timeoutConnexion )
{
	// Packet personnalise
	NPacketPersonnalise *packet;

	// On est connecte?
	if( NLib_Module_Reseau_Client_NClient_EstConnecte( this->m_client ) )
		// Il y a une erreur?
		if( NLib_Module_Reseau_Client_NClient_EstErreur( this->m_client ) )
			// Deconnecter client
			NLib_Module_Reseau_Client_NClient_Deconnecter( this->m_client );

	// On est deconnecte?
	if( !NLib_Module_Reseau_Client_NClient_EstConnecte( this->m_client ) )
		// Se connecter
		if( !NLib_Module_Reseau_Client_NClient_Connecter2( this->m_client,
			timeoutConnexion ) )
		{
			// Notifier
			NOTIFIER_AVERTISSEMENT( NERREUR_SOCKET_CONNECTION_FAILED );

			// Detruire la requete
			NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &requete );

			// Quitter
			return NFALSE;
		}

	// Creer packet http
	if( !( packet = NHTTP_Commun_Traitement_Requete_NRequeteHTTP_CreerPacket( requete ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire la requete
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &requete );

		// Quitter
		return NFALSE;
	}

	// Detruire la requete
	NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &requete );

	// On a un delai entre deux envois?
	while( NLib_Temps_ObtenirTick64( ) - this->m_tickDernierEnvoi < this->m_delaiEntreEnvoi )
		NLib_Temps_Attendre( 1 );

	// Envoyer la requete
	if( !NLib_Module_Reseau_Client_NClient_AjouterPacket( this->m_client,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Enregistrer tick envoi
	this->m_tickDernierEnvoi = NLib_Temps_ObtenirTick64( );

	// OK
	return NTRUE;
}

/**
 * Envoyer une requete
 *
 * @param this
 * 		Cette instance
 * @param typeRequete
 * 		Le type de requete a envoyer
 * @param elementRequete
 * 		L'element demande
 * @param timeoutConnexion
 * 		Le timeout a la connexion
 *
 * @return si l'operation s'est bien passee
 */
NBOOL NHTTP_Client_NClientHTTP_EnvoyerRequete2( NClientHTTP *this,
	NTypeRequeteHTTP typeRequete,
	const char *elementRequete,
	const char *dataRequete,
	NU32 tailleDataRequete,
	NU32 timeoutConnexion )
{
	return NHTTP_Client_NClientHTTP_EnvoyerRequete3( this,
		typeRequete,
		elementRequete,
		dataRequete,
		tailleDataRequete,
		timeoutConnexion,
		NULL,
		NULL );
}

/**
 * Envoyer une requete authentifiee
 *
 * @param this
 * 		Cette instance
 * @param typeRequete
 *		Le type de requete a envoyer
 * @param elementRequete
 * 		L'element demande
 * @param timeoutConnexion
 * 		Le timeout a la connexion
 * @param basicAuthUsername
 * 		Le nom d'utilisateur (ou NULL)
 * @param basicAuthPassword
 * 		Le mot de passe (ou NULL)
 *
 * @return si l'operation a reussi
 */
NBOOL NHTTP_Client_NClientHTTP_EnvoyerRequete3( NClientHTTP *this,
	NTypeRequeteHTTP typeRequete,
	const char *elementRequete,
	const char *dataRequete,
	NU32 tailleDataRequete,
	NU32 timeoutConnexion,
	const char *basicAuthUsername,
	const char *basicAuthPassword )
{
	// Requete
	NRequeteHTTP *requete;

	// Header authentification
	char authentification[ 256 ];

	// Buffer
	char *buffer;

	// Creer la requete
	if( !( requete = NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Construire2( typeRequete,
		elementRequete ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Construire authentification
	if( basicAuthUsername )
	{
		// Construire identifiant/mot de passe
		snprintf( authentification,
			256,
			"%s:%s",
			basicAuthUsername,
			basicAuthPassword != NULL ?
				basicAuthPassword
				: "" );

		// Convertir Base64
		if( !( buffer = NLib_Chaine_ConvertirBase64( authentification,
			(NU32)strlen( authentification ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire
			NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_Detruire( &requete );

			// Quitter
			return NFALSE;
		}

		// Construire authentification
		snprintf( authentification,
			256,
			"Basic %s",
			buffer );

		// Free
		NFREE( buffer );

		// Ajouter header
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterAttribut( requete,
			NMOT_CLEF_REQUETE_HTTP_KEYWORD_AUTHORIZATION,
			authentification );
	}

	// Il y a des donnees?
	if( tailleDataRequete > 0 )
		// Ajouter data
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_AjouterData( requete,
			dataRequete,
			tailleDataRequete );

	// Envoyer
	return NHTTP_Client_NClientHTTP_EnvoyerRequete( this,
		requete,
		timeoutConnexion );
}

/**
 * Obtenir donnees utilisateur
 *
 * @param this
 * 		Cette instance
 *
 * @return les donnees utilisateur
 */
void *NHTTP_Client_NClientHTTP_ObtenirDataUtilisateur( const NClientHTTP *this )
{
	return (void*)this->m_dataUtilisateur;
}

/**
 * Obtenir IP serveur a contacter
 *
 * @param this
 * 		Cette instance
 *
 * @return l'ip qui sera contactee
 */
const char *NHTTP_Client_NClientHTTP_ObtenirIPServeur( const NClientHTTP *this )
{
	// Adresse
	const NDetailIP *adresse;

	// Obtenir adresse
	if( ( adresse = NLib_Module_Reseau_Resolution_NListeResolution_ObtenirIPV4( this->m_resolution ) ) != NULL )
		return NLib_Module_Reseau_IP_NDetailIP_ObtenirAdresse( adresse );

	// Erreur
	return NULL;
}

/**
 * Est packet en attente?
 *
 * @param this
 * 		Cette instance
 *
 * @return si il y a des packets en attente
 */
NBOOL NHTTP_Client_NClientHTTP_EstPacketAttente( const NClientHTTP *this )
{
	return (NBOOL)( NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( this->m_client ) ) > 0 );
}

/**
 * Est erreur?
 *
 * @param this
 * 		Cette instance
 *
 * @return si il y a une erreur
 */
NBOOL NHTTP_Client_NClientHTTP_EstErreur( const NClientHTTP *this )
{
	return ( this->m_client != NULL ) ?
		NLib_Module_Reseau_Client_NClient_EstErreur( this->m_client )
		: NFALSE;
}
