#include "../../../include/NHTTP.h"

// ------------------------------
// namespace NHTTP::Client::Outil
// ------------------------------

/**
 * Callback reception
 *
 * @param packet
 * 		Le packet recu
 * @param client
 * 		Le client
 *
 * @return si l'operation a reussi
 */
__CALLBACK NBOOL NHTTP_Client_Outil_CallbackReception( const NPacket *packet,
	const NClient *client )
{
#ifdef IS_DEFAULT_HTTP_RECEIVE_CALLBACK_DISPLAY_STDOUT
	// Copie donnee pour affichage
	char *data;

	// Iterateur
	NU32 i;

	// Allouer memoire
	if( !( data = calloc( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Copier donnees
	memcpy( data,
		NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) );

	// Afficher reponse
	for( i = 0; i < NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ); i++ )
		printf( "%c", data[ i ] );

	// Liberer
	NFREE( data );
#endif // IS_DEFAULT_HTTP_RECEIVE_CALLBACK_DISPLAY_STDOUT

	// OK
	return NTRUE;
}

/**
 * Callback deconnexion
 *
 * @param client
 * 		Le client qui se deconnecte
 *
 * @return si l'operation s'est bien passee
 */
__CALLBACK NBOOL NHTTP_Client_Outil_CallbackDeconnexion( const NClient *client )
{
	// OK
	return NTRUE;
}

