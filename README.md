Serveur NHTTP
=============

Introduction
------------

La nouvelle version de NHTTPServeur.

Gere:

- Les logs
- Le choix du port
- La possibilite d'avoir plusieurs instances tournant sur differents ports a la fois
- Une personnalisation facile et pratique

Compilation
-----------

NMakefile is [here](https://gitlab.com/NLibProject/NLib/-/blob/master/Autre/NMakefile.c)

    NMakefile --compile "../../../NLib;." --lib "pthread;rt" --pp "NLIB_MODULE_RESEAU;NLIB_MODULE_REPERTOIRE;NHTTP_DESACTIVER_THREAD_COMMANDE;NHTTP_SERVEUR_PROJECT" NHTTP
    make

Ou

    cmake CMakeLists.txt
    make

Lancement
---------

    Lancer sur le port 8085
    ./NHTTP 8085

    Lancer sur le port par defaut defini dans NHTTP.h
    ./NHTTP

Preprocesseur
-------------

	- NHTTP_DESACTIVER_THREAD_COMMANDE:
	  Desactiver le thread de gestion des commandes texte (Utile pour lancement sous docker)

	- NHTTP_PORT_DEFAUT:
	  Le port defini par defaut si aucun parametre n'est passe au programme (Defini dans NHTTP.h)

	- NHTTP_SERVEUR_PROJECT:
	  Active le point d'entree par defaut de ce projet (Serveur)

Auteur
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/NLibProject/NHTTP.git
